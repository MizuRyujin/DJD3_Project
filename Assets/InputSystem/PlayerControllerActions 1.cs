// GENERATED AUTOMATICALLY FROM 'Assets/InputSystem/PlayerControllerActions 1.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControllerActions1 : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControllerActions1()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControllerActions 1"",
    ""maps"": [
        {
            ""name"": ""Player Controls"",
            ""id"": ""a481f5ae-3d70-4f7d-8f2a-f1443da713d5"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""8824f71e-ff18-4e20-bfcf-6bd06c65ecf8"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""5bd9a82a-8a08-4c1f-b046-ab377d94ddc8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interaction"",
                    ""type"": ""Button"",
                    ""id"": ""bb339488-c86b-4aa2-be15-235a2d0a71b7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""62cc0548-bffa-4551-a33d-cd106f698cb4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Photon Thrust"",
                    ""type"": ""Button"",
                    ""id"": ""190f6d33-1a15-40ea-b264-45fd5c184001"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CameraControl"",
                    ""type"": ""PassThrough"",
                    ""id"": ""7eeb7964-adae-47ec-921a-3e59ce475846"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PrepareForFlight"",
                    ""type"": ""Button"",
                    ""id"": ""4c017c58-0ca5-4058-ad78-f6130c63eac4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SpaceRoll"",
                    ""type"": ""PassThrough"",
                    ""id"": ""9700dfa5-a836-4972-a979-7b3ad8709ec8"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c216d0cf-5839-4838-8ae1-ab2aeb3cf03b"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""b100a7d1-00b4-4a98-953c-22d84ce5cd71"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""930b6743-3f3c-46cd-b17f-aafc911d9f67"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""5fd6103a-d9b7-42a4-8f10-6dbf32aefc27"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""0c8a192d-8fbf-44ce-bcad-777e74775a9b"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b0439e67-3439-460f-96c3-b827e1f0ec87"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""83a96c85-a6dc-43b2-908e-d0e60bb4af9b"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bbb57f51-89fa-41d3-b257-5f13b4c96595"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""245db05f-7216-48d1-a808-c9a6c03e1b8b"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Photon Thrust"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""0cc5edea-cf60-454c-94a6-3ab6ad6d2c03"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Photon Thrust"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""c089b25f-4a0e-42f7-8404-29396bbf7a0b"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Photon Thrust"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""28aa700f-ea02-47ac-9723-d23d350fa032"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Photon Thrust"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""2db12f84-6b99-4d85-96c9-1e376906f3c6"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Photon Thrust"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""a94293be-6c81-40d7-9bfb-10d2ef5fa522"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Photon Thrust"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""db622a83-1912-40dc-b7b1-a04058848cc8"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""CameraControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8f56359f-e04e-4ab9-a8f7-32d557a269cf"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""CameraControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""53c901e4-2c79-49c3-9aab-e0f918318462"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PrepareForFlight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3cbc7edc-3fd3-4a57-9b5b-0c2ef381dc53"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PrepareForFlight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a5cd476e-c48f-4842-8e2b-f8b654f7140e"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a257766f-5a1b-494b-8907-a70fdfa7e518"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interaction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""2f480aa0-ae86-4694-93db-ba98e7215c68"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpaceRoll"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ead4082e-1174-414d-ba11-9454be130790"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""SpaceRoll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""8e15ccb0-7f29-4395-91e0-888adff3d030"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""SpaceRoll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""30052846-5a93-432a-84bb-0012e1d99481"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f0aab328-2c89-4a45-bcd8-6bb7edfc5b67"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Player OnPress Sounds"",
            ""id"": ""85663bd0-29c1-4b7d-afde-35d1ddaa5ed3"",
            ""actions"": [
                {
                    ""name"": ""PrepareForFlightSound"",
                    ""type"": ""Button"",
                    ""id"": ""65db63a3-7062-4e43-a237-dbcbbac47134"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PhotonThrustSound"",
                    ""type"": ""Button"",
                    ""id"": ""68aabd78-dc66-45d1-903d-ccc4dec25acd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""InteractionSound"",
                    ""type"": ""Button"",
                    ""id"": ""f0eb6f02-c3ae-4be6-a2ab-35de4064707a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""JumpSound"",
                    ""type"": ""Button"",
                    ""id"": ""8d46e3d0-e9bd-4c28-b672-b29c25855fed"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveSound"",
                    ""type"": ""PassThrough"",
                    ""id"": ""862cee37-b5a7-430b-8696-dc5a2c7cfd9c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9c9e2adc-d582-49bb-904e-e538752cf2bb"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""MoveSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""7f15b604-6215-4144-a5c1-2526e225458f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveSound"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""e805524e-916f-4fd7-acec-3bd147d71005"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MoveSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""183e1ef6-e612-4d69-b3dc-3d7cb6fc1552"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MoveSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""e25a05c9-213d-4ca0-b5d5-ca3d2bc21fac"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MoveSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""3b13e310-d267-4dc9-b758-01247ec8b1ca"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MoveSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""c08e6a26-9195-404a-905a-66904c6c1a9a"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""JumpSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""530bc65d-d3f7-4a9f-93a4-6a104633a464"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""JumpSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2b4042b0-29d5-4f69-9baa-6843fd529717"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""InteractionSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""73a6608c-b74b-4ad7-b5aa-9230baf1da2f"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""InteractionSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""761ecb95-ebf2-468c-96a9-de4cb16f0128"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PhotonThrustSound"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""22edb3d6-6c65-4b30-908b-4ac74590f895"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PhotonThrustSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""719aee8b-5db9-47fc-8abe-bb15fe2aeba2"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PhotonThrustSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""a1bde8fb-0fb7-45b6-902b-15dbd9dfad20"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PhotonThrustSound"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""35cfb6d7-612e-4987-956e-0a42eaab5653"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PhotonThrustSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""747f4398-b47e-4640-94ae-3d7208245ced"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PhotonThrustSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""86b3201a-c598-4731-b64a-9063242ebbc3"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PrepareForFlightSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a6054618-6a31-4c0e-973e-891db870432e"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""PrepareForFlightSound"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Dev Buttons"",
            ""id"": ""6d701aab-77ac-4ef8-85f4-ca7b9df247e7"",
            ""actions"": [
                {
                    ""name"": ""Reset"",
                    ""type"": ""Button"",
                    ""id"": ""14eda163-4248-4c40-89b9-ad434932a80c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""09c10b0d-a6bc-4791-b47a-b761817f5a8f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""d69eb6c3-5184-48ad-a9fc-23acc7adc802"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2af61575-90b6-42d9-bb36-ea3547c94a40"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Reset"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4acf4ef7-40ff-4158-b729-15dcc9ef6528"",
                    ""path"": ""<Keyboard>/numLock"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""787f16b4-530a-44b9-b509-33275219909e"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": []
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": []
        }
    ]
}");
        // Player Controls
        m_PlayerControls = asset.FindActionMap("Player Controls", throwIfNotFound: true);
        m_PlayerControls_Move = m_PlayerControls.FindAction("Move", throwIfNotFound: true);
        m_PlayerControls_Jump = m_PlayerControls.FindAction("Jump", throwIfNotFound: true);
        m_PlayerControls_Interaction = m_PlayerControls.FindAction("Interaction", throwIfNotFound: true);
        m_PlayerControls_Pause = m_PlayerControls.FindAction("Pause", throwIfNotFound: true);
        m_PlayerControls_PhotonThrust = m_PlayerControls.FindAction("Photon Thrust", throwIfNotFound: true);
        m_PlayerControls_CameraControl = m_PlayerControls.FindAction("CameraControl", throwIfNotFound: true);
        m_PlayerControls_PrepareForFlight = m_PlayerControls.FindAction("PrepareForFlight", throwIfNotFound: true);
        m_PlayerControls_SpaceRoll = m_PlayerControls.FindAction("SpaceRoll", throwIfNotFound: true);
        // Player OnPress Sounds
        m_PlayerOnPressSounds = asset.FindActionMap("Player OnPress Sounds", throwIfNotFound: true);
        m_PlayerOnPressSounds_PrepareForFlightSound = m_PlayerOnPressSounds.FindAction("PrepareForFlightSound", throwIfNotFound: true);
        m_PlayerOnPressSounds_PhotonThrustSound = m_PlayerOnPressSounds.FindAction("PhotonThrustSound", throwIfNotFound: true);
        m_PlayerOnPressSounds_InteractionSound = m_PlayerOnPressSounds.FindAction("InteractionSound", throwIfNotFound: true);
        m_PlayerOnPressSounds_JumpSound = m_PlayerOnPressSounds.FindAction("JumpSound", throwIfNotFound: true);
        m_PlayerOnPressSounds_MoveSound = m_PlayerOnPressSounds.FindAction("MoveSound", throwIfNotFound: true);
        // Dev Buttons
        m_DevButtons = asset.FindActionMap("Dev Buttons", throwIfNotFound: true);
        m_DevButtons_Reset = m_DevButtons.FindAction("Reset", throwIfNotFound: true);
        m_DevButtons_Exit = m_DevButtons.FindAction("Exit", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player Controls
    private readonly InputActionMap m_PlayerControls;
    private IPlayerControlsActions m_PlayerControlsActionsCallbackInterface;
    private readonly InputAction m_PlayerControls_Move;
    private readonly InputAction m_PlayerControls_Jump;
    private readonly InputAction m_PlayerControls_Interaction;
    private readonly InputAction m_PlayerControls_Pause;
    private readonly InputAction m_PlayerControls_PhotonThrust;
    private readonly InputAction m_PlayerControls_CameraControl;
    private readonly InputAction m_PlayerControls_PrepareForFlight;
    private readonly InputAction m_PlayerControls_SpaceRoll;
    public struct PlayerControlsActions
    {
        private @PlayerControllerActions1 m_Wrapper;
        public PlayerControlsActions(@PlayerControllerActions1 wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_PlayerControls_Move;
        public InputAction @Jump => m_Wrapper.m_PlayerControls_Jump;
        public InputAction @Interaction => m_Wrapper.m_PlayerControls_Interaction;
        public InputAction @Pause => m_Wrapper.m_PlayerControls_Pause;
        public InputAction @PhotonThrust => m_Wrapper.m_PlayerControls_PhotonThrust;
        public InputAction @CameraControl => m_Wrapper.m_PlayerControls_CameraControl;
        public InputAction @PrepareForFlight => m_Wrapper.m_PlayerControls_PrepareForFlight;
        public InputAction @SpaceRoll => m_Wrapper.m_PlayerControls_SpaceRoll;
        public InputActionMap Get() { return m_Wrapper.m_PlayerControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerControlsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerControlsActions instance)
        {
            if (m_Wrapper.m_PlayerControlsActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnMove;
                @Jump.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJump;
                @Interaction.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnInteraction;
                @Interaction.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnInteraction;
                @Interaction.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnInteraction;
                @Pause.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPause;
                @PhotonThrust.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPhotonThrust;
                @PhotonThrust.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPhotonThrust;
                @PhotonThrust.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPhotonThrust;
                @CameraControl.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnCameraControl;
                @CameraControl.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnCameraControl;
                @CameraControl.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnCameraControl;
                @PrepareForFlight.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPrepareForFlight;
                @PrepareForFlight.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPrepareForFlight;
                @PrepareForFlight.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPrepareForFlight;
                @SpaceRoll.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSpaceRoll;
                @SpaceRoll.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSpaceRoll;
                @SpaceRoll.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSpaceRoll;
            }
            m_Wrapper.m_PlayerControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Interaction.started += instance.OnInteraction;
                @Interaction.performed += instance.OnInteraction;
                @Interaction.canceled += instance.OnInteraction;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @PhotonThrust.started += instance.OnPhotonThrust;
                @PhotonThrust.performed += instance.OnPhotonThrust;
                @PhotonThrust.canceled += instance.OnPhotonThrust;
                @CameraControl.started += instance.OnCameraControl;
                @CameraControl.performed += instance.OnCameraControl;
                @CameraControl.canceled += instance.OnCameraControl;
                @PrepareForFlight.started += instance.OnPrepareForFlight;
                @PrepareForFlight.performed += instance.OnPrepareForFlight;
                @PrepareForFlight.canceled += instance.OnPrepareForFlight;
                @SpaceRoll.started += instance.OnSpaceRoll;
                @SpaceRoll.performed += instance.OnSpaceRoll;
                @SpaceRoll.canceled += instance.OnSpaceRoll;
            }
        }
    }
    public PlayerControlsActions @PlayerControls => new PlayerControlsActions(this);

    // Player OnPress Sounds
    private readonly InputActionMap m_PlayerOnPressSounds;
    private IPlayerOnPressSoundsActions m_PlayerOnPressSoundsActionsCallbackInterface;
    private readonly InputAction m_PlayerOnPressSounds_PrepareForFlightSound;
    private readonly InputAction m_PlayerOnPressSounds_PhotonThrustSound;
    private readonly InputAction m_PlayerOnPressSounds_InteractionSound;
    private readonly InputAction m_PlayerOnPressSounds_JumpSound;
    private readonly InputAction m_PlayerOnPressSounds_MoveSound;
    public struct PlayerOnPressSoundsActions
    {
        private @PlayerControllerActions1 m_Wrapper;
        public PlayerOnPressSoundsActions(@PlayerControllerActions1 wrapper) { m_Wrapper = wrapper; }
        public InputAction @PrepareForFlightSound => m_Wrapper.m_PlayerOnPressSounds_PrepareForFlightSound;
        public InputAction @PhotonThrustSound => m_Wrapper.m_PlayerOnPressSounds_PhotonThrustSound;
        public InputAction @InteractionSound => m_Wrapper.m_PlayerOnPressSounds_InteractionSound;
        public InputAction @JumpSound => m_Wrapper.m_PlayerOnPressSounds_JumpSound;
        public InputAction @MoveSound => m_Wrapper.m_PlayerOnPressSounds_MoveSound;
        public InputActionMap Get() { return m_Wrapper.m_PlayerOnPressSounds; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerOnPressSoundsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerOnPressSoundsActions instance)
        {
            if (m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface != null)
            {
                @PrepareForFlightSound.started -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnPrepareForFlightSound;
                @PrepareForFlightSound.performed -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnPrepareForFlightSound;
                @PrepareForFlightSound.canceled -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnPrepareForFlightSound;
                @PhotonThrustSound.started -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnPhotonThrustSound;
                @PhotonThrustSound.performed -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnPhotonThrustSound;
                @PhotonThrustSound.canceled -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnPhotonThrustSound;
                @InteractionSound.started -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnInteractionSound;
                @InteractionSound.performed -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnInteractionSound;
                @InteractionSound.canceled -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnInteractionSound;
                @JumpSound.started -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnJumpSound;
                @JumpSound.performed -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnJumpSound;
                @JumpSound.canceled -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnJumpSound;
                @MoveSound.started -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnMoveSound;
                @MoveSound.performed -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnMoveSound;
                @MoveSound.canceled -= m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface.OnMoveSound;
            }
            m_Wrapper.m_PlayerOnPressSoundsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PrepareForFlightSound.started += instance.OnPrepareForFlightSound;
                @PrepareForFlightSound.performed += instance.OnPrepareForFlightSound;
                @PrepareForFlightSound.canceled += instance.OnPrepareForFlightSound;
                @PhotonThrustSound.started += instance.OnPhotonThrustSound;
                @PhotonThrustSound.performed += instance.OnPhotonThrustSound;
                @PhotonThrustSound.canceled += instance.OnPhotonThrustSound;
                @InteractionSound.started += instance.OnInteractionSound;
                @InteractionSound.performed += instance.OnInteractionSound;
                @InteractionSound.canceled += instance.OnInteractionSound;
                @JumpSound.started += instance.OnJumpSound;
                @JumpSound.performed += instance.OnJumpSound;
                @JumpSound.canceled += instance.OnJumpSound;
                @MoveSound.started += instance.OnMoveSound;
                @MoveSound.performed += instance.OnMoveSound;
                @MoveSound.canceled += instance.OnMoveSound;
            }
        }
    }
    public PlayerOnPressSoundsActions @PlayerOnPressSounds => new PlayerOnPressSoundsActions(this);

    // Dev Buttons
    private readonly InputActionMap m_DevButtons;
    private IDevButtonsActions m_DevButtonsActionsCallbackInterface;
    private readonly InputAction m_DevButtons_Reset;
    private readonly InputAction m_DevButtons_Exit;
    public struct DevButtonsActions
    {
        private @PlayerControllerActions1 m_Wrapper;
        public DevButtonsActions(@PlayerControllerActions1 wrapper) { m_Wrapper = wrapper; }
        public InputAction @Reset => m_Wrapper.m_DevButtons_Reset;
        public InputAction @Exit => m_Wrapper.m_DevButtons_Exit;
        public InputActionMap Get() { return m_Wrapper.m_DevButtons; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DevButtonsActions set) { return set.Get(); }
        public void SetCallbacks(IDevButtonsActions instance)
        {
            if (m_Wrapper.m_DevButtonsActionsCallbackInterface != null)
            {
                @Reset.started -= m_Wrapper.m_DevButtonsActionsCallbackInterface.OnReset;
                @Reset.performed -= m_Wrapper.m_DevButtonsActionsCallbackInterface.OnReset;
                @Reset.canceled -= m_Wrapper.m_DevButtonsActionsCallbackInterface.OnReset;
                @Exit.started -= m_Wrapper.m_DevButtonsActionsCallbackInterface.OnExit;
                @Exit.performed -= m_Wrapper.m_DevButtonsActionsCallbackInterface.OnExit;
                @Exit.canceled -= m_Wrapper.m_DevButtonsActionsCallbackInterface.OnExit;
            }
            m_Wrapper.m_DevButtonsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Reset.started += instance.OnReset;
                @Reset.performed += instance.OnReset;
                @Reset.canceled += instance.OnReset;
                @Exit.started += instance.OnExit;
                @Exit.performed += instance.OnExit;
                @Exit.canceled += instance.OnExit;
            }
        }
    }
    public DevButtonsActions @DevButtons => new DevButtonsActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerControlsActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnInteraction(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
        void OnPhotonThrust(InputAction.CallbackContext context);
        void OnCameraControl(InputAction.CallbackContext context);
        void OnPrepareForFlight(InputAction.CallbackContext context);
        void OnSpaceRoll(InputAction.CallbackContext context);
    }
    public interface IPlayerOnPressSoundsActions
    {
        void OnPrepareForFlightSound(InputAction.CallbackContext context);
        void OnPhotonThrustSound(InputAction.CallbackContext context);
        void OnInteractionSound(InputAction.CallbackContext context);
        void OnJumpSound(InputAction.CallbackContext context);
        void OnMoveSound(InputAction.CallbackContext context);
    }
    public interface IDevButtonsActions
    {
        void OnReset(InputAction.CallbackContext context);
        void OnExit(InputAction.CallbackContext context);
    }
}
