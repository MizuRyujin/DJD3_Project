﻿using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Set volume of sliders in options
/// </summary>
public class SetVolume : MonoBehaviour
{
    [SerializeField] public AudioMixer mixer;

    /// <summary>
    /// Sets volume, converts to logarithmic
    /// </summary>
    /// <param name="sliderValue"></param>
    public void SetLevel (float sliderValue)
    {
        mixer.SetFloat("MasterVol", Mathf.Log10 (sliderValue) * 20);
    }
}
