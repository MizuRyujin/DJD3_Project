using UnityEngine;

    public class MoveTowards : MonoBehaviour
    {
        [SerializeField] private float _speed;

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            transform.position = transform.position + (
                transform.forward * Time.deltaTime * _speed);
        }
    }
