﻿using System;
using Galaxy;
using Galaxy.CelestialObjects;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Galaxy.PlanetarySystem))]
public class PlanetarySystemInspector : Editor
{
    Galaxy.PlanetarySystem _system;

    private void OnEnable()
    {
        _system = target as Galaxy.PlanetarySystem;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (_system == null) return;

        GUI.backgroundColor = Color.green;
 
        if (GUILayout.Button("Add Celestial Utilities", GUILayout.Height(20)))
        {
            RecursiveChild(_system.transform, AddCelestialUtilities);
        }

        GUI.backgroundColor = Color.red;
        if (GUILayout.Button("Remove Celestial Utilities", GUILayout.Height(20)))
        {
            RecursiveChild(_system.transform, RemoveCelestialUtilities);
        }
    }

    private void RecursiveChild(Transform parent, Action<Transform> actionPerChild)
    {
        actionPerChild.Invoke(parent);

        for (int i = 0; i < parent.transform.childCount; i++)
            RecursiveChild(parent.transform.GetChild(i), actionPerChild);
    }

    private void AddCelestialUtilities(Transform child)
    {
        if (child.GetComponent<CelestialObject>() != null)
            if(child.GetComponent<CelestialUtilities>() == null)
                child.gameObject.AddComponent<CelestialUtilities>();
    }

    private void RemoveCelestialUtilities(Transform child)
    {
        CelestialUtilities utilities;
        if (child.TryGetComponent<CelestialUtilities>(out utilities))
        {
            DestroyImmediate(utilities);
        }
    }
}