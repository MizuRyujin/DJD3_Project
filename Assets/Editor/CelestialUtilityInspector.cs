﻿using Galaxy.CelestialObjects;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CelestialUtilities))]
public class CelestialUtilityInspector : Editor
{
    CelestialUtilities _celestialUtilities;

    SerializedProperty _minutes;
    SerializedProperty _seconds;
    string _text = "";

    private void OnEnable()
    {
        _celestialUtilities = target as CelestialUtilities;
        _minutes = serializedObject.FindProperty("_minutes");
        _seconds = serializedObject.FindProperty("_seconds");
    }

    public override void OnInspectorGUI()
    {
        if (_celestialUtilities == null)
        {
            EditorGUILayout.LabelField(""); // A space
            EditorGUILayout.HelpBox("No CelestialObject found on this object!",
                MessageType.Warning); // This is a small box
            return;
        }

        Space(3);
        EditorGUILayout.PropertyField(_minutes,
            new GUIContent("Minutes"));

        Space(3);
        EditorGUILayout.PropertyField(_seconds,
            new GUIContent("Seconds"));

        if (GUILayout.Button("Calculate Position", GUILayout.Height(20)))
        {
            Vector3 pos = _celestialUtilities
                .GetGetOrbitPositionFromElapsedTime(_minutes.intValue, _seconds.intValue);
            _text =
                $"At Minute: {_minutes.intValue}\nAnd Second: {_seconds.intValue}\n" +
                $"This celestial object will be at\n\nX: {pos.x}\nY: {pos.y}\nZ: {pos.z}";
        }

        EditorGUILayout.LabelField(""); // A space

        if (GUILayout.Button("Move to new position", GUILayout.Height(20)))
        {
            _celestialUtilities.MoveToNewPosition();
        }

        EditorGUILayout.LabelField(""); // A space
        EditorGUILayout.HelpBox(_text, MessageType.Info); // This is a small box

        serializedObject.ApplyModifiedProperties();
    }
    private void Space(int height)
    {
        EditorGUILayout.LabelField("", GUILayout.Height(height));
    }
}