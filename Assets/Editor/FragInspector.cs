using UnityEngine;
using UnityEditor;
using Scripts;

    /// <summary>
    /// Class that creates a special button to test loss of life of Frag boss
    /// </summary>
    [CustomEditor(typeof(BossStats))]
    public class FragInspector : Editor
    {
        /// <summary>
        /// Reference to FragStats script
        /// </summary>
        private BossStats _bossStats;

        private void OnEnable()
        {
            _bossStats = target as BossStats;    
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Take Life"))
            {
                _bossStats.OnHit();
            }
        }
    }
