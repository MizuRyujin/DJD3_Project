using UnityEngine;
using PuzzleComponents;
using UnityEditor;

[CustomEditor(typeof(Door), true)]
public class DoorInspector : Editor
{
    Door _door;

    private void OnEnable() 
    {
        _door = target as Door;    
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Toggle Door"))
        {
            Debug.Log("Pressed");
            _door.ToggleDoor();
        }
    }
}