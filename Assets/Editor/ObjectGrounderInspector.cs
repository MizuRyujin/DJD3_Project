﻿using UnityEditor;
using UnityEngine;
using Utilities;

[CustomEditor(typeof(ObjectGrounder))]
public class ObjectGrounderInspector : Editor
{    
    private ObjectGrounder grounder;

    public void OnSceneGUI()
    {
        // Target is the object you are referencing with yourTypeInstance script.
        grounder = target as ObjectGrounder;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Align Normals"))
        {
            grounder.AlignNormals();
        }
        if (GUILayout.Button("Ground Object"))
        {
            grounder.Ground();
        }
        if (GUILayout.Button("Ground and Align"))
        {
            grounder.GroundAndAlign();
        }
    }
}