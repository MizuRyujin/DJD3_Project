﻿using System;
using Galaxy.Generation;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PlanetarySysGenerator))]
public class PlanetarySysGeneratorInspector : Editor
{
    private PlanetarySysGenerator _generator;

    SerializedProperty _useCustomSeed;
    SerializedProperty _seed;
    SerializedProperty _properties;
    SerializedProperty _targetCelestial;

    public void OnEnable()
    {

        // Target is the object you are referencing with yourTypeInstance script.
        _generator = target as PlanetarySysGenerator;
        _useCustomSeed = serializedObject.FindProperty("_useCustomSeed");
        _seed = serializedObject.FindProperty("_seed");
        _properties = serializedObject.FindProperty("_systemProperties");
        _targetCelestial = serializedObject.FindProperty("_targetCelestial");

    }
    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();
        if (Application.isPlaying) return;

        Space(3);

        EditorGUILayout.PropertyField(_properties,
            new GUIContent("System Properties"));

        Space(3);

        EditorGUILayout.PropertyField(_useCustomSeed);

        if (_useCustomSeed.boolValue)
            EditorGUILayout.PropertyField(_seed);

        Space(5);

        GUIButton("Generate New", 50, _generator.NewSystem);

        Space(3);

        GUIButton("Reset Seed", 20, _generator.ResetSeed);

        Space(3);

        GUIButton("Clear", 20, _generator.DestroyCurrent);

        Space(16);

        EditorGUILayout.PropertyField(_targetCelestial,
            new GUIContent("Target Celestial"));

        Space(1);

        GUIButton("Add Moon to target celestial", 20, _generator.CreateMoon);

        // Apply changes to the serializedProperty - 
        // always do this at the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();

        EditorGUILayout.LabelField(""); // A space
        EditorGUILayout.HelpBox(_generator.GalaxyInfo, MessageType.Info); // This is a small box
        EditorGUILayout.LabelField("");
    }

    private void GUIButton(string name, int height, Action onPerformed)
    {
        if (GUILayout.Button(name, GUILayout.Height(height)))
        {
            onPerformed.Invoke();
        }
    }

    private void Space(int height)
    {
        EditorGUILayout.LabelField("", GUILayout.Height(height));
    }
}