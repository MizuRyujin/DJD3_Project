// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33098,y:32708,varname:node_3138,prsc:2|emission-739-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32549,y:32715,ptovrint:False,ptlb:Fresnel Color,ptin:_FresnelColor,varname:_FresnelColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Fresnel,id:5189,x:32562,y:32964,varname:node_5189,prsc:2|NRM-4419-OUT,EXP-3703-OUT;n:type:ShaderForge.SFN_Multiply,id:1591,x:32720,y:32828,varname:node_1591,prsc:2|A-7241-RGB,B-5189-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3703,x:32226,y:33024,ptovrint:False,ptlb:Fresnel Power,ptin:_FresnelPower,varname:_FresnelPower,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_NormalVector,id:4419,x:32214,y:32809,prsc:2,pt:False;n:type:ShaderForge.SFN_Tex2d,id:1569,x:32226,y:33156,ptovrint:False,ptlb:Surface Texture,ptin:_SurfaceTexture,varname:_SurfaceTexture,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b2c8ad33a99d46a42a6279746023df0d,ntxv:3,isnm:False|UVIN-2653-OUT;n:type:ShaderForge.SFN_Multiply,id:8511,x:32457,y:33173,varname:node_8511,prsc:2|A-1569-RGB,B-8471-RGB;n:type:ShaderForge.SFN_Color,id:8471,x:32206,y:33369,ptovrint:False,ptlb:Texture Color,ptin:_TextureColor,varname:_TextureColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.5531368,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:739,x:32910,y:33020,varname:node_739,prsc:2|A-1591-OUT,B-8511-OUT;n:type:ShaderForge.SFN_Vector1,id:3473,x:32964,y:33486,varname:node_3473,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Time,id:6882,x:31444,y:33618,varname:node_6882,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:3237,x:31665,y:33292,varname:node_3237,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:7128,x:31462,y:33821,ptovrint:False,ptlb:Spped,ptin:_Spped,varname:node_7128,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Add,id:2653,x:32003,y:33297,varname:node_2653,prsc:2|A-3237-UVOUT,B-8895-OUT;n:type:ShaderForge.SFN_Multiply,id:522,x:31651,y:33667,varname:node_522,prsc:2|A-6882-T,B-7128-OUT;n:type:ShaderForge.SFN_Add,id:8895,x:31846,y:33554,varname:node_8895,prsc:2|A-1922-OUT,B-522-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:1922,x:31459,y:33420,varname:node_1922,prsc:2|IN-9624-RGB,IMIN-4964-OUT,IMAX-9784-OUT,OMIN-4085-OUT,OMAX-1786-OUT;n:type:ShaderForge.SFN_Vector1,id:4964,x:31276,y:33406,varname:node_4964,prsc:2,v1:-1;n:type:ShaderForge.SFN_Vector1,id:9784,x:31258,y:33454,varname:node_9784,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:4085,x:31258,y:33526,varname:node_4085,prsc:2,v1:-1.5;n:type:ShaderForge.SFN_Vector1,id:1786,x:31247,y:33586,varname:node_1786,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Tex2d,id:9624,x:31258,y:33245,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:_SurfaceTexture_copy,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:2,isnm:False;proporder:7241-3703-1569-8471-7128-9624;pass:END;sub:END;*/

Shader "Shader Forge/SunSurface" {
    Properties {
        _FresnelColor ("Fresnel Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _FresnelPower ("Fresnel Power", Float ) = 0.3
        _SurfaceTexture ("Surface Texture", 2D) = "bump" {}
        _TextureColor ("Texture Color", Color) = (1,0.5531368,0,1)
        _Spped ("Spped", Float ) = 1
        [HideInInspector]_Noise ("Noise", 2D) = "black" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma target 3.0
            uniform sampler2D _SurfaceTexture; uniform float4 _SurfaceTexture_ST;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _FresnelColor)
                UNITY_DEFINE_INSTANCED_PROP( float, _FresnelPower)
                UNITY_DEFINE_INSTANCED_PROP( float4, _TextureColor)
                UNITY_DEFINE_INSTANCED_PROP( float, _Spped)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _FresnelColor_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FresnelColor );
                float _FresnelPower_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FresnelPower );
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_4964 = (-1.0);
                float node_4085 = (-1.5);
                float4 node_6882 = _Time;
                float _Spped_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Spped );
                float3 node_2653 = (float3(i.uv0,0.0)+((node_4085 + ( (_Noise_var.rgb - node_4964) * (0.5 - node_4085) ) / (1.0 - node_4964))+(node_6882.g*_Spped_var)));
                float4 _SurfaceTexture_var = tex2D(_SurfaceTexture,TRANSFORM_TEX(node_2653, _SurfaceTexture));
                float4 _TextureColor_var = UNITY_ACCESS_INSTANCED_PROP( Props, _TextureColor );
                float3 emissive = ((_FresnelColor_var.rgb*pow(1.0-max(0,dot(i.normalDir, viewDirection)),_FresnelPower_var))*(_SurfaceTexture_var.rgb*_TextureColor_var.rgb));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
