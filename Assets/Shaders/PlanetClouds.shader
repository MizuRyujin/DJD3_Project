// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33061,y:32721,varname:node_4013,prsc:2|emission-1304-RGB,alpha-5899-OUT,clip-2059-R;n:type:ShaderForge.SFN_Color,id:1304,x:32488,y:32526,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_TexCoord,id:3599,x:32039,y:32954,varname:node_3599,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:2059,x:32724,y:32978,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_2059,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ef2e9881354120646a9efb294ce1c12d,ntxv:0,isnm:False|UVIN-8019-OUT;n:type:ShaderForge.SFN_Vector4Property,id:9529,x:31832,y:33317,ptovrint:False,ptlb:UV Tiling,ptin:_UVTiling,varname:node_9529,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:1,v3:0,v4:0;n:type:ShaderForge.SFN_Multiply,id:8769,x:32298,y:33061,varname:node_8769,prsc:2|A-3599-UVOUT,B-2189-OUT;n:type:ShaderForge.SFN_ComponentMask,id:2189,x:32039,y:33163,varname:node_2189,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-9529-XYZ;n:type:ShaderForge.SFN_Time,id:4216,x:32022,y:32783,varname:node_4216,prsc:2;n:type:ShaderForge.SFN_Add,id:8019,x:32454,y:32963,varname:node_8019,prsc:2|A-2240-OUT,B-8769-OUT;n:type:ShaderForge.SFN_Multiply,id:2240,x:32259,y:32783,varname:node_2240,prsc:2|A-4484-OUT,B-4216-T;n:type:ShaderForge.SFN_ValueProperty,id:4484,x:32069,y:32627,ptovrint:False,ptlb:Cloud Speed,ptin:_CloudSpeed,varname:node_4484,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.01;n:type:ShaderForge.SFN_Slider,id:5899,x:32684,y:32871,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_5899,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1538462,max:1;proporder:1304-2059-9529-4484-5899;pass:END;sub:END;*/

Shader "Shader Forge/PlanetClouds" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _Noise ("Noise", 2D) = "white" {}
        _UVTiling ("UV Tiling", Vector) = (1,1,0,0)
        _CloudSpeed ("Cloud Speed", Float ) = 0.01
        _Opacity ("Opacity", Range(0, 1)) = 0.1538462
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _Color)
                UNITY_DEFINE_INSTANCED_PROP( float4, _UVTiling)
                UNITY_DEFINE_INSTANCED_PROP( float, _CloudSpeed)
                UNITY_DEFINE_INSTANCED_PROP( float, _Opacity)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float _CloudSpeed_var = UNITY_ACCESS_INSTANCED_PROP( Props, _CloudSpeed );
                float4 node_4216 = _Time;
                float4 _UVTiling_var = UNITY_ACCESS_INSTANCED_PROP( Props, _UVTiling );
                float2 node_8019 = ((_CloudSpeed_var*node_4216.g)+(i.uv0*_UVTiling_var.rgb.rg));
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(node_8019, _Noise));
                clip(_Noise_var.r - 0.5);
////// Lighting:
////// Emissive:
                float4 _Color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Color );
                float3 emissive = _Color_var.rgb;
                float3 finalColor = emissive;
                float _Opacity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Opacity );
                fixed4 finalRGBA = fixed4(finalColor,_Opacity_var);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _UVTiling)
                UNITY_DEFINE_INSTANCED_PROP( float, _CloudSpeed)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float _CloudSpeed_var = UNITY_ACCESS_INSTANCED_PROP( Props, _CloudSpeed );
                float4 node_4216 = _Time;
                float4 _UVTiling_var = UNITY_ACCESS_INSTANCED_PROP( Props, _UVTiling );
                float2 node_8019 = ((_CloudSpeed_var*node_4216.g)+(i.uv0*_UVTiling_var.rgb.rg));
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(node_8019, _Noise));
                clip(_Noise_var.r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
