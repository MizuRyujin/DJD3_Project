﻿Shader "Custom/ObjectCull" 
{
	Properties 
    {
		_StencilMask("Stencil mask", Int) = 0
	}

	SubShader {
		Tags {
			"RenderType" = "Opaque"
			"Queue" = "Geometry-100"
		}

		ColorMask 0
		ZWrite off

		Stencil {
			Ref[_StencilMask]
			Comp always
			Pass replace
		}

        Pass{}
	}
}