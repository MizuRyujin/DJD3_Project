// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-7910-OUT;n:type:ShaderForge.SFN_ViewVector,id:466,x:31795,y:32674,varname:node_466,prsc:2;n:type:ShaderForge.SFN_Cubemap,id:3256,x:32052,y:32754,ptovrint:False,ptlb:Galaxy Cube Map,ptin:_GalaxyCubeMap,varname:node_3256,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:d10b2672d0dc7334191aadff1976de11,pvfc:2|DIR-466-OUT;n:type:ShaderForge.SFN_Color,id:6178,x:31996,y:32521,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6178,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:6850,x:32211,y:32619,varname:node_6850,prsc:2|A-6178-RGB,B-3256-RGB;n:type:ShaderForge.SFN_Add,id:7910,x:32426,y:32822,varname:node_7910,prsc:2|A-6850-OUT,B-1131-OUT;n:type:ShaderForge.SFN_Fresnel,id:700,x:31920,y:33162,varname:node_700,prsc:2|EXP-6091-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6091,x:31654,y:33140,ptovrint:False,ptlb:Fresnel Intensity,ptin:_FresnelIntensity,varname:node_6091,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Color,id:7848,x:31873,y:33009,ptovrint:False,ptlb:Fresnel Color,ptin:_FresnelColor,varname:node_7848,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9515676,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1131,x:32133,y:32991,varname:node_1131,prsc:2|A-7848-RGB,B-700-OUT;proporder:3256-6178-6091-7848;pass:END;sub:END;*/

Shader "Shader Forge/GalaxyTexture" {
    Properties {
        _GalaxyCubeMap ("Galaxy Cube Map", Cube) = "_Skybox" {}
        _Color ("Color", Color) = (1,1,1,1)
        _FresnelIntensity ("Fresnel Intensity", Float ) = 5
        _FresnelColor ("Fresnel Color", Color) = (0,0.9515676,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma target 3.0
            uniform samplerCUBE _GalaxyCubeMap;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _Color)
                UNITY_DEFINE_INSTANCED_PROP( float, _FresnelIntensity)
                UNITY_DEFINE_INSTANCED_PROP( float4, _FresnelColor)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _Color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Color );
                float4 _FresnelColor_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FresnelColor );
                float _FresnelIntensity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FresnelIntensity );
                float3 emissive = ((_Color_var.rgb*texCUBE(_GalaxyCubeMap,viewDirection).rgb)+(_FresnelColor_var.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelIntensity_var)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
