#ifndef GREYSCALE_INCLUDE
#define GREYSCALE_INCLUDE

void Greyscale_float(float3 RGB, out float3 Out)
{
    Out = (RGB.r + RGB.g + RGB.b) / 3;
}
#endif