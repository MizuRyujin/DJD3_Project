﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Utilities
{
    [ExecuteAlways]
    public class ObjectGrounder : MonoBehaviour
    {
        private const string _NULL_TARGET_MSG = "Good job! You can click a button, " +
            "now give me a target.";

        [Space]
        [SerializeField] private GameObject _targetObject = null;

        [SerializeField] private bool _updateRotationInEditMode;

        [SerializeField] private RaycastHit hit;

        private void Awake()
        {
            if (Application.isPlaying)
                Destroy(this);
        }

        private bool DoLinecast()
        {
            return (Physics.Linecast(transform.position, _targetObject.transform.position, out hit));
        }

        public void GroundAndAlign()
        {
            AlignNormals();
            Ground();
        }

        public void Ground()
        {
            if (_targetObject == null)
            {
                Debug.LogError(_NULL_TARGET_MSG);
                return;
            }

            RaycastHit hit;
            if (Physics.Linecast(transform.position, _targetObject.transform.position, out hit))
            {
                transform.position = hit.point;
                transform.SetParent(hit.transform);
            }
            else
            {
                Debug.LogError("Unable to ground object, please give a collider" +
                    $"to {_targetObject.name}");
            }
        }

        public bool AlignNormals()
        {
            if (_targetObject == null)
            {
                Debug.LogError(_NULL_TARGET_MSG);
                return false;
            }

            if (!DoLinecast())
            {
                Debug.LogError("Unable to ground object, please give a collider" +
                    $" to {_targetObject.name}");
                return false;
            }

            transform.up = transform.position - _targetObject.transform.position;
            return true;
        }

        private void Update()
        {
            if (_updateRotationInEditMode && _targetObject != null)
            {
                transform.LookAt(_targetObject.transform.position);
                transform.up = -transform.forward;
            }
        }

        private void OnDrawGizmos()
        {
            if (_targetObject == null) return;
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, _targetObject.transform.position);
        }
    }
}