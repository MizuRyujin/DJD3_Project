﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MoveAlongSurface : MonoBehaviour
{
    [SerializeField] private LayerMask hitMask = default;
    [SerializeField] private float smooth;
    [SerializeField] private Vector3 offset;

    private Ray ray;
    private RaycastHit info;
    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(0, 0, 0.1f);
        ray = new Ray(transform.position, -transform.forward);
    }

    // Update is called once per frame
    void Update()
    {
        ray.origin = transform.position;
        ray.direction = -transform.forward;

        Debug.DrawRay(transform.position, -transform.forward, Color.blue);
        if (Physics.Raycast(ray, out info, 1, hitMask))
        {
            transform.localRotation = Quaternion.FromToRotation(Vector3.forward, info.normal);
            Debug.DrawRay(info.point, info.normal, Color.green);
        }
        else
        {
            Debug.DrawRay(transform.position, -transform.forward, Color.red);
        }

    }
}
