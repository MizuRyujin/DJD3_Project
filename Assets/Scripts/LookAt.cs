using UnityEngine;

[ExecuteInEditMode]
public class LookAt : MonoBehaviour
{
    [SerializeField] private Transform toLookat;
    private bool look;
    private Vector3 newDir;

    private void Awake() 
    {
        look = false;
    }

    private void Update() 
    {
        newDir = transform.position - toLookat.position;
        transform.forward = newDir;
    }

    public void LookTarget(Transform newTarget)
    {
        toLookat = newTarget;
    }

}