﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopToMainMenu : MonoBehaviour
{
    [SerializeField] private float _time = default;
    private WaitForSeconds _timer = default;

    // Start is called before the first frame update
    void Start()
    {
        _timer = new WaitForSeconds(_time);
        StartCoroutine(TimerBeforeWhoosh());
    }

    private IEnumerator TimerBeforeWhoosh()
    {
        yield return _timer;
        Debug.LogError("Go back to main menu");
        SceneLoader.Load("Main Menu");
    }
}
