﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CluckHolderMove : MonoBehaviour
{
    [SerializeField] private Transform _child = default;
    [SerializeField] private float _rotSpeed = default;
    [SerializeField] private float _movSpeed = default;
    [SerializeField] private Transform[] _navPoints = default;

    private Vector3 _moveDir;
    private int _navIndex;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        _navIndex = 0;
        _moveDir = _navPoints[_navIndex].position - transform.position;
    }

    // Update is called once per frame
    private void Update()
    {
        _child.transform.Rotate(_child.transform.up, _rotSpeed * Time.deltaTime);

        if (transform.position != _navPoints[_navIndex].position)
        {
            transform.Translate(_moveDir * _movSpeed * Time.deltaTime);
        }
        else
        {
            _navIndex++;
            _navIndex = _navIndex > _navPoints.Length ? 0 : _navIndex;
            _moveDir = _navPoints[_navIndex].position - transform.position;
        }
    }
}
