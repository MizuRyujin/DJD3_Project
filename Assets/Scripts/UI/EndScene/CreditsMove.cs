﻿using UnityEngine;

public class CreditsMove : MonoBehaviour
{
    [SerializeField] private float SPEEEEEEED = default;

    private float _time;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.up * SPEEEEEEED * Time.deltaTime);

        _time += 1 * Time.deltaTime;

        print(_time);
    }
}
