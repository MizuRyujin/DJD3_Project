﻿using UnityEngine;
using UnityEngine.UI;

public class HpIconArray : MonoBehaviour
{
    [SerializeField] private Image[] _hitCounter = default;

    public Image[] HitCounter => _hitCounter;


    //! THIS IS A BAAAAAD IMPLEMENTATION, I DIDN'T WANT TO THINK MUCH
    /// <summary>
    /// Method to disable a life counter
    /// </summary>
    public void LoseHP()
    {
        for (int i = 0; i < _hitCounter.Length; i++)
        {
            if (_hitCounter[i].gameObject.activeSelf)
            {
                _hitCounter[i].gameObject.SetActive(false);
                return;
            }
        }
    }
}
