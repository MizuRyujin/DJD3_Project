﻿using System.Collections;
using Cinemachine;
using Player;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class TVOptions : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private GameObject _optionSettingsHolder = default;
        [SerializeField] private CinemachineVirtualCamera _settingsCamera = default;
        [SerializeField] private TextMeshProUGUI _resetSaveTxt = default;
        [SerializeField] private AudioSource _menuAudioSource = default;
        [SerializeField] private Selectable _defaultSelectable = default;
        [SerializeField] private Button _onCloseSelectedButton = default;
        [SerializeField] private CameraValues _camValues = default;

        [Header("Tabs")]
        [SerializeField] private GameObject _optionsTab = default;
        [SerializeField] private Image _optionsTabSelectedImage = default;
        [SerializeField] private GameObject _soundTab = default;
        [SerializeField] private Image _soundTabSelectedImage = default;
        [SerializeField] private GameObject _controlsTab = default;
        [SerializeField] private Image _controlsTabSelectedImage = default;

        [Header("TV")]
        [SerializeField] private GameObject _tvButton = default;
        [SerializeField] private GameObject _tvSlider = default;

        [Header("Sounds")]
        [SerializeField] private AudioClip _tabChangeClip = default;
        [SerializeField] private AudioClip _selectionChangeClip = default;
        [SerializeField] private AudioClip _clickClip = default;
        [SerializeField] private AudioClip _sliderClip = default;

        private SaveSystem.SaveSystemHelper _saveHelper;

        private Vector3 _TVButtonInitialPosition;
        private int _rotIndex;
        private bool _waitingForResetCheck;
        private bool _cursorWasVisible;
        private PauseMenu _pauseMenu;

        public bool Opened { get; private set; }

        private void Awake()
        {
            Opened = false;
            _rotIndex = 1;
            _saveHelper = GameObject.FindObjectOfType<SaveSystem.SaveSystemHelper>();
            _TVButtonInitialPosition = _tvButton.transform.position;
            UpdateObjects();
        }

        public void Open()
        {
            if (Opened) return;
            Opened = true;
            _cursorWasVisible = false;
            _defaultSelectable.Select();
            Debug.Log(Cursor.visible);
            if (Cursor.visible)
            {
                _cursorWasVisible = true;
                Cursor.visible = false;
            }
            UpdateObjects();
        }

        public void Close()
        {
            if (!Opened) return;
            Opened = false;
            EventSystem.current.SetSelectedGameObject(null);
            if (_cursorWasVisible)
                Cursor.visible = true;
            //Cursor.visible = Opened;
            Invoke("UpdateObjects", 0.3f);
            _onCloseSelectedButton?.Select();
        }

        private void UpdateObjects()
        {
            _waitingForResetCheck = false;
            UpdateResetText();
            _settingsCamera.enabled = Opened;
            _optionSettingsHolder.SetActive(Opened);
        }

        private void UpdateResetText()
        {
            if (_waitingForResetCheck)
                _resetSaveTxt.SetText("ARE YOU SURE?");
            else
                _resetSaveTxt.SetText("RESET SAVE");
        }

        public void TvButtonPress()
        {
            if (LeanTween.isTweening(_tvButton)) LeanTween.cancel(_tvButton);
            print("Henlo? Fren?");

            _tvButton.transform.position = _TVButtonInitialPosition;

            LeanTween.moveLocalX(_tvButton, _tvButton.transform.localPosition.x + 0.04f, 0.5f).setEasePunch();
        }

        public void TvButtonRotate()
        {
            if (LeanTween.isTweening(_tvSlider)) LeanTween.cancel(_tvSlider);

            LeanTween.rotateZ(_tvSlider, 45 * _rotIndex, .6f).setEaseOutCirc();

            int add = 0;

            if (_rotIndex * 45 > 360)
                add = -1;
            else if (_rotIndex * 45 < 360)
                add = 1;
            else
                add = Random.Range(0f, 1f) > 0.5f ? 1 : -1;

            _rotIndex += add;
        }

        public void ResetSavePressed()
        {
            if (_waitingForResetCheck)
                _saveHelper.DeleteSave();

            _waitingForResetCheck = !_waitingForResetCheck;
            UpdateResetText();
        }

        public void MouseSensitivityChange(float sliderValue)
        {
            _camValues.STILL_ANGULAR_VELOCITY_FACTOR = sliderValue;
        }

        public void TriggerOptionsTab()
        {
            _waitingForResetCheck = false;
            UpdateResetText();
            EnableTab(_optionsTab);
        }

        public void TriggerSoundTab()
        {
            EnableTab(_soundTab);
        }

        public void TriggerControlsTab()
        {
            EnableTab(_controlsTab);
        }

        public void TabChangeSound()
        {
            _menuAudioSource.pitch = Random.Range(0.75f, 1f);
            _menuAudioSource.PlayOneShot(_tabChangeClip);
        }

        public void ClickSound()
        {
            _menuAudioSource.pitch = Random.Range(0.85f, 1f);
            _menuAudioSource.PlayOneShot(_clickClip);
        }

        public void SliderSound()
        {
            _menuAudioSource.pitch = Random.Range(0.85f, 1f);
            _menuAudioSource.PlayOneShot(_sliderClip);
        }

        public void SelectionChangeSound()
        {
            _menuAudioSource.pitch = Random.Range(0.8f, 1f);
            _menuAudioSource.PlayOneShot(_selectionChangeClip);
        }

        private void OnApplicationFocus(bool focusStatus)
        {
            if (!focusStatus) Close();
        }

        private void EnableTab(GameObject tab)
        {
            _controlsTab.SetActive(_controlsTab == tab);
            _optionsTab.SetActive(_optionsTab == tab);
            _soundTab.SetActive(_soundTab == tab);
            _controlsTabSelectedImage.enabled = _controlsTab == tab;
            _optionsTabSelectedImage.enabled = _optionsTab == tab;
            _soundTabSelectedImage.enabled = _soundTab == tab;
        }
    }
}