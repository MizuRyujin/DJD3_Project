﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CinematicBars : MonoBehaviour
{
    private RectTransform _topBar, _bottomBar;
    private float _targetSize;
    private float _changeSpeed;
    private bool _active;

    private void Awake() 
    {
        // Create each bar
        GameObject obj = new GameObject("TopBar", typeof(Image));
        obj.transform.SetParent(transform, false);
        obj.GetComponent<Image>().color = Color.black;
        _topBar = obj.GetComponent<RectTransform>();
        _topBar.anchorMin = new Vector2(0, 1);
        _topBar.anchorMax = new Vector2(1, 1);
        _topBar.sizeDelta = new Vector2(0, 0);

        obj = new GameObject("BottomBar", typeof(Image));
        obj.transform.SetParent(transform, false);
        obj.GetComponent<Image>().color = Color.black;
        _bottomBar = obj.GetComponent<RectTransform>();
        _bottomBar.anchorMin = new Vector2(0, 0);
        _bottomBar.anchorMax = new Vector2(1, 0);
        _bottomBar.sizeDelta = new Vector2(0, 0);
    }

    private void Update() 
    {
        if (!_active) return;
        Vector2 sizeDelta = _topBar.sizeDelta;
        // Calculate the delta with the speed at witch is suppose to move
        //! Since we calculate the speed in the functions and change the speed variable
        //! directly, we only need the speed, Since the speed is signed (+/-)
        //! with the currect direction of the movement, we only need the speed to move
        //! the bars.
        sizeDelta.y += _changeSpeed * Time.deltaTime;

        // Stops moving if it has passed the target
        if (_changeSpeed > 0)
        {
            if (sizeDelta.y >= _targetSize)
            {
                sizeDelta.y = _targetSize;
                _active = false;
            }
        }
        else if (_changeSpeed < 0)
        {
            if (sizeDelta.y <= _targetSize)
            {
                sizeDelta.y = _targetSize;
                _active = false;
            }
        }
        
        // Update the values
        _topBar.sizeDelta = sizeDelta;
        _bottomBar.sizeDelta = sizeDelta;
    }

    /// <summary>
    /// Show the cinematic bars
    /// </summary>
    /// <param name="targetSize"> target size of the cinematic bars </param>
    /// <param name="time"> time to take for the animation to complete </param>
    public void Show(float targetSize, float time)
    {
        _targetSize = targetSize;
        // Set the speed of the bar
        //! For anyone wondering, the distance between the 2 points divided by the
        //! time you want something to take gives you the speed and direction of which the object
        //! has to move.
        _changeSpeed = (_targetSize - _topBar.sizeDelta.y) / time;
        _active = true;
    }

    /// <summary>
    /// Hide cinematic bars
    /// </summary>
    /// <param name="time"> time to take for the bars to hide</param>
    public void Hide(float time)
    {
        // Target sizeDelta equals 0, therefore it will hide the bars
        _targetSize = 0;
        // Set the speed of the bar
        //! For anyone wondering, the distance between the 2 points divided by the
        //! time you want something to take gives you the speed of which the object
        //! has to move.
        _changeSpeed = (_targetSize - _topBar.sizeDelta.y) / time;
        _active = true;
    }
}
