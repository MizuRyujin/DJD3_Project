using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class SoftAnimations : MonoBehaviour
    {
        [SerializeField] private LeanTweenType _easeType = LeanTweenType.easeOutCirc;
        [SerializeField] private float _intensity = 1;
        [SerializeField] private float _duration = 0.4f;
        public void Enlarge()
        {
            StopTween();
            LeanTween.scale(
                gameObject,
                transform.localScale * _intensity * 1.2f,
                _duration).setEase(_easeType);
        }

        public void Shrink()
        {
            StopTween();
            LeanTween.scale(
                gameObject,
                transform.localScale * -_intensity,
                _duration).setEase(_easeType);
        }

        public void BackToNormal()
        {
            StopTween();
            LeanTween.scale(gameObject, Vector3.one, _duration * 0.5f)
                .setEase(_easeType);
        }

        private void StopTween()
        {
            if (LeanTween.isTweening(gameObject))
                LeanTween.cancel(gameObject);
        }
    }
} 