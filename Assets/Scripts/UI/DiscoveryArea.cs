﻿using System.Collections.Generic;
using Interactables;
using Interactables.Individuals;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Galaxy;

namespace UI
{
    public class DiscoveryArea : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private RectTransform _parent = default;
        [SerializeField] private Image _icon = default;
        [SerializeField] private TextMeshProUGUI _title = default;
        [SerializeField] private TextMeshProUGUI _name = default;

        [Header("Parameters")]
        [SerializeField] private LeanTweenType _slideEaseType = default;
        [SerializeField] private float _slideDuration = default;
        [SerializeField] private float _slideDistance = default;
        [SerializeField] private float _idleDuration = default;
        [SerializeField] private AudioClip[] _discoverySounds = default;

        private static DiscoveryArea _activeInstance;
        private PlanetarySystem _system;
        private Queue<PlanetaryDiscovery> _queuedDiscoveries;
        private Queue<PhotonCharge> _queuedPhotonCharges;
        private Vector3 _initialPos;
        private bool _displaying;
        private AudioSource _audioSource;

        private void Awake()
        {
            _activeInstance = this;
            _queuedDiscoveries = new Queue<PlanetaryDiscovery>(3);
            _queuedPhotonCharges = new Queue<PhotonCharge>(3);
            _system = GameObject.FindObjectOfType<PlanetarySystem>();
            _initialPos = _parent.GetComponent<RectTransform>().anchoredPosition;
            _displaying = false;
            _audioSource = GetComponent<AudioSource>();
        }

        public static void NewDiscoveryFound(PlanetaryDiscovery discovery) =>
            _activeInstance?.InstanceDiscoveryFound(discovery);
        public static void NewPhotonChargeFound(PhotonCharge charge) =>
            _activeInstance?.InstanceChargeFound(charge);

        private void InstanceDiscoveryFound(PlanetaryDiscovery discovery)
        {
            _queuedDiscoveries.Enqueue(discovery);

            if (!_displaying)
                Show(_queuedDiscoveries.Dequeue());
        }

        private void InstanceChargeFound(PhotonCharge charge)
        {
            _queuedPhotonCharges.Enqueue(charge);

            if (!_displaying)
                Show(_queuedPhotonCharges.Dequeue());
        }

        private void Show(PlanetaryDiscovery d)
        {
            _displaying = true;
            _title.SetText("NEW DISCOVERY");
            _name.SetText(d.DiscoveryName);
            SlideIn();
        }

        private void Show(PhotonCharge c)
        {
            _displaying = true;
            _title.SetText("PHOTON CHARGE");
            int collected = _system.PhotonChargesCollected;
            int all = _system.PhotonCharges.Length;
            string final = collected == all ?
                "<SIZE=90%>All collected,\nthe star awaits." :
                _system.PhotonChargesCollected + "/" + _system.PhotonCharges.Length;
            _name.SetText( final);
            SlideIn();
        }

        private void SlideIn()
        {
            _audioSource.PlayOneShot(GetRandomSound());
            LeanTween.moveX(_parent, _parent.anchoredPosition.x - _slideDistance, 1.0f)
                .setEase(_slideEaseType)
                .setOnComplete(IdleTime);
        }

        private void IdleTime()
        {
            LeanTween.scale(_icon.gameObject, Vector3.one * 1.5f, _idleDuration / 2)
                .setEasePunch()
                .setDelay(_idleDuration / 3);
            Invoke("SlideOut", _idleDuration);
        }

        private void SlideOut()
        {
            LeanTween.move(_parent, _initialPos, 1.0f)
                .setEase(_slideEaseType)
                .setOnComplete(OnDisplayEnd);
        }

        private void OnDisplayEnd()
        {
            if (_queuedPhotonCharges.Count > 0)
                Show(_queuedPhotonCharges.Dequeue());
            else if (_queuedDiscoveries.Count > 0)
                Show(_queuedDiscoveries.Dequeue());
            else
                _displaying = false;
        }

        private AudioClip GetRandomSound()
        {
            AudioClip clip;
            clip = _discoverySounds[Random.Range(0, _discoverySounds.Length)];
            return clip;
        }
    }
}