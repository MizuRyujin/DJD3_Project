﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using SaveSystem;

namespace UI
{
    public class MainMenuController : MonoBehaviour
    {
        private const int MAX_INDEX = 2;
        private const int MIN_INDEX = 0;

        [SerializeField] private float _moveCooldownAmount = 0.2f;
        [SerializeField] private string _takeoffSceneToLoad = "PrototypeScene";

        [Header("Cinemachine References")]
        [SerializeField] private CinemachineBrain _cineBrain = default;
        [SerializeField] private CinemachineVirtualCamera _takeoffCamera = default;
        [SerializeField] private CinemachineVirtualCamera _optionsCamera = default;
        [SerializeField] private CinemachineVirtualCamera _exitCamera = default;

        [Header("Other references")]
        [SerializeField, Tooltip("Following menu index order")]
        private ParticleSystem[] _selectionParticles = default;
        [SerializeField] private TVOptions _TVOptions = default;

        private UIControls _uiControls;
        private int _selectionIndex;
        private Vector2 _navigationInput;
        private Vector2 _previousInput;

        private float _moveCooldown;
        private float _transitionCooldownAmount;
        private float _transitionCooldown;

        private bool CanNavigate => _moveCooldown <= 0 && CanInteract;
        private bool OnTransition => _transitionCooldown > 0;
        private bool CanInteract => !_TVOptions.Opened;
        private SaveSystemHelper _saveHelper;

        private void Awake()
        {
            _uiControls = new UIControls();
            _uiControls.BasicNavigation.Select.performed += ctx => OnPressed();
            _uiControls.BasicNavigation.Pause.performed += ctx => _TVOptions.Close();
            _uiControls.BasicNavigation.Move.performed += ctx =>
                _navigationInput = ctx.ReadValue<Vector2>();

            _selectionIndex = 0;
            _transitionCooldownAmount = _cineBrain.m_DefaultBlend.m_Time;
            _transitionCooldownAmount = _transitionCooldownAmount -
                (_transitionCooldownAmount * 0.1f);
        
            _saveHelper = GetComponent<SaveSystemHelper>();
        }

        private void Start() {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Confined;
        }

        private void Update()
        {
            UpdateNavigation();

            _moveCooldown = _moveCooldown > 0 ?
                _moveCooldown - Time.deltaTime :
                0;
            _transitionCooldown = _transitionCooldown > 0 ?
                _transitionCooldown - Time.deltaTime :
                0;
            _previousInput = _navigationInput;
        }

        private void UpdateNavigation()
        {
            if (CanNavigate)
            {
                if (_navigationInput.x > 0.01f)
                    PreviousIndex();
                else if (_navigationInput.x < -0.01f)
                    NextIndex();
            }
            else
            {
                // If the player is in a rush somewhy, let him zoom through the menu
                if (Mathf.Abs(_navigationInput.x) < Mathf.Abs(_previousInput.x))
                    _moveCooldown = 0;
            }
        }

        private void NextIndex()
        {
            Debug.Log("Next Index");
            _selectionIndex++;

            if (_selectionIndex > MAX_INDEX)
                _selectionIndex = MIN_INDEX;

            OnIndexChanged();
        }

        private void PreviousIndex()
        {
            Debug.Log("Previous Index");
            _selectionIndex--;

            if (_selectionIndex < MIN_INDEX)
                _selectionIndex = MAX_INDEX;

            OnIndexChanged();
        }

        private void OnIndexChanged()
        {
            _moveCooldown = _moveCooldownAmount;
            _transitionCooldown = _transitionCooldownAmount;

            switch (_selectionIndex)
            {
                case 0:
                    // Take off selected
                    EnableCam(_takeoffCamera);
                    break;
                case 1:
                    // Options selected
                    EnableCam(_optionsCamera);
                    break;
                case 2:
                    // Exit selected
                    EnableCam(_exitCamera);
                    break;
            }
        }

        private void OnPressed()
        {
            if (OnTransition || !CanInteract) return;
            _selectionParticles[_selectionIndex].Emit(1);

            switch (_selectionIndex)
            {
                case 0:
                    // Take off selected
                    TakeoffPressed();
                    break;
                case 1:
                    // Options selected
                    OptionsPressed();
                    break;
                case 2:
                    // Exit selected
                    ExitPressed();
                    break;
            }
        }

        private void EnableCam(CinemachineVirtualCamera cam)
        {
            _takeoffCamera.enabled = _takeoffCamera == cam;
            _optionsCamera.enabled = _optionsCamera == cam;
            _exitCamera.enabled = _exitCamera == cam;
        }

        private void TakeoffPressed()
        {
            _saveHelper.LoadSaveFile();
            SceneLoader.Load(_takeoffSceneToLoad);
        }

        private void OptionsPressed()
        {
            Debug.LogWarning("Options Pressed");
            _TVOptions.Open();
        }

        private void ExitPressed()
        {
            Debug.LogWarning("Exit Pressed");
            Application.Quit();
        }

        private void OnEnable() => _uiControls.Enable();
        private void OnDisable() => _uiControls.Disable();
    }
}