﻿using UnityEngine;
using UnityEngine.UI;
using Player.Movement;

namespace UI
{
    public class PlayerUIListener : MonoBehaviour
    {
        // Temp stuff, will be much fancier than a common slider (hopefully)
        private HpIconArray _hpImage;
        private Slider _staminaSlider;
        private Image[] _hpCounters;
        private Player.Player player;

        private float GetStaminaPercentage =>
            player.AttachedMovement.MoveBehaviour.CurrentStamina / player.Values.MaxStamina;

        private byte GetHitPoints => player.PlayerHP;

        private void Awake()
        {
            player = GameObject.FindObjectOfType<Player.Player>();
            _staminaSlider = GetComponentInChildren<Slider>();
            _hpImage = GetComponentInChildren<HpIconArray>();
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            if (PlayerMovement.staticBossFight || player.AttachedMovement.BossFight)
            {
                _staminaSlider.gameObject.SetActive(false);
                _hpImage.gameObject.SetActive(true);
            }
            else
            {
                _staminaSlider.gameObject.SetActive(true);
                _hpImage.gameObject.SetActive(false);
            }
        }

        private void Update()
        {
            if (!player.AttachedMovement.BossFight ||
                !player.AttachedMovement.BossFight)
            {
                UpdateStaminaSlider();
            }
        }

        private void UpdateStaminaSlider()
        {
            _staminaSlider.value = GetStaminaPercentage;
            if (GetStaminaPercentage <= 0.99f)
            {
                _staminaSlider.gameObject.SetActive(true);
            }
            else
            {
                _staminaSlider.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        void OnEnable()
        {
            player.PlayerHit += _hpImage.LoseHP;
        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled or inactive.
        /// </summary>
        void OnDisable()
        {
            player.PlayerHit -= _hpImage.LoseHP;
        }

    }
}