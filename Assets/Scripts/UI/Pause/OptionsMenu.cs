﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UI
{
    public class OptionsMenu : MonoBehaviour
    {
        [SerializeField] private Button _closeButton = null;
        [SerializeField] private GameObject _holder = null;
        [SerializeField] private Button _onCloseSelectedButton = null;
        private void Awake()
        {
            Open();
            _closeButton.onClick.AddListener(Close);
            Close();
        }

        public void Open()
        {
            _holder.SetActive(true);
            _closeButton.Select(); 
        }

        public void Close()
        {
            _holder.SetActive(false);
            _onCloseSelectedButton?.Select();
        }
    }
}