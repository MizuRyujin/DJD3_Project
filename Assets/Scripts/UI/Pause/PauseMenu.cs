﻿using System.Collections;
using System.Collections.Generic;
using Galaxy;
using SaveSystem;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class PauseMenu : MonoBehaviour
    {
        [Header("Independent Elements")]
        [SerializeField] private Transform _pauseCamera = null;
        [SerializeField] private GameObject _pausePlanet = null;
        [SerializeField] private GameObject _holder = null;
        [SerializeField] private TextMeshProUGUI _pausePro = null;
        [SerializeField] private TVOptions _TVOptions = default;

        [SerializeField] private Image[] _bars;

        [Header("Buttons")]
        [SerializeField] private Button _resumeBtn = null;
        [SerializeField] private Button _optionsBtn = null;
        [SerializeField] private Button _quitBtn = null;

        [Header("Information Elements")]
        [SerializeField] private TextMeshProUGUI _systemNamePro = null;
        [SerializeField] private TextMeshProUGUI _collectableHeader = null;
        [SerializeField] private TextMeshProUGUI _discoveriesHeader = null;
        [SerializeField] private TextMeshProUGUI _collectableInfo = null;
        [SerializeField] private TextMeshProUGUI _discoveriesInfo = null;

        [Header("Parameters")]
        [SerializeField] private float _openAnimationSpeed = 1.0f;
        [SerializeField] private float _quitAnimationSpeed = 1.0f;

        private SaveSystemHelper _saveHelper;
        private Player.Player _player;
        private List < (GameObject obj, Vector3 initialPos, Vector3 initialScale, RectTransform rect) > _defaults;
        private(Button btn, int index) [] _buttons;
        private(TextMeshProUGUI pro, int index) [] _infoText;
        private Coroutine _animationCor;
        private PlanetarySystem _system;

        private bool _canInteract;
        public static bool PauseMenuActive { get; private set; }
        private bool CanPause => (!_player.Interacting || PauseMenuActive) && !_TVOptions.Opened;

        private void Awake()
        {
            _saveHelper = GetComponent<SaveSystemHelper>();
            _holder.SetActive(true);
            _player = GameObject.FindObjectOfType<Player.Player>();

            _canInteract = true;

            _resumeBtn.onClick.AddListener(CloseMenu);
            _optionsBtn.onClick.AddListener(OpenOptions);
            _quitBtn.onClick.AddListener(Quit);
            InitializeCollections();
            _holder.SetActive(false);
        }

        private void Start()
        {
            _player.PControls.PlayerControls.Pause.performed += _ => TogglePause();
            _system = FindObjectOfType<PlanetarySystem>();
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            _pausePlanet.SetActive(false);
        }

        private void TogglePause()
        {
            Debug.Log("Pause");
            if (!CanPause) return;

            PauseMenuActive = !PauseMenuActive;

            StopActiveTweens();

            if (PauseMenuActive) OpenMenu();
            else CloseMenu();
        }

        private void Resume()
        {
            // Maybe wait for camera transition to end? later
            Cursor.visible = false;
            _canInteract = true;
            _holder.SetActive(false);
            _player.TogglePause(false);
            _pausePlanet.SetActive(false);
        }

        private void OpenOptions()
        {
            _TVOptions.Open();
        }

        private void Quit()
        {
            _saveHelper.SaveGame();
            SceneLoader.Load("Main Menu");
        }

        private void OpenMenu()
        {
            ToggleButtonsInteractable(false);
            _holder.SetActive(true);
            _player.TogglePause(true);
            _TVOptions.Close();
            _canInteract = false;
            _discoveriesInfo.text = Mathf.RoundToInt(_system.DiscoveriesFoundPercentage * 100) + "%";
            _collectableInfo.text = _system.PhotonChargesCollected + "/" + _system.PhotonCharges.Length;
            _animationCor = StartCoroutine(COpenAnimations());
            _resumeBtn.Select();
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }

        private void CloseMenu()
        {
            ResetToInitialValues();
            CloseAnimationsTweening();
            _canInteract = false;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        #region Animations
        private IEnumerator COpenAnimations()
        {
            _pausePlanet.SetActive(true);
            PrepareForOpenAnimation();
            yield return new WaitForSeconds(2.3f);
            OpenAnimationsTweening();
            Cursor.visible = true;
            _animationCor = null;
        }

        private void OpenAnimationsTweening()
        {
            if (!PauseMenuActive) return;

            float buttonDelay = 0.5f;
            float infoDelay = .5f;

            LeanTween.move(
                    _defaults[0].rect,
                    _defaults[0].initialPos,
                    _openAnimationSpeed)
                .setEaseOutCirc();

            // Buttons
            for (int i = 0; i < _buttons.Length; i++)
            {
                LTDescr tween =
                    LeanTween.move(
                        _defaults[_buttons[i].index].rect,
                        _defaults[i + 1].initialPos,
                        _openAnimationSpeed)
                    .setEaseOutBack().setDelay(i * buttonDelay);

                if (i == _buttons.Length - 1) tween.setOnComplete(OpenBar);
            }

            // Info
            LeanTween.move(
                    _defaults[4].rect,
                    _defaults[4].initialPos,
                    _openAnimationSpeed * 0.5f)
                .setEaseOutCirc().setDelay((infoDelay));
            for (int i = 0; i < _infoText.Length; i++)
                LeanTween.move(
                    _defaults[_infoText[i].index].rect,
                    _defaults[i + 5].initialPos,
                    _openAnimationSpeed * 0.8f)
                .setEaseOutCirc().setDelay((i + 1) * infoDelay);

            void OpenBar()
            {
                _canInteract = true;

                _bars[0].transform.localScale *= 0.01f;
                LeanTween.scale(
                    _bars[0].gameObject,
                    _defaults[9].initialScale,
                    _openAnimationSpeed).setEaseInOutCirc();
                _bars[0].gameObject.SetActive(true);
                ToggleButtonsInteractable(true);
            }
        }

        private void ToggleButtonsInteractable(bool value)
        {
            for (int i = 0; i < _buttons.Length; i++)
                _buttons[i].btn.interactable = value;
        }

        private void CloseAnimationsTweening()
        {
            LTDescr finalTween = default;

            foreach ((GameObject, Vector3, Vector3, RectTransform) inf in _defaults)
                finalTween =
                LeanTween.scale(inf.Item1, Vector3.one * 0.1f, _quitAnimationSpeed).setEaseInBack();

            finalTween.setOnComplete(Resume);
        }

        private void PrepareForOpenAnimation()
        {
            ResetToInitialValues();

            // Header
            Vector2 newAnchor = _defaults[0].rect.anchoredPosition;
            newAnchor.y += 445;
            _defaults[0].rect.anchoredPosition = newAnchor;

            //Buttons
            for (int i = 0; i < _buttons.Length; i++)
            {
                newAnchor = _defaults[_buttons[i].index].rect.anchoredPosition;
                newAnchor.x -= 400;
                _defaults[_buttons[i].index].rect.anchoredPosition = newAnchor;
            }

            // Info

            // System name
            newAnchor = _defaults[4].rect.anchoredPosition;
            newAnchor.x += 450;
            _defaults[4].rect.anchoredPosition = newAnchor;

            for (int i = 0; i < _infoText.Length; i++)
            {
                newAnchor = _defaults[_infoText[i].index].rect.anchoredPosition;
                newAnchor.x += 355;
                _defaults[_infoText[i].index].rect.anchoredPosition = newAnchor;
            }

            // Bars
            for (int i = 0; i < _bars.Length; i++)
            {
                _bars[i].gameObject.SetActive(false);
            }
        }

        private void StopActiveTweens()
        {
            foreach ((GameObject, Vector3, Vector3, RectTransform) inf in _defaults)
                if (LeanTween.isTweening(inf.Item1))
                    LeanTween.cancel(inf.Item1);
        }

        private void ResetToInitialValues()
        {
            foreach ((GameObject, Vector3, Vector3, RectTransform) inf in _defaults)
            {
                inf.Item4.anchoredPosition = inf.Item2;
                inf.Item1.transform.localScale = inf.Item3;
            }
        }

        #endregion

        private void OnApplicationFocus(bool focusStatus)
        {
            if (focusStatus) 
                _resumeBtn.Select();
        }

        // I put this down here so it is hidden :,)
        private void InitializeCollections()
        {
            // Not the smartest way to do things, but oh well
            _defaults = new List < (GameObject, Vector3, Vector3, RectTransform) > (10);

            _defaults.Add((
                _pausePro.gameObject, default, default, default)); // 0

            _defaults.Add((
                _resumeBtn.transform.parent.gameObject, default, default, default)); // 1
            _defaults.Add((
                _optionsBtn.transform.parent.gameObject, default, default, default)); // 2
            _defaults.Add((
                _quitBtn.transform.parent.gameObject, default, default, default)); // 3

            _defaults.Add((
                _systemNamePro.gameObject, default, default, default)); // 4
            _defaults.Add((
                _collectableHeader.gameObject, default, default, default)); // 5
            _defaults.Add((
                _collectableInfo.gameObject, default, default, default)); // 6
            _defaults.Add((
                _discoveriesHeader.gameObject, default, default, default)); // 7
            _defaults.Add((
                _discoveriesInfo.gameObject, default, default, default)); // 8

            _defaults.Add((_bars[0].gameObject,
                _bars[0].transform.localPosition, _bars[0].transform.localScale, default)); // 9

            for (int i = 0; i < _defaults.Count; i++)
            {
                GameObject go = _defaults[i].obj;
                RectTransform rect = _defaults[i].Item1.GetComponent<RectTransform>();
                _defaults[i] = (go, rect.anchoredPosition, go.transform.localScale, rect);
            }

            _buttons = new(Button, int) [3]
            {
                (_resumeBtn, 1),
                (_optionsBtn, 2),
                (_quitBtn, 3)
            };

            _infoText = new(TextMeshProUGUI, int) [4]
            {
                (_collectableHeader, 5),
                (_collectableInfo, 6),
                (_discoveriesHeader, 7),
                (_discoveriesInfo, 8)
            };
        }
    }
}