﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class MaterialSwitch : MonoBehaviour
{
    [SerializeField] private Material _switchMat;
    private Material _originalMat;
    private MeshRenderer _meshRenderer;
    /// <summary>
    /// Tracks the currently used material
    /// </summary>
    private bool _activeMat;

    private void Start() 
    {
        // Im using false for og mat, true for siwtched mat
        _activeMat = false;
        _meshRenderer = GetComponent<MeshRenderer>();
        _originalMat = _meshRenderer.material;
    }

    public void Switch()
    {
        _activeMat = !_activeMat;
        _meshRenderer.material = _activeMat ? _switchMat : _originalMat;
    }
}