using UnityEngine;

namespace Interactables
{
    /// <summary>
    /// Put this behaviour in the center of the object that will check for an interaction
    /// around it, in the player put 
    /// </summary>
    public class InteractionZoneChecker : MonoBehaviour
    {
        private Interaction _currentInteraction;
        [SerializeField] private float _interactRadius = 2f;
        [SerializeField] private float _interactRange = 2f;
        private LayerMask _mask;

        public Interaction CurrentInteractable { get; }

        [Foldout("Debug Variables", true)]
        [SerializeField] private bool _debugInteraction = false;
        [SerializeField] private Color _rayColor = Color.red;
        [SerializeField] private bool _showSpheres = false;
        [SerializeField] private Color _sphereColor = Color.blue;
        [SerializeField] private int _sphereDensity = 3;
        [Range(1f, 5f)]
        [SerializeField] private float _sphereDistsance = 1f;

        private void Start()
        {
            _mask = LayerMask.GetMask(Interaction.INTERACTION_LAYER);
        }

        private void Update()
        {
            RaycastHit hit;
            Ray ray = new Ray(transform.forward, transform.position);

            if (Physics.SphereCast(ray, _interactRadius, out hit, _interactRange, _mask))
            {
                _currentInteraction = hit.transform.GetComponent<Interaction>();
                _currentInteraction?.Selected(true);
            }
            else
            {
                _currentInteraction?.Selected(false);
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (!_debugInteraction) return;

            // Draw rays
            Gizmos.color = _rayColor;
            Gizmos.DrawRay(transform.position, transform.forward * _interactRange);

            if (_showSpheres)
            {
                Gizmos.color = _sphereColor;
                for (int i = 0; i < _sphereDensity; i++)
                {
                    i += (int)_sphereDistsance;
                    Gizmos.DrawWireSphere
                    (transform.position + transform.forward * i,
                     _interactRadius);
                }
            }
        }
    }
}