﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Interactables.Individuals
{
    public class CluckInteractable : Interaction
    {
        private static readonly string _possibleSpecials = "1234567890";
        private static readonly string _possibleHitChars =
            "ABCDEFGHIJKLMNOPQRSTUVXZ" + _possibleSpecials;
        private static readonly string _possibleIdleTalkChars =
            _possibleHitChars.ToLower() + _possibleHitChars;
        [SerializeField] private GameObject _feather = null;
        [SerializeField, Range(0f, 1f)] private float _featherChance = 0.2f;
        [SerializeField] private TMP_FontAsset _fontAsset = null;
        private Animator _anim;
        private Player.Player _player;
        private Coroutine _talkCor;
        private TextMeshPro _textObjPro;
        private bool _playing;

        private void Awake()
        {
            _player = GameObject.Find("Player").GetComponent<Player.Player>();
            if (_player == null) Debug.LogWarning("WARNING. PLAYER OBJECT NOT FOUND.");

            _anim = GetComponentInChildren<Animator>();
            if (Random.Range(0.0f, 1.0f) > _featherChance) Destroy(_feather);
            //else Debug.Log("I hab fetha.\n- Said " + name);

            _anim.speed = Random.Range(0.65f, 1.35f);

            CreateTextObject();

            if (_player != null)
                _talkCor = StartCoroutine(CIdleTalk());
        }

        private void LateUpdate()
        {
            if (_textObjPro.gameObject.activeSelf)
                TextLookAtPlayerCam();
        }

        public override void Interact(Player.Player p = null,
            Vector3 hitDirection = default)
        {
            base.Interact(p, hitDirection);

            if (p != null)
            {
                _player = p;
                _anim.speed = 1;
                _anim.SetTrigger("Hit");
                _anim.speed = Random.Range(0.65f, 1.35f);
                Talk(_possibleHitChars, 0.5f, 3, 6);
                OnHit?.Invoke();
            }
        }

        private void Talk(string possibleChars, float speedFactor = 1,
            int minWordChars = 2, int maxWordChars = 4)
        {
            if (_playing) return;
            string s = "";
            _playing = true;

            if (_talkCor != null)
            {
                StopCoroutine(_talkCor);
                _talkCor = null;
            }

            int charsAmount = Random.Range(minWordChars, maxWordChars);
            for (int i = 0; i < charsAmount; i++)
            {
                s += possibleChars[Random.Range(0, possibleChars.Length)];
                if (Random.Range(0f, 1f) < 0.3f)
                {
                    s += " ";
                    i /= 2;
                }
            }

            _textObjPro.gameObject.SetActive(true);
            _textObjPro.text = s;

            Vector3 scale = _textObjPro.gameObject.transform.localScale;
            _textObjPro.transform.localScale = Vector3.one * 0.1f;
            LeanTween.scale(_textObjPro.gameObject, scale, 0.7f)
                .setOnComplete(StartZoomOut)
                .setEaseOutCirc();

            void StartZoomOut()
            {
                LeanTween.moveLocalY(_textObjPro.gameObject,
                        _textObjPro.transform.localPosition.y + 3, 3.0f * speedFactor)
                    .setEaseOutCirc();
                LeanTween.scale(_textObjPro.gameObject,
                        Vector3.one * 0.05f, 3.0f * speedFactor)
                    .setEaseOutCirc()
                    .setOnComplete(Finish);
            }

            void Finish()
            {
                if (_talkCor == null)
                    _talkCor = StartCoroutine(CIdleTalk());
                _textObjPro.gameObject.SetActive(false);
                ResetText();
            }
        }

        private IEnumerator CIdleTalk()
        {
            byte rnd = (byte) Random.Range(5, 60);
            yield return new WaitForSeconds(rnd);
            Talk(_possibleIdleTalkChars, 1.5f);
        }

        private void CreateTextObject()
        {
            GameObject go = new GameObject(name + " Talk");
            _textObjPro = go.AddComponent<TextMeshPro>();
            _textObjPro.font = _fontAsset;
            _textObjPro.alignment = TextAlignmentOptions.Midline;
            _textObjPro.enableWordWrapping = false;
            _textObjPro.fontSize = 12;
            _textObjPro.gameObject.transform.SetParent(transform);
            ResetText();
            _textObjPro.gameObject.SetActive(false);
        }

        private void ResetText()
        {
            _textObjPro.transform.position = transform.position + (transform.up * 3);
            _textObjPro.transform.localScale = Vector3.one;
            _textObjPro.text = null;
            _playing = false;
        }

        private void TextLookAtPlayerCam()
        {
            _textObjPro.transform.LookAt(_player.CameraControls.Cameras[0].transform);
        }

        public Action OnHit;
    }
}