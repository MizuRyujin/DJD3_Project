﻿using System.Collections;
using UnityEngine;

namespace Interactables
{
    /// <summary>
    /// Class responsible for the interactable camera's behaviour
    /// </summary>
    public class InteractChangeCam : MonoBehaviour
    {
        /// <summary>
        /// Bool to verify if a camera is a dialogue camera
        /// </summary>
        [Tooltip("Check to define this camera as a dialogue camera")]
        [SerializeField] private bool _isDialogueCam = default;

        /// <summary>
        /// Time until camera change is reverted
        /// </summary>
        [Tooltip("Time that the interaction cam is active")]
        [SerializeField] private float _timeToReset = default;

        /// <summary>
        /// Reference to the player's camera
        /// </summary>
        [Tooltip("Reference to the player's camera")]
        [SerializeField] private GameObject _playerCam = default;

        /// <summary>
        /// Reference to the desired interaction camera
        /// </summary>
        [Tooltip("Reference to the desired interaction cam")]
        [SerializeField] private GameObject _vCam = default;

        /// <summary>
        /// Reference to player script to make it know it is interacting
        /// </summary>
        private Player.Player _pScript = default;

        Coroutine _runningCoroutine;

        /// <summary>
        /// Method to be called when game object is enabled
        /// </summary>
        private void ActivateInteractionCam()
        {
            if (_isDialogueCam)
            {
                _playerCam.SetActive(false);
                _vCam.SetActive(true);
                _pScript.Interacting = true;
            }
            else
            {
                _runningCoroutine = StartCoroutine(
                        ShowPlayerInteractableAction());
            }
        }

        /// <summary>
        /// Coroutine to change active vCams for a moment and switch back
        /// </summary>
        /// <returns> Time to wait until switch back </returns>
        IEnumerator ShowPlayerInteractableAction()
        {
            _playerCam.SetActive(false);
            _vCam.SetActive(true);
            _pScript.Interacting = true;
            yield return new WaitForSeconds(_timeToReset);
            _vCam.SetActive(false);
            _playerCam.SetActive(true);
            _pScript.Interacting = false;
            this.gameObject.SetActive(false);
        }

        public void ReturnToPlayer()
        {
            if (_runningCoroutine != null)
            {
                StopCoroutine(_runningCoroutine);
            }

            if (_pScript != null)
            {
                _pScript.Interacting = false;
            }
            
            _vCam.SetActive(false);
            _playerCam.SetActive(true);
            this.gameObject.SetActive(false);
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        private void OnEnable()
        {
            if (_pScript == null)
            {
                _pScript = GameObject.Find("Player").GetComponent<Player.Player>();
            }

            ActivateInteractionCam();
        }
    }
}
