﻿using System;
using Random = UnityEngine.Random;
using UnityEngine;
using UI;

namespace Interactables.Individuals
{
    [RequireComponent(typeof(Interaction))]
    public class PhotonCharge : MonoBehaviour
    {
        public const string PHOTON_TAG_NAME = "PhotonCollectable";

        private Interaction _interactionHandler;

        [SerializeField] private int _shellHitPoints = 2;
        private PhotonChargeShell _chargeShell;
        private PhotonChargeSphere _chargeSphere;
        private ParticleSystem _photonParticles;

        // Will be useful when scene changing comes int play to store the charge state
        public bool ChargeCollected { get => _chargeSphere.Collected; }

        private void Awake()
        {
            _interactionHandler = GetComponent<Interaction>();

            _chargeShell = GetComponentInChildren<PhotonChargeShell>();
            _chargeSphere = GetComponentInChildren<PhotonChargeSphere>();
            _photonParticles = GetComponentInChildren<ParticleSystem>();

            _chargeShell.Init(_shellHitPoints, this);
            _chargeSphere.Init(this);
            _interactionHandler.OnUse += OnInteracted;
        }

        private void OnEnable()
        {
            _photonParticles?.transform.SetParent(transform);
        }

        // When the player attacks it
        private void OnInteracted()
        {
            HitShell();
        }

        private void HitShell()
        {
            _chargeShell.Hit();
            _photonParticles.Emit(Random.Range(2, 5));

            OnHit?.Invoke();
        }

        public void OnShellBroken()
        {
            _photonParticles.Emit(Random.Range(10, 15));
            _chargeSphere.Activate();
        }

        // Player collects it, called from child
        public void OnPickUp()
        {
            DoCollectedParticles();
            DiscoveryArea.NewPhotonChargeFound(this);
            gameObject.SetActive(false);
        }

        private void DoCollectedParticles()
        {
            _photonParticles.Emit(Random.Range(30, 50));
            _photonParticles.transform.SetParent(null);
        }

        public Action OnHit;
    }
}