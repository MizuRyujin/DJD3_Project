﻿using Player;
using UnityEngine;
using System;

namespace Interactables.Individuals
{
    public class PhotonChargeSphere : MonoBehaviour
    {
        private PhotonCharge _parent;

        private bool _activated;

        public bool Collected { get; private set; }

        public void Init(PhotonCharge parent)
        {
            _parent = parent;
            _activated = false;
            Collected = false;
        }

        public void DestroyMe(ChargeContainer container)
        {
            Collected = true;
            container.AddNewCharge(_parent);
            _parent.gameObject.SetActive(false);
        }

        public void Activate()
        {
            _activated = true;
            GetComponent<Collider>().enabled = true;
        }

        private void CollectCharge(ChargeContainer container)
        {
            Debug.Log("Picked Up");
            Collected = true;
            container.AddNewCharge(_parent);
            _parent.OnPickUp();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!_activated || Collected) return;

            ChargeContainer container;
            if (other.TryGetComponent<ChargeContainer>(out container))
            {
                onCollect?.Invoke(true);
                CollectCharge(container);
            }
        }

        public Action<bool> onCollect;

    }
}