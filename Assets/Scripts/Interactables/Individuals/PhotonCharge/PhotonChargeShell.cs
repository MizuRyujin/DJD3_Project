﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Interactables.Individuals
{
    public class PhotonChargeShell : MonoBehaviour
    {
        private int _hp;
        private bool BreakPointReached => _hp <= 0;
        private bool _canBeHit;
        private PhotonCharge _parent;

        public void Init(int hp, PhotonCharge parent)
        {
            _hp = hp;
            _canBeHit = true;
            _parent = parent;
        }

        public void Hit()
        {
            if (!_canBeHit) return;

            _hp -= 1;

            if (BreakPointReached)
                OnBreakPointReached();
            else
                DoHitAnimation();
        }

        private void DoHitAnimation()
        {
            LeanTween.scale(gameObject, transform.localScale * 2.0f, 0.4f).setEasePunch();
        }

        private void OnBreakPointReached()
        {
            _canBeHit = false;
            DoBreakAnimation();
        }

        private void DoBreakAnimation()
        {
            LeanTween.scale(gameObject, transform.localScale * 2.5f, 0.5f).setEaseOutCirc().setOnComplete(Break);
        }

        private void Break()
        {
            gameObject.SetActive(false);
            _parent.OnShellBroken();
        }
    }
}