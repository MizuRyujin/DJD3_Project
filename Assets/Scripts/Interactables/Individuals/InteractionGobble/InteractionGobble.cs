﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Interactables.Individuals
{
    public class InteractionGobble : Interaction
    {
        [SerializeField] private Transform _armature;
        private Transform _topBone;
        private Transform _middleBone;
        private Transform _bottomBone;
        private Vector3 _initialMiddleBonePos;
        private bool _animationPlaying;

        private void Start()
        {
            _topBone = _armature.GetChild(0).GetChild(0).GetChild(0);
            _middleBone = _armature.GetChild(0).GetChild(0);
            _bottomBone = _armature.GetChild(0);
        }

        public override void Interact(Player.Player p = null, Vector3 hitDirection = default)
        {
            base.Interact(p, hitDirection);

            if (p != null && !_animationPlaying)
            {
                ToggleLock();
                _animationPlaying = true;

                // Due to float uncertainty store initial position
                _initialMiddleBonePos = _middleBone.localPosition;

                hitDirection += _middleBone.up;
                LeanTween.moveLocal(_middleBone.gameObject,
                        _middleBone.localPosition + hitDirection.normalized * 0.012f, .5f)
                    .setEaseOutElastic().setLoopPingPong(1)
                    .setOnComplete(OnAnimationEnd);

                OnHit?.Invoke();
            }
        }

        private void Update()
        {
            LookAtBottom();
        }

        private void LookAtBottom()
        {
            // Make the middle bone always look at the bottom one
            _middleBone.LookAt(_bottomBone.position + ((_bottomBone.up * 0.5f) * _bottomBone.localScale.y));
            _middleBone.up = -_middleBone.forward;
        }

        private void OnAnimationEnd()
        {
            _middleBone.localPosition = _initialMiddleBonePos;
            ToggleLock();
            Locked = false;
            _animationPlaying = false;
        }

        public Action OnHit;
    }
}