﻿using UnityEngine;
using UnityEngine.Experimental.VFX;

namespace Interactables.Individuals
{
    public class Domito : Interaction
    {
        [SerializeField] private VisualEffect _explosion = default;
        [SerializeField] private AudioSource _explosionWooshSource = default;
        private Animator _anim;
        private bool _collected;

        private void Awake()
        {
            _anim = GetComponent<Animator>();
            _collected = false;
        }

        public override void Interact(Player.Player p = null,
            Vector3 hitDirection = default)
        {
            if (_collected) return;
            _collected = true;
            base.Interact(p, hitDirection);
            _anim.SetTrigger("Collected");
        }

        // Called as an animation event
        public void PlaySound()
        {
            _explosionWooshSource.Play();
        }

        // Called as an animation event
        public void PlayExplosion()
        {
            _explosion.Play();
        }

        // Called as an animation event
        public void OnCollectedAnimationEnd()
        {
            gameObject.SetActive(false);
        }
    }
}