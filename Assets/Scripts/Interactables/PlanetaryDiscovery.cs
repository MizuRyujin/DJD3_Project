﻿using UnityEngine;
using UI;

namespace Interactables
{
    public class PlanetaryDiscovery : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)] private float _weight = 0.5f;
        [SerializeField] private bool _found = default;
        [SerializeField] private string _discoveryName = default;

        public float Weight => _weight;
        public string DiscoveryName => _discoveryName;
        public bool Found => _found;

        public void Discovered()
        {
            if (_found) return;

            // Play discovery sound
            _found = true;
            DiscoveryArea.NewDiscoveryFound(this);
            Debug.LogWarning("Discovery Found! " + name);
        }

        public void MarkDiscovered()
        {
            _found = true;
        }
    }
}