using System;
using UnityEngine;
using UnityEngine.Events;

namespace Interactables
{
    [RequireComponent(typeof(InteractionHighlighter))]
    public class Interaction : MonoBehaviour
    {
        public const string INTERACTION_LAYER = "Interactables";
        public static int InteractionLayerNum { get; private set; }
        public const float TIME_OUT = .5f;
        private InteractionHighlighter _highlighter;
        [Tooltip("Locks interaction after first use")]
        [SerializeField] private bool _lockInteraction = false;
        private float _currentTime;
        [SerializeField] private bool _locked;

        public event Action OnUse;
        public UnityEvent OnUseUnity;

        private PlanetaryDiscovery _discoveryScript;

        public bool Locked
        {
            get => _locked;
            set
            {
                _locked = value;
            }
        }

        private void Awake()
        {
            if (InteractionLayerNum == default)
                InteractionLayerNum = LayerMask.NameToLayer(INTERACTION_LAYER);
            _highlighter = GetComponent<InteractionHighlighter>();
            _discoveryScript = GetComponent<PlanetaryDiscovery>();
            _currentTime = 0;
            AddToInteractionLayer();
        }

        private void Update()
        {
            if (_currentTime < TIME_OUT)
            {
                _currentTime += Time.deltaTime;
            }
            else if (_currentTime >= TIME_OUT)
                _locked = false;
        }

        protected void AddToInteractionLayer()
        {
            gameObject.layer = LayerMask.NameToLayer(INTERACTION_LAYER);
        }

        public virtual void Interact(Player.Player p = null, Vector3 hitDirection = default)
        {
            if (_locked) return;
            OnUse?.Invoke();
            OnUseUnity?.Invoke();
            _discoveryScript?.Discovered();
            
            if (_lockInteraction)
            {
                _locked = true;
                return;
            }

            _locked = true;
            _currentTime = 0.0f;
        }

        public void Selected(bool active)
        {
            _highlighter.HighLight(active);
        }

        public void ToggleLock()
        {
            _lockInteraction = !_lockInteraction;
        }
    }
}