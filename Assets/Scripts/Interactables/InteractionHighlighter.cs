using UnityEngine;

namespace Interactables
{
    public class InteractionHighlighter : MonoBehaviour
    {
        [ColorUsage(true, true)]
        [SerializeField] private Color _selectedColor;
        [SerializeField] private InteractableThickness thickness;
        private Renderer _objRenderer;
        private bool _usable;

        private void Awake()
        {
            _objRenderer = GetComponent<Renderer>();
            if (_objRenderer == null) _objRenderer = GetComponentInChildren<Renderer>();
            
            _objRenderer.material.SetFloat("_OutlineFactor", 1f);
            _objRenderer.material.SetColor("_OutlineColor", _selectedColor);
        }

        public void HighLight(bool active)
        {
            float factor = active ? thickness.ThicknessValue : 1;
            _objRenderer.material.SetFloat("_OutlineFactor", factor);
        }
    }
}