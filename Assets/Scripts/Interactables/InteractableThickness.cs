using UnityEngine;

namespace Interactables
{
    [CreateAssetMenu(menuName = "NSOY/InteractionThickness")]
    public class InteractableThickness : ScriptableObject
    {
        [SerializeField] private float globalThickness;

        public float ThicknessValue => globalThickness;
    }
}