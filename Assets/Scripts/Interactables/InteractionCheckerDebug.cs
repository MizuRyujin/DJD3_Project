using UnityEngine;

namespace Interactables
{
    /// <summary>
    /// Debugger behaviour, put it on the camera to check with the mouse if an
    /// interaction is working.
    /// </summary>
    public class InteractionCheckerDebug : MonoBehaviour 
    {
        private Camera eventCamera;
        private Interaction interaction;
        private DebugMouseControls mouseControls;
        private Vector2 mousePosition;

        private void OnEnable() {
            mouseControls.Enable();
        }

        private void OnDisable() {
            mouseControls.Disable();
        }

        private void Awake() 
        {
            mouseControls = new DebugMouseControls();
            mouseControls.action.MousePosition.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
            mouseControls.action.MouseClick.performed += ctx => InteractWithItem();
            eventCamera = GetComponent<Camera>();
        }

        private void Update() 
        {
            RaycastHit hit;
            Ray ray = eventCamera.ScreenPointToRay(mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                interaction = hit.transform.GetComponent<Interaction>();

                interaction?.Selected(true);
                if (Input.GetMouseButtonDown(0))
                {
                    interaction.Interact();
                }
            }
            else
            {
                interaction?.Selected(false);
            }
        }

        private void InteractWithItem()
        {
            interaction?.Interact();
            interaction = null;
        }
    }
}