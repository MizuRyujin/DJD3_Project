﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayPostProcessSound : MonoBehaviour
{
    [Tooltip("Where the post process sound clip is added")]
    [SerializeField] private AudioClip _postProcessSFX = default;
    [SerializeField] private float _timeToStart = default;
    private AudioSource _thisAudioSource;



    // Start is called before the first frame update
    private void Awake()
    {
        _thisAudioSource = GetComponent<AudioSource>();
        _thisAudioSource.clip = _postProcessSFX;
        _thisAudioSource.PlayDelayed(_timeToStart);
    }
}
