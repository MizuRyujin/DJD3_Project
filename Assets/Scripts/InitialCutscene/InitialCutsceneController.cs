﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class InitialCutsceneController : MonoBehaviour
{
    private const float INTRO_DURATION = 58.5F;
    [SerializeField] private float _startDelay = default;
    [SerializeField] private AudioSource _narratorSource = default;
    [Tooltip("Already taking into account the Start Delay time.")]
    [SerializeField] private float _okapiLogoDelay = default;
    [SerializeField] private GameObject _okapiLogo = null;
    [Tooltip("Already taking into account the okapi logo spawn time.")]
    [SerializeField] private float _teamLogoDelay = default;
    [SerializeField] private GameObject _teamLogo = null;
    [Tooltip("Already taking into account the Team logo spawn time.")]
    [SerializeField] private float _gameLogoDelay = default;
    [SerializeField] private GameObject _gameLogo = null;

    [SerializeField] private TextMeshProUGUI _subtitlesPro = default;


    [Header("Subtitles")]
    [TextArea]
    [SerializeField] private string[] _subtitles = default;
    [SerializeField] private float[] _delays = default;

    private void Start()
    {
        StartCoroutine(CInitialCutscene());
        StartCoroutine(CLoadNextScene());

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private IEnumerator CInitialCutscene()
    {
        yield return new WaitForSeconds(_startDelay);
        _narratorSource.Play();
        StartCoroutine(CStartSubtitles());
        yield return new WaitForSeconds(_okapiLogoDelay);
        _okapiLogo.SetActive(true);
        yield return new WaitForSeconds(_teamLogoDelay);
        Destroy(_okapiLogo);
        _teamLogo.SetActive(true);
        yield return new WaitForSeconds(_gameLogoDelay);
        Destroy(_teamLogo);
        _gameLogo.SetActive(true);
    }

    private IEnumerator CStartSubtitles()
    {
        for (int i = 0; i < _subtitles.Length; i++)
        {
            _subtitlesPro.SetText(_subtitles[i]);
            yield return new WaitForSeconds(i > 0 ? _delays[i] - _delays[i-1] : _delays[i]);
        }
    }
    
    private IEnumerator CLoadNextScene()
    {
        yield return new WaitForSeconds(INTRO_DURATION);
        SceneLoader.Load("Main Menu");
    }
}