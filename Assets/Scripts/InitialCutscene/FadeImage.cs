﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;


namespace InitialCutscene
{
    public class FadeImage : MonoBehaviour
    {
        [Tooltip("False is fade out")]
        [SerializeField] private bool _fadeIn = false;
        [SerializeField] private float _delayBeforeFade = default;
        [SerializeField] private float _fadeSpeed = 1;
        private Image _whiteImg;
        private void Awake()
        {
            _whiteImg = GetComponent<Image>();
        }

        private void Start()
        {
            StartCoroutine(CWaitBeforeFade());
        }

        private void Update()
        {
            UpdateAction?.Invoke();
        }

        private IEnumerator CWaitBeforeFade()
        {
            yield return new WaitForSeconds(_delayBeforeFade);
            if (_fadeIn)
            {
                Color transparentWhite = Color.white;
                transparentWhite.a = 0;
                SetColor(transparentWhite);
                UpdateAction = FadeIn;
            }
            else
            {
                _whiteImg.color = Color.white;
                UpdateAction = FadeOut;
            }
        }

        private void FadeIn()
        {
            Color current = _whiteImg.color;
            float a = current.a;
            a = Mathf.Clamp01(a + Time.deltaTime * _fadeSpeed);
            current.a = a;
            SetColor(current);
            if (a == 1) UpdateAction = null;
        }

        private void FadeOut()
        {
            Color current = _whiteImg.color;
            float a = current.a;
            a = Mathf.Clamp01(a - Time.deltaTime * _fadeSpeed);
            current.a = a;
            SetColor(current);
            if (a == 0) UpdateAction = null;
        }

        private void SetColor(Color c)
        {
            _whiteImg.color = c;
        }

        private Action UpdateAction;
    }
}