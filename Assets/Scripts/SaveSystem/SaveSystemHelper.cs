using UnityEngine;
using SaveSystem.Serialization;
using System.IO;

namespace SaveSystem
{
    public class SaveSystemHelper : MonoBehaviour
    {
        public void LoadSaveFile()
        {
            SaveData.Current = (SaveData)Serializer.Load
                (Application.persistentDataPath +
                Serializer.SAVE_FOLDER +
                "/Profile1.sprm");

            Debug.Log($"Loaded : {SaveData.Current.LevelDiscoveries.Count} discoveries");
        }

        public void SaveGame()
        {
            Serializer.Save("Profile1", SaveData.Current);
        }

        public void DeleteSave()
        {
            string path = Application.persistentDataPath + 
                Serializer.SAVE_FOLDER + 
                "/Profile1.sprm";
            
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}