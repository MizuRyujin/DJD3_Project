﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveSystem
{
    [System.Serializable]
    public class PlanetaryDiscoveryData
    {
        /// <summary>
        /// ID of the object in scene to which the data refers to
        /// </summary>
        /// <value> generated string ID</value>
        public int ID;
        public bool Found;
        public PlanetaryDiscoveryData(int id)
        {
            ID = id;
        }
    }
}
