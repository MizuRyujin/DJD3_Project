﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interactables;

namespace SaveSystem
{
    public class PlanetaryDiscoveryDataHandler : MonoBehaviour
    {
        /// <summary>
        /// This must never be equal to any other instance
        /// </summary>
        [Tooltip("Make this different for every planetary discovery handler in the scene")]
        [SerializeField] private int _id;
        [Tooltip("Destroys the gameobject if it has been found on load")]
        [SerializeField] private bool _destroyIfDiscovered = false;
        private PlanetaryDiscoveryData _planetaryDiscovery = null;
        private PlanetaryDiscovery _discovery = null;
        // Start is called before the first frame update

        private void Awake() 
        {
            _discovery = GetComponent<PlanetaryDiscovery>();    
        }

        private void Start()
        {
            for (int i = 0; i < SaveData.Current.LevelDiscoveries.Count; i++)
            {
                if (SaveData.Current.LevelDiscoveries[i].ID == _id)
                {
                    _planetaryDiscovery = SaveData.Current.LevelDiscoveries[i];
                    if (_planetaryDiscovery.Found)
                    {
                        _discovery.MarkDiscovered();
                        if (_destroyIfDiscovered)
                        {
                            Destroy(gameObject);
                        }
                    }
                    Debug.Log("Discovery Loaded" + _id);
                }
            }

            if (_planetaryDiscovery == null)
            {
                _planetaryDiscovery = new PlanetaryDiscoveryData(_id);
            }
        }

        public void UpdateData(bool value)
        {
            _planetaryDiscovery.Found = value;
            if (!SaveData.Current.LevelDiscoveries.Contains(_planetaryDiscovery))
            {
                SaveData.Current.LevelDiscoveries.Add(_planetaryDiscovery);
            }
        }
    }
}