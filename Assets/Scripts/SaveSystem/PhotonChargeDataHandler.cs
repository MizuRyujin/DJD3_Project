﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interactables.Individuals;
using SaveSystem;
using Player;

public class PhotonChargeDataHandler : MonoBehaviour
{
    [SerializeField] private int _id;
    private PhotonChargeData _data = null;
    private PhotonChargeSphere _chargeSphere;

    private void Awake()
    {
        _chargeSphere = GetComponent<PhotonChargeSphere>();
    }

    private void OnEnable() 
    {
        _chargeSphere.onCollect += UpdateData;    
    }

    private void OnDisable() 
    {
        _chargeSphere.onCollect -= UpdateData;
    }

    private void Start()
    {
        for (int i = 0; i < SaveData.Current.PhotonCharges.Count; i++)
        {
            if (SaveData.Current.PhotonCharges[i].ID == _id)
            {
                _data = SaveData.Current.PhotonCharges[i];
                if (_data.PickedUp)
                {
                    // Get container somehow and add the photon charge
                    ChargeContainer container = GameObject.FindObjectOfType<ChargeContainer>();
                    _chargeSphere.DestroyMe(container);
                }
                Debug.Log("Charge Loaded" + _id);
            }
        }

        if (_data == null)
        {
            _data = new PhotonChargeData(_id);
        }
    }

    public void UpdateData(bool value)
    {
        _data.PickedUp = value;
        if (!SaveData.Current.PhotonCharges.Contains(_data))
        {
            SaveData.Current.PhotonCharges.Add(_data);
        }
    }
}
