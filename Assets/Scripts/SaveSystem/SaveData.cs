﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SaveSystem
{
    [System.Serializable]
    public class SaveData
    {
        private static SaveData _current;
        public static SaveData Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new SaveData();
                    _current.LevelDiscoveries = new List<PlanetaryDiscoveryData>();
                    _current.PhotonCharges = new List<PhotonChargeData>();
                }
                return _current;
            }

            set
            {
                _current = value;
            }
        }

        public List<PlanetaryDiscoveryData> LevelDiscoveries;
        public List<PhotonChargeData> PhotonCharges;
        public PlayerData PlayerData {get; set;}
    }
}
