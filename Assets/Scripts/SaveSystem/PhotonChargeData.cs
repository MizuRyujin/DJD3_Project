﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PhotonChargeData
{
    /// <summary>
    /// ID of the object in scene to which the data refers to
    /// </summary>
    /// <value> generated string ID</value>
    public int ID;
    public bool PickedUp;
    public PhotonChargeData(int id)
    {
        ID = id;
    }
}
