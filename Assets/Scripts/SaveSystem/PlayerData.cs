﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveSystem
{
    [System.Serializable]
    public class PlayerData
    {
        /// <summary>
        /// Id is used if other objects have a reference to this one, so we can
        /// re-do de scene references
        /// </summary>
        /// <value> String with the id of the object</value>
        public string Id {get; set;}
        public string ProfileName{get; set;}

        // Transform variables
        public Vector3 Position{get; set;}
        public Quaternion Rotation{get; set;}
    }
}