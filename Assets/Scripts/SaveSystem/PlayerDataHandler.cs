﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveSystem
{
    public class PlayerDataHandler : MonoBehaviour
    {
        public PlayerData PlayerSaveData;

        private void Start() 
        {
            if (string.IsNullOrEmpty(PlayerSaveData.Id))
            {
                // Generate a new ID for the player
                PlayerSaveData.Id = System.DateTime.Now.ToLongDateString() + 
                    System.DateTime.Now.ToLongTimeString() + 
                    Random.Range(0, int.MaxValue).ToString();
                
            }
            SaveData.Current.PlayerData = PlayerSaveData;
        }

        private void Update() 
        {
            PlayerSaveData.Rotation = transform.rotation;
            PlayerSaveData.Position = transform.position;    
        }
    }
}
