﻿using UnityEngine;

public class LoadSceneHelper : MonoBehaviour
{
    [SerializeField] private string sceneName;

    public void Load()
    {
        SceneLoader.Load(sceneName);
    }
}