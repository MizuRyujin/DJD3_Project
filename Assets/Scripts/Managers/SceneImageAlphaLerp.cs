﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SceneImageAlphaLerp : MonoBehaviour
{
    private Image _img;
    private Color _initialColor;
    // Start is called before the first frame update
    private void Awake() 
    {
        _img = GetComponent<Image>();
    }

    public void FadeIn(float speed)
    {
        StartCoroutine(LerpColor(new Color(0, 0, 0, 0), Color.white, speed));
    }

    public void FadeOut(float speed)
    {
        StartCoroutine(LerpColor(Color.white, new Color(0, 0, 0, 0), speed));
    }

    private IEnumerator LerpColor(Color start, Color finish, float speed)
    {
        Color finalColor = start;
        while(finalColor != finish)
        {
            finalColor = Color.Lerp(finalColor, finish, Time.deltaTime * speed);
            _img.color = finalColor;
            yield return null;
        }

        //yield return LerpColor(finalColor, new Color(0f, 0f, 0f, 0f));
    }
}
