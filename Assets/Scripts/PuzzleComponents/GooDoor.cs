using UnityEngine;
using System.Collections;

namespace PuzzleComponents
{
    public class GooDoor : Door
    {
        [Header("GooDoor specific variables")]
        
        [Tooltip("Time to wait before opening a door, can be zero")]
        [SerializeField] private float _timeBeforeOpen = default;

        [Tooltip("Speed which the door opens, can't be less than 1")]
        [SerializeField] private float _openCloseSpeed = 4f;

        private Material _mat;
        private bool _isLerping;
        protected override void Start() 
        {
            base.Start();
            _mat = _theThingThatOpens.GetComponent<Renderer>().material;
        }

        public override void ToggleDoor()
        {
            if (_isLerping) return;

            float currentTime;
            if (_doorCollider.enabled)
            {
                currentTime = 1;
                StartCoroutine(LerpMat(1, 0, _openCloseSpeed));
            }
            else
            {
                currentTime = 0;
                StartCoroutine(LerpMat(0, 1, _openCloseSpeed));
            }
            
            IEnumerator LerpMat(float startValue, float endValue, float speed)
            {
                yield return new WaitForSeconds(_timeBeforeOpen);

                _isLerping = true;
                while(CheckValues(currentTime, startValue, endValue))
                {
                    if (startValue > endValue)
                        currentTime -= Time.deltaTime;
                    else
                        currentTime += Time.deltaTime;

                    _mat.SetFloat("_Cutout", currentTime * speed);

                    yield return null;
                }
                base.ToggleDoor();
                _isLerping = false;
            }

            bool CheckValues(float current, float start, float final)
            {
                if (start > final)
                {
                    return current >= final;
                }
                else
                    return current <= final;
            }
        }
    }
}