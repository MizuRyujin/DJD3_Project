﻿using UnityEngine;

/// <summary>
/// Takes care of updating plataform movement
/// with available plataform movement scripts
/// </summary>
[RequireComponent(typeof(PlataformPhysics))]
public class Plataform : MonoBehaviour
{
    private PlataformMovement[] _movements;

    public bool MovementAvailable {get; private set;}
    private void Awake() 
    {
        _movements = GetComponents<PlataformMovement>();
        MovementAvailable = _movements != null;
    }

    // Update is called once per frame
    void Update()
    {
        if (!MovementAvailable) return;
        for(int i = 0; i < _movements.Length; i++)
        {
            _movements[i].Move();
        }
    }
}
