﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the physics of objects on top of moving platforms
/// Collider must be in trigger mode
/// </summary>
[RequireComponent(typeof(Collider))]
public class PlataformPhysics : MonoBehaviour
{
    private Transform _target;
    private Vector3 _offset;
    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    private void Start() 
    {
        // Initializes the target just so we don't have any weird behaviors
        _target = null;
    }

    private void OnTriggerStay(Collider other) 
    {
        // Calculates the offset of the target in the plataform
        // ie when the player jumps it creates an offset or when he moves around, etc...
        _target = other.transform;
        _offset = _target.position - transform.position;
    }

    private void OnTriggerExit(Collider other) 
    {
        // Remove target
        _target = null;
    }

    // We use late update so it's done after all the speed/movement calculations.
    private void LateUpdate() 
    {
        if (!_target) return;
        _target.position = transform.position + _offset;
    }
}
