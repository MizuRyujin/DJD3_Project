﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformCourse : MonoBehaviour
{
    [SerializeField] private CourseSavePoint[] _savePoints;
    [SerializeField] private TriggerEvent[] _resetColliders;
    private bool _isCourseActive;

    private void Start() 
    {
        for (int i = 0; i < _savePoints.Length; i++)
        {
            // Give the save points an id, order, maybe a government? something to follow.
            _savePoints[i].ID = i;
        }
    }

    /// <summary>
    /// Receives the ID of the calling Save point and enables it as the active Save point
    /// </summary>
    public void EnableSavePoint(int savePointID)
    {
        // If no save point has been reached before, then activate the save points
        if (!_isCourseActive) _isCourseActive = true;
        _savePoints[savePointID].EnablePoint();
        SetupTriggers(_savePoints[savePointID]);
        for (int i = 0; i < _savePoints.Length; i++)
        {
            if (i != savePointID)
                _savePoints[i].DisablePoint();
        }
    }

    private void SetupTriggers(CourseSavePoint point)
    {
        for (int i = 0; i < _resetColliders.Length; i++)
        {
            // Override other actions and add the new point as the reset point
            _resetColliders[i].TriggerAction = point.ResetPlayer;
        }
    }
}
