﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformVerticalMovement : PlataformMovement
{
    [SerializeField] private float _speed;
    [SerializeField] private float _distance;

    /// <summary>
    /// The start position of the platform
    /// </summary>
    private Vector3 _startPosition;

    /// <summary>
    /// The end position of the platform
    /// </summary>
    private Vector3 _endPosition;
    // Saves us an if statement
    private Vector3 _targetPosition;

#if UNITY_EDITOR
    private Vector3 _translation;
#endif
    private void Start() 
    {
        _startPosition = transform.localPosition;    
        _endPosition = _startPosition;
        _endPosition += Vector3.up * _distance;
        _targetPosition = _startPosition;
    }

    public override void Move()
    {
        // If it's on any of the endpoints, wait a little
        if (Vector3.Distance(transform.localPosition, _targetPosition) < 0.2f)
        {
            Stop();
            _targetPosition = _targetPosition.Equals(_startPosition) ? _endPosition : _startPosition;
            return;
        }
        // Updates the position of the plataform
        if (!_stop)
        {
            transform.localPosition = Vector3.MoveTowards
                (transform.localPosition, _targetPosition, Time.deltaTime * _speed);
#if UNITY_EDITOR
            _translation = transform.localPosition;
#endif
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos() 
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.green;
        Gizmos.DrawLine(Vector3.zero - _translation, Vector3.zero + (Vector3.up * _distance) - _translation);
        
        Color c = Gizmos.color;
        c.a = 0.5f;
        Gizmos.color = c;
        Gizmos.DrawCube(Vector3.zero + 
            (Vector3.up * _distance) - _translation
            , new Vector3(1f, 2f, 1f));
        
        c = Color.red;
        c.a = 0.5f;
        Gizmos.color = c;
        Gizmos.DrawCube(Vector3.zero - _translation, new Vector3(1f, 2f, 1f));
    }
#endif
}