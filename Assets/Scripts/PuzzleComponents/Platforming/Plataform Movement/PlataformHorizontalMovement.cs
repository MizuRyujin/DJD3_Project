﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlataformHorizontalMovement : PlataformMovement
{
    [SerializeField] private float _speed;
    [SerializeField] private float _distance;
    [SerializeField] private Transform _planet;

    private float _angle;
    private float _currentAngle;

    private void Start()
    {
        _angle = (_distance / Vector3.Distance(transform.position, _planet.position)) * Mathf.Rad2Deg;
        _currentAngle = 0;
    }

    public override void Move()
    {
        if (!_planet || _stop) return;

        // Reset plataform movement, this means it has reached one of the limits
        if ( Mathf.Abs(_currentAngle) >= _angle)
        {
            Stop();
            _currentAngle = 0;
            _speed = -_speed;
        }

        _currentAngle += Time.deltaTime * _speed;
        transform.RotateAround(_planet.position, transform.right, Time.deltaTime * _speed);
    }

    private void OnDrawGizmos() 
    {
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.position + transform.forward * _distance);
    }
}
