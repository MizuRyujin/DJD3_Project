﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformRotateAround : PlataformMovement
{
    [SerializeField] private Transform _rotateTarget;
    [SerializeField] private float _speed;
    
    /// <summary>
    /// This variable is a collection of angles for the plataform to stop at
    /// </summary>
    [Tooltip("An array with angles in degrees on where the plataform should stop" +
        "for a while")]
    [SerializeField] private int[] _anglesToStopAt;

    // Angle of the movement
    private float _currentAngle;
    private List<int> _stopedAtAngles;

    private void Awake() 
    {
        _stopedAtAngles = new List<int>(_anglesToStopAt.Length);
        _stopedAtAngles.Clear();   
    }

    public override void Move()
    {
        if (!_rotateTarget || _stop) return;

        // Checks if it has passed a stop angle, if so then stop
        // for the designated amount of time
        if (CheckAngles(_currentAngle))
        {
            Stop();
        }

        _currentAngle += Time.deltaTime * _speed;

        // Update position
        transform.RotateAround(_rotateTarget.position, 
            _rotateTarget.up, Time.deltaTime * _speed);

        // Resets the movement completing 1 rotation
        if (_currentAngle >= 360)
        {
            _currentAngle = 0.0f;
            _stopedAtAngles.Clear();
        }
    }

    private bool CheckAngles(float angle)
    {
        for (int i = 0; i < _anglesToStopAt.Length; i++)
        {
            // Checks if the angle is larger or equal to one of the angles
            // in the stop angles array, and if the angle was already passed.
            if (angle >= _anglesToStopAt[i] && 
                !_stopedAtAngles.Contains(i))
            {
                // Save the stop angle in the passed angles list.
                _stopedAtAngles.Add(i);
                return true;
            }
        }

        return false;
    }
}
