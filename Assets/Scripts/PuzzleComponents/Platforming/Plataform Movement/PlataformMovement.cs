﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Parent class for plataform types of movement
/// </summary>
public class PlataformMovement : MonoBehaviour, IActionContainer<float>
{
    [SerializeField] private float _stopTime;
    /// <summary>
    /// Called when a plataform stops and resumes the movement, so you need
    /// some kind of toggle-able method
    /// </summary>
    [Tooltip("Make sure the event in here is toggle-able, check code for more info")]
    [SerializeField] private UnityEngine.Events.UnityEvent _onStop;
    // Used to know when to start the timer
    protected bool _stop;
    private float _timer;
    private bool _demandedStop;
    public virtual void Move()
    {

    }

    // Update only used to update the timer inside
    protected virtual void Update() 
    {
        if (_timer > 0.0f)
        {
            _timer -= Time.deltaTime;
        }
        else if (_demandedStop)
        {
            _stop = false;
            _demandedStop = false;
            _onStop?.Invoke();
        }
        else
        {
            _stop = false;
        }
    }

    public void Stop()
    {
        _stop = true;
        _timer = _stopTime;
    }

    /// <summary>
    /// Stops the plataform for a certain time
    /// </summary>
    /// <param name="time"> time to stop</param>
    public void Stop(float time)
    {
        _stop = true;
        _timer = time;
    }

    public void Invoke(float t)
    {
        _demandedStop = true;
        _onStop?.Invoke();
        Stop(t);
    }
}