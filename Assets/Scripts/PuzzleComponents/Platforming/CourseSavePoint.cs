﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Saves a plataform
/// </summary>
public class CourseSavePoint : MonoBehaviour
{
    [SerializeField] private Transform _spawn;
    [SerializeField] private PlataformCourse _course;
    [SerializeField] private UnityEngine.Events.UnityEvent _onPlayerEnter;
    private Collider _checkCollider;
    private Transform _player;
    
    public int ID {get; set;}

    [Header("Visual varialbes !Move to other class later!")]
    //! This variables are used for visual part of it, supposed to be moved later to
    //! a specific class
    /// <summary>
    /// Ball of the funky looking lollipop on the plataform
    /// </summary>
    [SerializeField]private MeshRenderer _savePoleBall;
    private Material _ballMat;

    private void Awake() 
    {
        _checkCollider = GetComponent<Collider>();   
        _ballMat = _savePoleBall.material;
        _ballMat.DisableKeyword("_EMISSION");    
    }

    public void ResetPlayer()
    {
        _player.position = _spawn.position;
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.CompareTag("Player"))
        {
            _player = other.transform;
            _onPlayerEnter?.Invoke();
            _course.EnableSavePoint(ID);
        }
    }

    /// <summary>
    /// Add other visual flare for when the point is enabled here
    /// </summary>
    public void EnablePoint()
    {
        _ballMat.EnableKeyword("_EMISSION");
    }

    /// <summary>
    /// Add stuff you want to happen when this point is disabled
    /// </summary>
    public void DisablePoint()
    {
        _ballMat.DisableKeyword("_EMISSION");
        _player = null;
    }
}
