﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PuzzleComponents
{
    public class Door : MonoBehaviour
    {
        [SerializeField] protected Transform _theThingThatOpens;
        protected Collider _doorCollider;
        public Action onDoorOpen;
        public Action onDoorClose;

        protected virtual void Start()
        {
            _doorCollider = _theThingThatOpens.GetComponent<Collider>();
        }

        public virtual void ToggleDoor()
        {
            Debug.Log("Door Opened: " + _doorCollider.enabled);
            _doorCollider.enabled = !_doorCollider.enabled;

            if (_doorCollider.enabled)
                onDoorClose?.Invoke();
            else
                onDoorOpen?.Invoke();
        }
    }
}
