﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class TriggerEvent : MonoBehaviour
{
    public Action TriggerAction;
    [Tooltip("Tag of the object to detect")]
    [SerializeField] private string _tag;

    private void OnTriggerEnter(Collider other) 
    {
        if (other.CompareTag(_tag))
            TriggerAction?.Invoke();
    }
}
