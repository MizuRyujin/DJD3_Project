﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject _spawnObject;
    [SerializeField] private bool _manualSpawn;
    [SerializeField] private float _spawnInterval;
    private Transform _spawnPoint;
    private WaitForSeconds _spawnWaitTime;

    private void Start() 
    {
        _spawnWaitTime = new WaitForSeconds(_spawnInterval);
        _spawnPoint = transform.GetChild(0);

        // If the spawn isnt manual then start the loop
        if (!_manualSpawn) StartCoroutine(SpawnLoop());
    }

    public void Spawn()
    {
        if (_spawnPoint.childCount > 0) return;

        _spawnPoint.localScale = Vector3.one * 0.01f;
        Instantiate(_spawnObject, _spawnPoint);
        _spawnPoint.LeanScale(Vector3.one, 0.9f).setEaseOutBack();

    }

    private IEnumerator SpawnLoop()
    {
        while(true)
        {
            yield return _spawnWaitTime;
            Spawn();
        }
    }
}
