﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines that the object has an interaction event to be called on trigger
/// </summary>
public interface IActionContainer<T>
{
    void Invoke(T type);
}
