﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Triggers an event from another object on collision
/// </summary>
[RequireComponent(typeof(Collider))]
public class MovingTrigger : MonoBehaviour
{
    public UnityEvent OnTrigger;
    public bool Active {get; set;}
    private void OnTriggerEnter(Collider other)
    {
        if (!Active) return;
        IActionContainer<float> container;
        if (other.TryGetComponent<IActionContainer<float>>(out container))
        {
            container.Invoke(5f);
            OnTrigger?.Invoke();
        }
    }
}
