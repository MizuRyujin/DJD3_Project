﻿using UnityEngine;

namespace Player
{
    [CreateAssetMenu(menuName = "Player/Values")]
    public class PlayerValues : ScriptableObject
    {
        [Tooltip("Player max hit points value")]
        [SerializeField] private byte _maxHP = 3;

        //* General Movement
        [SerializeField] private GameObject _tailHitPrefab = null;
        [Tooltip("Gravity force to be applied to the player")]
        [SerializeField] private float _gravity = 50.0f;
        [Tooltip("Max stamina that the player has")]
        [SerializeField] private float _maxStamina = 100.0f;
        [Tooltip("Player's max space flight velocity")]
        [SerializeField] private float _maxOutPlanetVelocity = 2.0f;
        [Tooltip("Player's max self rotation speed")]
        [SerializeField] private float _maxRotationSpeed = 10.0f;
        [Tooltip("Player's stamina recharge rate")]
        [SerializeField] private float _staminaRechargeRate = 10.0f;
        [Tooltip("Player's stamina usage rate")]
        [SerializeField] private float _staminaUseRate = 10.0f;

        //* OnPlanetMovement
        [Tooltip("Player's movement speed value")]
        [SerializeField] private float _speed = 15.0f;
        [Tooltip("Player's rotational speed value")]
        [SerializeField] private float _rotateSpeed = 5.0f;
        [Tooltip("Player's jump height value")]
        [SerializeField] private float _jumpHeight = 1.0f;
        [Tooltip("Player's jump force value")]
        [SerializeField] private float _jumpForce = 1000.0f;

        //* OutPlanetMovement
        [Tooltip("Player's space movement vector")]
        [SerializeField] private Vector3 _spaceMoveVector = new Vector3(2f, 2f, 2f);

        public float MaxStamina => _maxStamina;
        public float Speed => _speed;
        public float RotateSpeed => _rotateSpeed;
        public float JumpHeight => _jumpHeight;
        public Vector3 SpaceMoveVector => _spaceMoveVector;
        public float Gravity => _gravity;
        public byte MaxHP => _maxHP;
        public float MaxRotationSpeed => _maxRotationSpeed;
        public float MaxOutPlanetVelocity => _maxOutPlanetVelocity;
        public float StaminaRechargeRate => _staminaRechargeRate;
        public float StaminaUseRate => _staminaUseRate;
        public GameObject TailHitPrefab => _tailHitPrefab;
        public float JumpForce => _jumpForce;
    }
}