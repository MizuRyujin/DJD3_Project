﻿using System.Collections;
using Galaxy.CelestialObjects;
using Player.Movement.Behaviours;
using UnityEngine;

namespace Player.Movement
{
    public class PlayerMovement : MonoBehaviour, IFovValue
    {
        //* Movement Variables
        //! TEMP BOOL FOR PROTOTYPE ONLY!!!!
        public static bool staticBossFight = false;
        [SerializeField] private bool _bossFight = default;

        /// <summary>
        /// Reference to the feet gameObject transform, 
        /// used in the movement strategy pattern
        /// </summary>
        [Tooltip("Child game object that act as the player's feet to check" +
            " if is on ground")]
        [SerializeField] private Transform _feet = default;

        /// <summary>
        /// Layer mask used on the DetectPlanet() to check if the player 
        /// is in range of a planet
        /// </summary>
        [Tooltip("Layer to be used to check if the player as reached a planet")]
        [SerializeField] private LayerMask _planetLayermask = default;

        /// <summary>
        /// Reference to the player's Rigidbody component
        /// </summary>
        private Rigidbody _rb;

        /// <summary>
        /// Reference to the sphere collider component
        /// </summary>
        private SphereCollider _selfCollider;

        /// <summary>
        /// Variable used to store the current 
        /// type of movement being used by the player, main part of the movement 
        /// strategy pattern
        /// </summary>
        private MovementBehaviour _moveBehaviour;

        /// <summary>
        /// Auxiliary variable to inter-change the current stamina values between
        /// movement behaviours
        /// </summary>
        private float _auxStamina;

        private CelestialObject _currentCelestial;

        /// <summary>
        /// Game object that is created when the player goes to space flight, 
        /// to be used as a reference transform position in case player loses 
        /// all stamina
        /// </summary>
        private GameObject _lastPlanetPosition;

        /// <summary>
        /// Variable to store the space roll axis values, gotten by the new 
        /// input system
        /// </summary>
        private float _spaceRoll;

        //* Movement strategy scripts
        /// <summary>
        /// Reference to the on planet movement behaviour, part of the movement 
        /// strategy pattern
        /// </summary>
        private MovementBehaviour _onPlanet;
        /// <summary>
        /// Reference to the space movement behaviour, part of the movement 
        /// strategy pattern
        /// </summary>
        private MovementBehaviour _outPlanet;
        /// <summary>
        /// Reference to the boss movement behaviour, part of the movement 
        /// strategy pattern
        /// </summary>
        private MovementBehaviour _bossMovement;
        /// <summary>
        /// Reference to the attack behaviour, part of the movement strategy 
        /// pattern
        /// </summary>
        private MovementBehaviour _attackBehaviour;

        //* Class properties
        /// <summary>
        /// Bool to check if the player is attacking
        /// </summary>
        /// <value> True when attack movement is active </value>
        public bool Attacking { get; private set; }

        /// <summary>
        /// Property to have the current planet the player is on
        /// </summary>
        public CelestialObject CurrentCelestial => _currentCelestial;

        /// <summary>
        /// Value to lerp with velocity while flying in space
        /// </summary>
        public float LerpValue => MoveBehaviour.CurrentMovement.z / Values.MaxOutPlanetVelocity;

        /// <summary>
        /// Property to access the reference to the player's Rigidbody
        /// </summary>
        public Rigidbody Rigidbody => _rb;

        /// <summary>
        /// Property to access the reference to the player's feet 
        /// child gameObject
        /// </summary>
        public Transform Feet => _feet;

        /// <summary>
        /// Property to access the reference to the current movement behaviour
        /// </summary>
        public MovementBehaviour MoveBehaviour => _moveBehaviour;

        /// <summary>
        /// Reference to the current planet the player is on, 
        /// used in the gravity system of the movement strategy pattern
        /// </summary>
        [Tooltip("Current planet the player is on, reference gotten on Awake()." +
            " Player must be in a planet hierarchy")]
        [SerializeField] private Transform _currentPlanet = default;

        public Player AttachedPlayer { get; private set; }

        public PlayerValues Values => AttachedPlayer.Values;

        /// <summary>
        /// Property to access the reference to the current planet the player 
        /// is on
        /// </summary>
        /// <value> The current planet </value>
        public Transform CurrentPlanet
        {
            get => _currentPlanet;
            private set => _currentPlanet = value;
        }

        /// <summary>
        /// Property to access the reference to the space roll axis values
        /// </summary>
        /// <value> Axis value (-1 to 1) </value>
        public float SpaceRoll
        {
            get => _spaceRoll;
            private set => _spaceRoll = value;
        }
        // Its a variable so it is assigned on update and only runs once per frame and not every time it is called

        public bool IsGrounded { get; private set; }
        public bool BossFight => _bossFight;

        private void Awake()
        {
            AttachedPlayer = GetComponent<Player>();
            _rb = GetComponent<Rigidbody>();
            _selfCollider = GetComponent<SphereCollider>();

            _rb.freezeRotation = true;
            _rb.useGravity = false;
            _rb.drag = 10f;
            _rb.interpolation = RigidbodyInterpolation.Extrapolate;
            _rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

            _selfCollider.center = new Vector3(0.0f, 0.0f, 0.2f);
            _selfCollider.radius = 0.85f;

            _outPlanet = new OutPlanetMovement();
            _onPlanet = new OnPlanetMovement();
            _bossMovement = new BossMovementBehaviour();
            _attackBehaviour = new AttackBehaviour();
        }

        private void Start()
        {
            _outPlanet.MovementInit(this);
            _onPlanet.MovementInit(this);
            _bossMovement.MovementInit(this);
            _attackBehaviour.MovementInit(this);

            AttachedPlayer.PControls.PlayerControls.PhotonThrust.performed += ctx => ChangeToSpaceMovement();
            AttachedPlayer.PControls.PlayerControls.Jump.performed += ctx => _moveBehaviour.Jump(this);
            AttachedPlayer.PControls.PlayerControls.SpaceRoll.performed += ctx => _spaceRoll = ctx.ReadValue<float>();
            AttachedPlayer.PControls.PlayerControls.Interaction.performed += ctx => InteractPressed();
            // THIS SHOULD BE USED TO MAKE DASH HAVE DIFFERENT SPEED VALUES,
            // LONGER THE PRESS, HIGHER THE SPEED
            AttachedPlayer.PControls.PlayerControls.Jump.started += ctx => _moveBehaviour.Dash(this);
            AttachedPlayer.PControls.PlayerControls.Jump.canceled += ctx => _moveBehaviour.Dash(this);
            AttachedPlayer.PControls.PlayerControls.PrepareForFlight.started += ctx => PrepareForFlight();

            if (_bossFight || staticBossFight)
            {
                _moveBehaviour = _bossMovement;
                _moveBehaviour.OnPlanet = true;
            }
            else
            {
                _moveBehaviour = _onPlanet;
                _moveBehaviour.OnPlanet = true;
                _moveBehaviour.CurrentStamina = Values.MaxStamina;
            }

            if (CurrentPlanet == null)
                CurrentPlanet = transform.parent;
            _currentCelestial = CurrentPlanet?.GetComponent<CelestialObject>();
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            IsGrounded = Physics.CheckSphere(Feet.position, 0.01f, LayerMask.GetMask("Celestial"), QueryTriggerInteraction.Ignore);

            if (_moveBehaviour.CurrentStamina < 0.0f)
            {
                if (_lastPlanetPosition != null)
                {
                    ReturnToLastPlanet();
                }
            }
        }

        /// <summary>
        /// This function is called every fixed framerate frame,
        /// if the MonoBehaviour is enabled.
        /// </summary>
        private void FixedUpdate()
        {
            _moveBehaviour.Movement(this);

            DetectPlanet();
        }

        /// <summary>
        /// Detect a planet while in space movement
        /// </summary>
        private void DetectPlanet()
        {
            // If the player is moving in space
            if (_moveBehaviour.OnPlanet == false && Attacking == false)
            {
                RaycastHit hit;

                // When the ray intersects with a planet, the player will
                // Move in it
                if (Physics.Raycast(transform.position,
                        transform.forward, out hit,
                        10.0f, _planetLayermask))
                {
                    // For debuggin purposes, lets try all possibilities

                    // Switch MoveBehaviour if ray hits a celestial planet
                    if (hit.transform.TryGetComponent<CelestialObject>(
                            out _currentCelestial))
                    {
                        Destroy(_lastPlanetPosition);
                        ChangeToPlanetMovement(CurrentCelestial);
                    }
                    else
                    {
                        //! this might be able to be removed, all planets are 
                        //! now celestial objects
                        Destroy(_lastPlanetPosition);
                        _currentCelestial = null;
                        ChangeToPlanetMovement(hit.collider.gameObject);
                    }
                }
            }
        }

        /// <summary>
        /// Method to activate planet movement
        /// </summary>
        /// <param name="obj">
        /// Uses a GameObject as reference
        /// </param>
        private void ChangeToPlanetMovement(GameObject obj)
        {
            ChangeToPlanetMovement(FindCelestialScriptInParent(obj.transform));
        }

        private CelestialObject FindCelestialScriptInParent(Transform child)
        {
            CelestialObject celestial;
            // Check if parent exists
            if (child.parent == null)
            {
                return null;
            }
            // It was found
            if (child.parent.TryGetComponent<CelestialObject>(out celestial))
            {
                return celestial;
            }
            // Not found but has a parent
            else
            {
                return FindCelestialScriptInParent(child.parent);
            }
        }

        /// <summary>
        /// Overload method to activate planet movement
        /// </summary>
        /// <param name="celestial">
        /// Uses a CelestialObject script as reference
        /// </param>
        private void ChangeToPlanetMovement(CelestialObject celestial)
        {
            if (celestial == null)
            {
                Debug.LogError("Trying given celestial does not exist.");
                return;
            }

            Debug.Log("The player landed on " + celestial.name);

            celestial.SetPlanetChild(transform);

            AttachedPlayer.CameraControls.ChangeToGroundCamera();

            // ... get the current stamina...
            _auxStamina = _moveBehaviour.CurrentStamina;

            // ... reset movement vector to 0...
            _moveBehaviour.CurrentMovement = Vector3.zero;

            // ... assign current planet...
            CurrentPlanet = celestial.transform;

            // ... change movement behaviour and give the correct stamina value
            _moveBehaviour = _onPlanet;
            _moveBehaviour.PrepareForFlight = false;
            _moveBehaviour.CurrentStamina = _auxStamina;
        }

        /// <summary>
        /// Method to activate space movement
        /// </summary>
        private void ChangeToSpaceMovement()
        {
            // If the player is prepared to go into space movement...
            if (_moveBehaviour.PrepareForFlight)
            {
                // ... if the player is on a planet...
                if (_moveBehaviour.OnPlanet)
                {
                    // ... and is in a planet hierarchy...
                    if (CurrentCelestial != null)
                    {
                        // ... release the player from the planet hierarchy...
                        transform.parent = null;
                    }

                    // ... set last position in case of full stamina loss...
                    SetLastPlanetPosition();
                    AttachedPlayer.CameraControls.ChangeToFlightCamera();

                    // ... reset 3D model child object local rotation...
                    AttachedPlayer.VisualsObject.localRotation = Quaternion.identity;

                    // ... switch MoveBehaviour...
                    _moveBehaviour = _outPlanet;

                    // ... get the current stamina value...
                    _moveBehaviour.CurrentStamina = _auxStamina;

                    // ... reset the movement vector and velocity...
                    _moveBehaviour.CurrentMovement = Vector3.zero;
                    _rb.velocity = Vector3.zero;

                    // ... finally change dash bool value to true
                    _moveBehaviour.PressDash = true;
                }
            }
        }

        /// <summary>
        /// Method that is used to set position and last planet position in case player wants to fly
        /// </summary>
        private void PrepareForFlight()
        {
            // If the player has met the requirements to go to space movement...
            if (_moveBehaviour.CurrentStamina >= Values.MaxStamina)
            {
                // ... change the value of the prepare for flight bool and
                // assign the current stamina values to the auxiliary variable
                _moveBehaviour.PrepareForFlight = !_moveBehaviour.PrepareForFlight;
                _auxStamina = _moveBehaviour.CurrentStamina;
            }
        }

        private void InteractPressed()
        {
            if (Attacking || (_moveBehaviour != _onPlanet && _moveBehaviour != _bossMovement)) return;
            Attacking = true;
            (_attackBehaviour as AttackBehaviour).NewAttack(this);
            _moveBehaviour = _attackBehaviour;

            Physics.gravity = new Vector3(0, -9.98f, 0);

            int dir = Random.Range(.0f, 1.0f) >= 0.5f ? 1 : -1;

            if (IsGrounded)
                LeanTween.rotateAround(
                    AttachedPlayer.VisualsObject.gameObject,
                    AttachedPlayer.VisualsObject.transform.up * dir,
                    360,
                    0.85f)
                .setEaseInOutBack()
                .setOnComplete(OnAttackFinished);
            else
                LeanTween.rotateAround(
                    AttachedPlayer.VisualsObject.gameObject,
                    AttachedPlayer.VisualsObject.transform.right * dir,
                    360,
                    0.85f)
                .setEaseInOutBack()
                .setOnComplete(OnAttackFinished);
        }

        private void OnAttackFinished()
        {
            Attacking = false;
            if (staticBossFight || _bossFight)
            {
                _moveBehaviour = _bossMovement;
                Physics.gravity = new Vector3(0f, -150f, 0f);
            }
            else
            {
                _moveBehaviour = _onPlanet;
            }
        }

        /// <summary>
        /// Method to set the player's last planet position before going to space flight
        /// </summary>
        private void SetLastPlanetPosition()
        {
            // If there isn't a last position reference create one
            if (_lastPlanetPosition == null)
            {
                _lastPlanetPosition = new GameObject("Last Position");
            }

            // Assign the postion values to be the same as the player's
            _lastPlanetPosition.transform.position = transform.position;
            _lastPlanetPosition.transform.rotation = transform.rotation;

            // Put the last position gameObject in the same hierarchy level as
            // the player
            _lastPlanetPosition.transform.SetParent(transform.parent);
        }

        /// <summary>
        /// Method to return to last planet position
        /// </summary>
        public void ReturnToLastPlanet()
        {
            _rb.velocity = Vector3.zero;
            transform.position = _lastPlanetPosition.transform.position;
            transform.rotation = _lastPlanetPosition.transform.rotation;
            Destroy(_lastPlanetPosition);
            ChangeToPlanetMovement(_currentCelestial);
        }

        /// <summary>
        /// Callback to draw gizmos that are pickable and always drawn.
        /// </summary>
        void OnDrawGizmos()
        {
            Gizmos.DrawSphere(_feet.position, 0.05f);
        }
    }
}