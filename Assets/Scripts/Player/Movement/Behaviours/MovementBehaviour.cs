﻿using UnityEngine;

namespace Player.Movement.Behaviours
{
    public abstract class MovementBehaviour
    {
        //* General Variables
        protected Vector3 _movement;
        protected Vector3 _turn;
        protected float _stamina;
        protected float _rotation;
        protected bool _onPlanet;
        protected bool prepareForFlight;
        protected bool _flying;
        protected bool _isGrounded;
        protected bool _pressDash;

        public bool OnPlanet { get => _onPlanet; set { _onPlanet = value; } }
        public bool PrepareForFlight { get => prepareForFlight; set => prepareForFlight = value; }
        public float CurrentStamina { get => _stamina; set { _stamina = value; } }
        public Vector3 CurrentMovement { get => _movement; set { _movement = value; } }
        public bool IsGrounded { get => _isGrounded; }
        public bool PressDash { get => _pressDash; set => _pressDash = value; }

        public virtual void MovementInit(PlayerMovement playerMov)
        {
            _stamina = playerMov.Values.MaxStamina;
        }

        /// <summary>
        /// Virtual method for movement, to be override by actual behaviour
        /// </summary>
        /// <param name="playerMov">Reference to the player script</param>
        public virtual void Movement(PlayerMovement playerMov)
        { }

        /// <summary>
        /// Method to let the player jump when on planet
        /// </summary>
        /// <param name="playerMov"> 
        /// Reference to the player's script
        /// </param>
        public virtual void Jump(PlayerMovement playerMov)
        { }

        /// <summary>
        /// Method that makes player move faster in space
        /// </summary>
        /// <param name="playerMov"> 
        /// Reference to the player's script
        /// </param>
        public virtual void Dash(PlayerMovement playerMov)
        { }

        /// <summary>
        /// Method to change the drag value of the player's Rigidbody component
        /// </summary>
        /// <param name="playerMov"> Reference to the player's script </param>
        public void ChangeDrag(PlayerMovement playerMov)
        {
            if (!_isGrounded)
            {
                playerMov.Rigidbody.drag = 0f;
            }
            else
            {
                playerMov.Rigidbody.drag = 10f;
            }
        }

        /// <summary>
        /// Regenerates stamina while on planet
        /// </summary>
        /// <param name="playerMov"> 
        /// Reference to the player's script
        /// </param>
        public void StaminaRegen(PlayerMovement playerMov)
        {
            if (_stamina < playerMov.Values.MaxStamina)
            {
                _stamina += playerMov.Values.StaminaRechargeRate * Time.deltaTime;
            }
        }

        /// <summary>
        /// Method to reduce stamina.virtual To be used in space movement
        /// </summary>
        /// <param name="playerMov"> 
        /// Reference to the player's script
        /// </param>
        public void StaminaUse(PlayerMovement playerMov)
        {
            _stamina -= playerMov.Values.StaminaUseRate * Time.deltaTime;
        }
    }
}
