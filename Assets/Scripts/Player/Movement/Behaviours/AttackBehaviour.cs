﻿using System.Collections;
using System.Collections.Generic;
using Interactables;
using UnityEngine;

namespace Player.Movement.Behaviours
{
    public class AttackBehaviour : MovementBehaviour
    {
        private const int JOINT_POINTS = 3;
        private const float ATTACK_DELAY = 0.3f;
        private const float ATTACK_DURATION = ATTACK_DELAY + 0.3f;

        private LayerMask _interactableMask;
        private Transform initialJoint;
        private Transform finalJoint;

        private Transform[] _jointPoints;
        private Vector3[] _stashedJointPositions;
        private List<Interaction> _hitInteractables;

        private float _timeOfNewAttack;

        private float ElapsedTime => Time.time - _timeOfNewAttack;
        private bool InAttackWindow => !(ElapsedTime < ATTACK_DELAY || ElapsedTime > ATTACK_DURATION);

        public override void MovementInit(PlayerMovement playerMov)
        {
            _interactableMask = LayerMask.GetMask(Interaction.INTERACTION_LAYER);
            Transform bones = GameObject.Find("SpookSkeleton").transform;
            _hitInteractables = new List<Interaction>(10);

            _stashedJointPositions = new Vector3[JOINT_POINTS];
            _jointPoints = new Transform[JOINT_POINTS];
            _stamina = playerMov.AttachedPlayer.Values.MaxStamina;

            GetRecursiveChilds(bones.GetChild(0), _jointPoints, 0, 0,
                0, 4, 7);
        }

        private void GetRecursiveChilds(Transform current,
            Transform[] to,
            int childIndex, int index,
            params int[] childsToGet)
        {
            int nowIndex = index;
            for (int i = 0; i < childsToGet.Length; i++)
                if (childIndex == childsToGet[i])
                {
                    to[nowIndex] = current;
                    nowIndex++;
                    break;
                }

            if (current.childCount != 0)
                GetRecursiveChilds(current.GetChild(0), to, childIndex + 1,
                    nowIndex, childsToGet);
        }

        // reset the stashed positions to avoid checking positions from last attack
        public void NewAttack(PlayerMovement playerMov)
        {
            for (int i = 0; i < JOINT_POINTS; i++)
            {
                Debug.Log(_stashedJointPositions[i]);
                Debug.Log(_jointPoints[i]);
                _stashedJointPositions[i] = _jointPoints[i].position;
            }
            playerMov.Rigidbody.velocity *= 0.1f;
            _timeOfNewAttack = Time.time;
            _hitInteractables.Clear();
        }

        public override void Movement(PlayerMovement playerMov)
        {
            if (!InAttackWindow) return;

            Vector3[] thisJointPositions;
            thisJointPositions = new Vector3[JOINT_POINTS];

            for (int i = 0; i < JOINT_POINTS; i++)
            {
                thisJointPositions[i] = _jointPoints[i].position;
                TryHit(thisJointPositions[i], _stashedJointPositions[i], playerMov.AttachedPlayer);
            }

            _stashedJointPositions = thisJointPositions;
        }

        public void TryHit(Vector3 nowPosition, Vector3 prevPosition, Player p)
        {
            Vector3 offset = -p.VisualsObject.transform.forward * 1f;
            Interaction interaction = null;
            RaycastHit hit;

            if (Physics.SphereCast(
                    prevPosition,
                    1.0f,
                    (nowPosition - prevPosition).normalized,
                    out hit, Vector3.Distance(nowPosition, prevPosition),
                    _interactableMask))
            {
                interaction = hit.collider.gameObject.GetComponent<Interaction>();
                if (!AlreadyHit(interaction))
                {
                    _hitInteractables.Add(interaction);
                    interaction?.Interact(p, hit.transform.position - p.transform.position);
                    p.DoHitParticles(hit.point);
                }
            }
            Debug.DrawRay(prevPosition, (nowPosition - prevPosition).normalized *
                Vector3.Distance(nowPosition, prevPosition));

            bool AlreadyHit(Interaction i)
            {
                foreach (Interaction inter in _hitInteractables)
                    if (inter == i) return true;
                return false;
            }
        }
    }
}