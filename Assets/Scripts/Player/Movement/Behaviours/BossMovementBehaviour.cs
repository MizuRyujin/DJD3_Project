using UnityEngine;

namespace Player.Movement.Behaviours
{
    public class BossMovementBehaviour : MovementBehaviour
    {
        public override void MovementInit(PlayerMovement playerMov)
        {
            //TODO Put stamina in player movement script
            _stamina = 0.0f;
            Physics.gravity = new Vector3(0f, -150f, 0f);
        }

        public override void Movement(PlayerMovement playerMov)
        {
            playerMov.Rigidbody.useGravity = true;
            CharMovement(playerMov);
            RotateModel(playerMov);
            CheckGround(playerMov);
            ChangeDrag(playerMov);
        }

        private void CharMovement(PlayerMovement playerMov)
        {
            _movement.z = playerMov.AttachedPlayer.MovementInput.y;
            _movement.x = playerMov.AttachedPlayer.MovementInput.x;


            playerMov.transform.Translate(_movement *
                                    playerMov.Values.Speed * Time.deltaTime);
        }

        private void RotateModel(PlayerMovement playerMov)
        {
            if (playerMov.AttachedPlayer.MovementInput != Vector2.zero)
            {
                playerMov.AttachedPlayer.VisualsObject.localEulerAngles = Quaternion.Slerp(
                    Quaternion.Euler(playerMov.AttachedPlayer.VisualsObject.localEulerAngles),
                    Quaternion.Euler(new Vector3(
                    0.0f, Mathf.Atan2(
                        playerMov.AttachedPlayer.MovementInput.x,
                        playerMov.AttachedPlayer.MovementInput.y) * 180 / Mathf.PI, 0.0f)),
                        playerMov.Values.RotateSpeed * Time.deltaTime).eulerAngles;
            }
        }

        /// <summary>
        /// Override method to make the player jump
        /// </summary>
        /// <param name="playerMov"> Player's script reference </param>
        public override void Jump(PlayerMovement playerMov)
        {
            if (_isGrounded == true)
            {
                playerMov.Rigidbody.AddForce(playerMov.transform.up *
                            playerMov.Values.JumpForce * 1.75f *
                            playerMov.Values.JumpHeight);
            }
        }

        private void CheckGround(PlayerMovement playerMov)
        {
            _isGrounded = Physics.CheckSphere(
                playerMov.Feet.position, 0.01f, LayerMask.GetMask("Celestial"), QueryTriggerInteraction.Ignore);
        }
    }
}