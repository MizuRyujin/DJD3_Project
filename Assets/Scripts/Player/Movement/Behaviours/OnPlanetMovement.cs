using UnityEngine;

namespace Player.Movement.Behaviours
{
    public class OnPlanetMovement : MovementBehaviour
    {
        private bool lastGroundedStatus;

        /// <summary>
        /// Method that controls character movement, 
        /// override from parent abstract class
        /// </summary>
        /// <param name="playerMov"> Reference to player script </param>
        public override void Movement(PlayerMovement playerMov)
        {
            CheckGround(playerMov);
            GravityControl(playerMov);
            PlanetRotation(playerMov);
            StaminaRegen(playerMov);

            if (!playerMov.AttachedPlayer.Interacting)
            {
                CharMovement(playerMov);

                //* This works if not preparing for flight
                RotateModel(playerMov);
                RotateWithCamera(playerMov);

                ChangeDrag(playerMov);
            }

            //! This is triggering while walking, gotta rework the check
            if (!lastGroundedStatus && _isGrounded)
                OnLand(playerMov.Rigidbody, playerMov.CurrentPlanet);

            lastGroundedStatus = _isGrounded;
        }

        /// <summary>
        /// Method that controls planet side rotation
        /// </summary>
        /// <param name="playerMov"> Player script reference </param>
        private void PlanetRotation(PlayerMovement playerMov)
        {
            Quaternion toRotation = Quaternion.FromToRotation(
                playerMov.Rigidbody.transform.up, GetCelestialObjectNormal(
                    playerMov)) * playerMov.Rigidbody.transform.rotation;

            playerMov.transform.rotation = toRotation;
        }


        /// <summary>
        /// Method to control the input and actual movement
        /// </summary>
        /// <param name="playerMov"> Reference to player's script </param>
        private void CharMovement(PlayerMovement playerMov)
        {
            _movement.z = playerMov.AttachedPlayer.MovementInput.y;
            _movement.x = playerMov.AttachedPlayer.MovementInput.x;

            Vector3 camRot = playerMov.AttachedPlayer.CameraControls.transform.localRotation.eulerAngles;

            if (playerMov.AttachedPlayer.MovementInput != Vector2.zero && Mathf.Abs(camRot.y) > 1)
            {
                //! STILL "SNAPPY"
                playerMov.transform.rotation = playerMov.AttachedPlayer.CameraControls.transform.rotation;

                playerMov.AttachedPlayer.CameraControls.transform.localRotation = Quaternion.Euler(
                                                    camRot.x, 0.0f, camRot.z);
            }

            //* Only actually move if not preparing for flight
            if (!PrepareForFlight)
            {
                playerMov.transform.Translate(_movement *
                                            playerMov.Values.Speed * Time.deltaTime);
            }
        }

        /// <summary>
        /// Method to rotate player model towards input
        /// </summary>
        /// <param name="playerMov"> Reference to player's script </param>
        private void RotateModel(PlayerMovement playerMov)
        {
            if (playerMov.AttachedPlayer.MovementInput != Vector2.zero && !PrepareForFlight)
            {
                playerMov.AttachedPlayer.VisualsObject.localEulerAngles = Quaternion.Slerp(
                    Quaternion.Euler(playerMov.AttachedPlayer.VisualsObject.localEulerAngles),
                    Quaternion.Euler(new Vector3(
                    0.0f, Mathf.Atan2(
                        playerMov.AttachedPlayer.MovementInput.x, playerMov.AttachedPlayer.MovementInput.y) * 180 / Mathf.PI, 0.0f)),
                        playerMov.Values.RotateSpeed * Time.deltaTime).eulerAngles;
            }

        }

        /// <summary>
        /// Method to make player rotate with the camera input when moving
        /// </summary>
        /// <param name="playerMov"> Reference to player's script </param>
        private void RotateWithCamera(PlayerMovement playerMov)
        {
            if (!PrepareForFlight)
            {
                //* Check if there's input from cam controls and player control...
                if (playerMov.AttachedPlayer.CameraControls.CamInput != Vector2.zero &&
                        playerMov.AttachedPlayer.MovementInput != Vector2.zero)
                {
                    if (playerMov.AttachedPlayer.CameraControls.CamInput.x > 0 && _rotation < 0) _rotation = 0;
                    else if (playerMov.AttachedPlayer.CameraControls.CamInput.x < 0 && _rotation > 0) _rotation = 0;

                    //* ... if so, add delta of input to a float...
                    _rotation += playerMov.AttachedPlayer.CameraControls.CamInput.x;

                    _rotation = Mathf.Clamp(
                        _rotation,
                        -playerMov.Values.MaxRotationSpeed / 10,
                        playerMov.Values.MaxRotationSpeed / 10);

                    //* ... and finaly rotate said float
                    playerMov.transform.Rotate(0.0f, _rotation, 0.0f);
                }
                else
                {
                    _rotation = 0.0f;
                }
            }
        }

        /// <summary>
        /// Method that rotates the gameObject torwards input
        /// </summary>
        /// <param name="playerMov"> Reference to player's script </param>
        private void RotateTowardsInputAxis(PlayerMovement playerMov)
        {
            if (PrepareForFlight)
            {
                if (playerMov.AttachedPlayer.MovementInput.x != 0.0f)
                {
                    if (playerMov.AttachedPlayer.MovementInput.x > 0 && _rotation < 0) _rotation = 0;
                    else if (playerMov.AttachedPlayer.MovementInput.x < 0 && _rotation > 0) _rotation = 0;

                    _rotation += playerMov.AttachedPlayer.MovementInput.x * playerMov.Values.RotateSpeed * Time.fixedDeltaTime;

                    _rotation = Mathf.Clamp(
                        _rotation, -playerMov.Values.MaxRotationSpeed / 10,
                        playerMov.Values.MaxRotationSpeed / 10);

                    playerMov.transform.Rotate(0.0f, _rotation, 0.0f);
                }
            }
            else
            {
                _rotation = 0.0f;
            }
        }

        /// <summary>
        /// Override method to make the player jump
        /// </summary>
        /// <param name="playerMov"> Player's script reference </param>
        public override void Jump(PlayerMovement playerMov)
        {
            if (_isGrounded == true && PrepareForFlight == false)
            {
                playerMov.Rigidbody.AddForce(playerMov.transform.up *
                            playerMov.Values.JumpForce * playerMov.Values.JumpHeight);
            }
        }

        /// <summary>
        /// Method to create gravity when on a planet
        /// </summary>
        /// <param name="playerMov"> Player's script reference </param>
        private void GravityControl(PlayerMovement playerMov)
        {
            if (!playerMov.BossFight)
            {
                Vector3 gravDirection = (
                    playerMov.Rigidbody.transform.position - playerMov.CurrentPlanet.transform.position).normalized;

                if (_isGrounded == false && PrepareForFlight == false)
                {
                    playerMov.Rigidbody.AddForce(gravDirection * -playerMov.Values.Gravity);
                }
                else if (PrepareForFlight)
                {
                    playerMov.Rigidbody.velocity = Vector3.zero;
                }
            }
        }

        /// <summary>
        /// Method to check if player is on the ground
        /// </summary>
        /// <param name="playerMov"> Player's script reference </param>
        private void CheckGround(PlayerMovement playerMov)
        {
            _isGrounded = Physics.CheckSphere(
                playerMov.Feet.position, 0.01f, LayerMask.GetMask("Celestial"), QueryTriggerInteraction.Ignore);
        }

        /// <summary>
        /// Method that gets the ground normal for gravity application
        /// </summary>
        /// <param name="playerMov"> Reference to player script </param>
        /// <returns> Vector with the ground normal value </returns>
        private Vector3 GetCelestialObjectNormal(PlayerMovement playerMov)
        {
            Vector3 normal = (playerMov.Rigidbody.transform.position - playerMov.CurrentPlanet.position).normalized;

            return normal;
        }

        private void OnLand(Rigidbody player, Transform planet)
        {
            Transform targetChild = player.transform.GetChild(0);
            Vector3 initialScale = targetChild.localScale;
            //! Squash();

            void Squash()
            {
                // Squash the FX object
                Vector3 squash = new Vector3(
                    targetChild.localScale.x + targetChild.localScale.x * 0.4f,
                    targetChild.localScale.y - targetChild.localScale.y * 0.4f,
                    targetChild.localScale.z);

                LeanTween.scale(targetChild.gameObject, squash, 0.1f).setEasePunch().setOnComplete(ForceResetScale);
            }

            void ForceResetScale()
            {
                targetChild.localScale = initialScale;
            }
        }
    }
}