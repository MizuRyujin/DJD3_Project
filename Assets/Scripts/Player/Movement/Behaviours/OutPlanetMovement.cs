﻿using UnityEngine;

namespace Player.Movement.Behaviours
{
    public class OutPlanetMovement : MovementBehaviour
    {
        private ParticleSystem _dashParticle;
        private float chargeTime = 3.0f;
        private float timer = 0.0f;
        private const float _maxDashPower = 2.0f;
        private float _dashPower = default;

        /// <summary>
        /// Override for the movement method, designed for outer planet movement
        /// </summary>
        /// <param name="playerMov"> Reference to player script </param>
        public override void Movement(PlayerMovement playerMov)
        {
            if (_flying == false)
            {
                LiftOf(playerMov);
            }

            FlyControls(playerMov);
            DashTimer(playerMov);
            SlowDown();

            playerMov.transform.Translate(_movement);
            playerMov.transform.Rotate(_turn);

            CheckStamina(playerMov);
        }

        /// <summary>
        /// Method to get more speed while on outer space movement
        /// </summary>
        /// <param name="values"></param>
        public override void Dash(PlayerMovement playerMov)
        {
            PressDash = !PressDash;

            // ON RELEASE
            if (PressDash == false)
            {
                // Spend Stamina based on power
                DashEffect(playerMov.Values, _dashPower);
            }
        }

        private void DashTimer(PlayerMovement playerMov)
        {
            if (PressDash == true && timer < chargeTime)
            {
                // Start counting
                timer += 1.0f * Time.deltaTime;
                StaminaUse(playerMov);
            }
            else if (PressDash == false || timer >= chargeTime)
            {
                // End counting
                _dashPower = timer;
            }
        }

        private void SlowDown()
        {
            float speedFraction = _movement.z * 0.25f;

            if (PressDash == true || _movement.z > 0f)
            {
                // Slow Down
                _movement.z -= speedFraction * Time.deltaTime;
            }
        }

        /// <summary>
        /// Is called to apply force of dash
        /// </summary>
        /// <param name="values"> Player values to apply </param>
        /// <param name="power"> Power to add </param>
        private void DashEffect(PlayerValues values, float power)
        {
            // Apply power
            _movement.z = power + values.SpaceMoveVector.z;

            _movement.z = Mathf.Clamp(
                    _movement.z, 0.0f, values.MaxOutPlanetVelocity);

            timer = 0.0f;
        }


        /// <summary>
        /// Method that controls player movement in space like a plane
        /// </summary>
        /// <param name="playerMov">
        /// Reference to player script, makes use of
        /// player values scriptable object
        /// </param>
        private void FlyControls(PlayerMovement playerMov)
        {
            if (playerMov.AttachedPlayer.MovementInput != Vector2.zero || playerMov.SpaceRoll != 0f)
            {
                _turn.x += playerMov.Values.SpaceMoveVector.x *
                        Time.deltaTime * playerMov.AttachedPlayer.MovementInput.y;

                _turn.y += playerMov.Values.SpaceMoveVector.y *
                        Time.deltaTime * playerMov.AttachedPlayer.MovementInput.x;

                _turn.z -= playerMov.Values.SpaceMoveVector.z *
                        Time.deltaTime * playerMov.SpaceRoll;
            }
            else
            {
                if (_turn.x < 0)
                {
                    _turn.x += playerMov.Values.SpaceMoveVector.x * Time.deltaTime;
                }
                if (_turn.x > 0)
                {
                    _turn.x -= playerMov.Values.SpaceMoveVector.x * Time.deltaTime;
                }
                if (_turn.y < 0)
                {
                    _turn.y += playerMov.Values.SpaceMoveVector.y * Time.deltaTime;
                }
                if (_turn.y > 0)
                {
                    _turn.y -= playerMov.Values.SpaceMoveVector.y * Time.deltaTime;
                }
                if (_turn.z < 0)
                {
                    _turn.z += playerMov.Values.SpaceMoveVector.z * Time.deltaTime;
                }
                if (_turn.z > 0)
                {
                    _turn.z -= playerMov.Values.SpaceMoveVector.z * Time.deltaTime;
                }
            }

            _turn.x = Mathf.Clamp(_turn.x, -2, 2);
            _turn.y = Mathf.Clamp(_turn.y, -2, 2);
            _turn.z = Mathf.Clamp(_turn.z, -2, 2);
        }

        /// <summary>
        /// Method to make the player lift of the planet
        /// </summary>
        /// <param name="playerMov"></param>
        private void LiftOf(PlayerMovement playerMov)
        {
            prepareForFlight = true;

            _onPlanet = false;

            _flying = true;

            playerMov.Rigidbody.velocity = Vector3.zero;

            _movement.z = playerMov.Values.MaxOutPlanetVelocity;
        }

        /// <summary>
        /// Method to check if player is out of stamina
        /// </summary>
        /// <param name="playerMov"></param>
        private void CheckStamina(PlayerMovement playerMov)
        {
            if (_stamina <= 0.0f)
            {
                // Returning to planet
                _movement = Vector3.zero;
                _turn = Vector3.zero;
                _onPlanet = true;
                _flying = false;
                PressDash = false;
                PrepareForFlight = false;
                Debug.Log("GOT NO MORE STAMINA!!");
            }
        }
    }
}
