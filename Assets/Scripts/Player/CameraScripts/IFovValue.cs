﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Value to lerp an FOV from a camera using the FOVChanger class
/// </summary>
public interface IFovValue
{
    /// <summary>
    /// Value between 0 and 1 to lerp values from another class
    /// </summary>
    /// <value></value>
    float LerpValue {get;}
}