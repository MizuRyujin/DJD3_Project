using UnityEngine;

namespace Player
{
    public class CameraControl : MonoBehaviour
    {
        /// <summary>
        /// Reference to camera values scriptable object
        /// </summary>
        [SerializeField] private CameraValues _camValues = default;

        /// <summary>
        /// Array of player cameras
        /// </summary>
        [Tooltip("Array of camera the player has in the camera rig child object")]
        [SerializeField] private GameObject[] _playerCams;
        public GameObject[] Cameras { get => _playerCams; private set { _playerCams = value; } }

        /// <summary>
        /// Reference to actual camera, which is a child of the rig
        /// </summary>
        private Transform _sensor = default;

        /// <summary>
        ///  Reference to player's script
        /// </summary>
        private Player _pScript = default;

        /// <summary>
        /// Reference to the player input system actions
        /// </summary>
        private PlayerControllerActions _pControls = default;

        /// <summary>
        /// Input system camera input value
        /// </summary>
        private Vector2 _camInput = default;

        //* Class variables
        /// <summary>
        /// Camera rotation value, with mouse movement
        /// </summary>
        private Vector3 _newRotation = default;

        /// <summary>
        /// Variable to store original camera position, before an obstacle came
        /// in between
        /// </summary>
        private Vector3 _intendedPosition = default;

        /// <summary>
        /// Information about what has the ray cast hit
        /// </summary>
        private RaycastHit _raycastHitInfo = default;

        /// <summary>
        /// Float that is used to check if camera position is to be resetted
        /// </summary>
        private float _resetTime = default;
        private float _minDistance = default;

        public Vector2 CamInput { get => _camInput; }

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            _sensor = transform.GetChild(0);

            _minDistance = _sensor.GetChild(0).transform.localPosition.z;

            _pScript = GetComponentInParent<Player>();

            _intendedPosition = _sensor.localPosition;

            _pControls = new PlayerControllerActions();
            _pControls.PlayerControls.CameraControl.performed += ctx => _camInput = Vector2.ClampMagnitude(ctx.ReadValue<Vector2>(), 1);

            _resetTime = _camValues.TimeToReset;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void LateUpdate()
        {

            UpdateCameraRotation(_camValues, _camInput);

            if (_pScript.AttachedMovement.MoveBehaviour.PrepareForFlight ||
                    !_pScript.AttachedMovement.MoveBehaviour.OnPlanet)
            {
                TimeToReset();
                ResetCameraPosition(_camValues);
            }
        }

        /// <summary>
        /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void FixedUpdate()
        {
            if (_pScript.AttachedMovement.MoveBehaviour.OnPlanet)
            {
                PreventCharacterOcclusion(_camValues);
            }
        }

        /// <summary>
        /// Method to control camera with mouse/left stick
        /// </summary>
        /// <param name="values"></param>
        /// <param name="input"></param>
        private void UpdateCameraRotation(CameraValues values, Vector2 input)
        {
            _newRotation = transform.localEulerAngles;

            if (_pScript.MovementInput == Vector2.zero)
            {
                _newRotation.y +=
                    input.x * values.STILL_ANGULAR_VELOCITY_FACTOR;
            }
            
            _newRotation.x -=
                input.y * values.ANGULAR_VELOCITY_FACTOR;

            _newRotation.x =
                _newRotation.x > 180f ? Mathf.Max(
                    _newRotation.x, values.MIN_ROTATION_X) :
                Mathf.Min(_newRotation.x, values.MAX_ROTATION_X);

            transform.localEulerAngles = _newRotation;
        }

        /// <summary>
        /// Method to reset camera rig rotation when left mouse button 
        /// is released
        /// </summary>
        private void ResetCameraPosition(CameraValues values)
        {
            if (_resetTime <= 0.0f)
            {
                //* if on planet
                //* reset cam to planet base position
                //*  if out of planet base position

                transform.localRotation = Quaternion.Slerp(
                    transform.localRotation,
                    Quaternion.identity,
                    values.AUTO_ADJUST_VELOCITY / 5 * Time.deltaTime);

            }
        }

        /// <summary>
        /// Method to check if the position reset timer is to be started
        /// </summary>
        private void TimeToReset()
        {
            if (_pScript.MovementInput != Vector2.zero || _camInput == Vector2.zero)
            {
                _resetTime -= Time.deltaTime;
            }
            else
            {
                _resetTime = _camValues.TimeToReset;
            }
        }

        /// <summary>
        /// Method to ensure character isn't hidden by objects
        /// </summary>
        private void PreventCharacterOcclusion(CameraValues values)
        {
            if (Physics.Linecast(transform.position,
                    transform.TransformPoint(_intendedPosition),
                    out _raycastHitInfo))
            {
                if (_raycastHitInfo.collider.isTrigger ||
                    _raycastHitInfo.collider.gameObject.layer ==
                    Interactables.Interaction.InteractionLayerNum) return;

                if (_sensor.localPosition.z + _minDistance < 0)
                {
                    _sensor.transform.position = Vector3.Lerp(
                        _sensor.position,
                        _raycastHitInfo.point,
                        values.AUTO_ADJUST_VELOCITY * Time.deltaTime);
                }
            }
            else
            {
                _sensor.localPosition = Vector3.Lerp(
                    _sensor.localPosition,
                    _intendedPosition,
                    values.AUTO_ADJUST_VELOCITY * Time.deltaTime);
            }
        }

        public void ChangeToGroundCamera()
        {
            // Change player active camera, to change perspective...
            _playerCams[0].SetActive(true);
            _playerCams[1].SetActive(false);
        }

        public void ChangeToFlightCamera()
        {
            // ... change player active camera, to change perspective...
            _playerCams[0].SetActive(false);
            _playerCams[1].SetActive(true);
        }

        public void DisableAll()
        {

        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        private void OnEnable()
        {
            _pControls.Enable();
        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled or inactive.
        /// </summary>
        private void OnDisable()
        {
            _pControls.Disable();
        }
    }
}