﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FovChanger : MonoBehaviour
{
    [Range(0, 120)]
    [SerializeField] private float _targetFOV;
    [SerializeField] private float _changeSpeed;
    [SerializeField] private CinemachineVirtualCamera _camera;

    private float _startFOV;
    private float _targetFovLerp;
    private IFovValue _fovValue;
    private void Awake() 
    {
        _fovValue = GetComponent<IFovValue>();
        _startFOV = _camera.m_Lens.FieldOfView;
    }

    public float LerpValue {get; set;}

    private void Update()
    {
        _targetFovLerp = Mathf.Lerp(_targetFovLerp, _fovValue.LerpValue, Time.deltaTime * _changeSpeed);
        _camera.m_Lens.FieldOfView = Mathf.Lerp(_startFOV, _targetFOV, _targetFovLerp);
    }
}