﻿using UnityEngine;

namespace Player
{
    [CreateAssetMenu(menuName = "Camera Values")]
    public class CameraValues : ScriptableObject
    {
        // Postion variables
        /// <summary>
        /// Max camera on planet distance
        /// </summary>
        /// <returns> Vector 3 with position values</returns>
        [SerializeField] private Vector3 _ON_PLANET_DISTANCE = new Vector3(0.0f, 3.0f, -10.0f);
       
        /// <summary>
        /// Max camera space flight distance
        /// </summary>
        /// <returns> Vector 3 with position values</returns>
        [SerializeField] private Vector3 _OUT_PLANET_DISTANCE = new Vector3(0.0f, 3.0f, -20.0f);

        // Rotation values
        /// <summary>
        /// Value for camera rotation velocity
        /// </summary>
        [SerializeField] private float _ANGULAR_VELOCITY_FACTOR = 5.0f;
        /// <summary>
        /// Value for camera rotation velocity while still
        /// </summary>
        [SerializeField] private float _STILL_VELOCITY_FACTOR = 60000.0f;

        /// <summary>
        /// Minimun rotation angle camera can be at
        /// </summary>
        [SerializeField] private float _MIN_ROTATION_X = 335.0f;
        
        /// <summary>
        /// Maximum rotation angle camera can be at
        /// </summary>
        [SerializeField] private float _MAX_ROTATION_X = 85.0f;

        // Camera obstacle move values
        /// <summary>
        /// Position adjustment velocity in case of character occlusion
        /// </summary>
        [SerializeField] private float _AUTO_ADJUST_VELOCITY = 10.0f;

        /// <summary>
        /// Camera position reset time
        /// </summary>
        [SerializeField] private float _timeToReset = 3.0f;

        /// <summary>
        /// Property for value for camera rotation velocity
        /// </summary>
        /// <value></value>
        public float ANGULAR_VELOCITY_FACTOR { get => _ANGULAR_VELOCITY_FACTOR; set => _ANGULAR_VELOCITY_FACTOR = value; }
        
        /// <summary>
        /// Property for value for camera rotation velocity
        /// </summary>
        /// <value></value>
        public float STILL_ANGULAR_VELOCITY_FACTOR { get => _STILL_VELOCITY_FACTOR; set => _STILL_VELOCITY_FACTOR = value; }
       
        /// <summary>
        /// Property for minimun rotation angle camera can be at
        /// </summary>
        /// <value></value>
        public float MIN_ROTATION_X { get => _MIN_ROTATION_X; set => _MIN_ROTATION_X = value; }
        
        /// <summary>
        /// Property for maximum rotation angle camera can be at
        /// </summary>
        /// <value></value>
        public float MAX_ROTATION_X { get => _MAX_ROTATION_X; set => _MAX_ROTATION_X = value; }
       
        /// <summary>
        /// Property for position adjustment velocity in case of character occlusion
        /// </summary>
        /// <value></value>
        public float AUTO_ADJUST_VELOCITY { get => _AUTO_ADJUST_VELOCITY; set => _AUTO_ADJUST_VELOCITY = value; }
       
        /// <summary>
        /// Property for camera position reset time
        /// </summary>
        /// <value></value>
        public float TimeToReset { get => _timeToReset; set => _timeToReset = value; }
        
        /// <summary>
        /// Property for max camera on planet distance
        /// </summary>
        /// <value></value>
        public Vector3 ON_PLANET_DISTANCE { get => _ON_PLANET_DISTANCE; set => _ON_PLANET_DISTANCE = value; }
        
        /// <summary>
        /// Property for max camera space flight distance
        /// </summary>
        /// <value></value>
        public Vector3 OUT_PLANET_DISTANCE { get => _OUT_PLANET_DISTANCE; set => _OUT_PLANET_DISTANCE = value; }
    }

}
