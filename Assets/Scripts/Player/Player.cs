﻿using Galaxy.CelestialObjects;
using Player.Movement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Player
{
    public class Player : MonoBehaviour
    {
        //* Class variables
        /// <summary>
        /// Reference to the particle system used by the player
        /// </summary>
        private ParticleSystem _hitParticles;
        /// <summary>
        /// Reference to the particle system used by the player when damaged
        /// </summary>
        [SerializeField] private ParticleSystem _damagedParticles = default;
        /// <summary>
        /// Variable to keep track of player's hit points
        /// </summary>
        [SerializeField] private byte _playerHP = default;
        /// <summary>
        /// Reference to the player's CameraControl script, 
        /// located in the CameraRig child gameObject
        /// </summary>
        private CameraControl _cameraControl;
        /// <summary>
        /// Property to access the reference to the camera control script
        /// </summary>
        public CameraControl CameraControls => _cameraControl;
        /// <summary>
        /// Reference to the photon charge container script
        /// </summary>
        private ChargeContainer _container;
        /// <summary>
        /// Bool to verify if the player is interacting with an interactable
        /// </summary>
        private bool _interacting;

        /// <summary>
        /// Property to access the reference to the charge container script
        /// </summary>
        public ChargeContainer ChargeContainer => _container;
        /// <summary>
        /// Property to access the reference to the interacting bool value
        /// </summary>
        /// <value> True or false depending ont the value of _interacting </value>
        public bool Interacting
        {
            get => _interacting;
            set => _interacting = value;
        }

        /// <summary>
        /// Reference to the child gameObject that contains the 3D model
        /// </summary>
        private Transform _visualsObject;
        /// <summary>
        /// Property to access the reference to the player's model
        /// </summary>
        public Transform VisualsObject => _visualsObject;
        //* Input System
        /// <summary>
        /// Reference to Unity's new input system script
        /// </summary>
        private PlayerControllerActions _pControls;

        //* Move vector/value
        /// <summary>
        /// Variable to store the movement input values, gotten by the new input
        /// system
        /// </summary>
        private Vector2 _movementInput;

        public PlayerControllerActions PControls => _pControls;
        /// <summary>
        /// Property to access the reference to the player values scriptable 
        /// object
        /// </summary>
        public PlayerValues Values => _values;

        /// <summary>
        /// Reference to the player values scriptable object
        /// </summary>
        [Tooltip("Reference to the player values scriptable object")]
        [SerializeField] private PlayerValues _values = default;

        /// <summary>
        /// Property to access the reference to the movement input values
        /// </summary>
        /// <value> Vector2 input values </value>
        /// 
        public Vector2 MovementInput
        {
            get => _movementInput;
            private set => _movementInput = value;
        }

        public bool IsGrounded => AttachedMovement.IsGrounded;

        public PlayerMovement AttachedMovement { get; private set; }
        public byte PlayerHP => _playerHP;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _playerHP = _values.MaxHP;
            _container = GetComponent<ChargeContainer>();
            _cameraControl = GetComponentInChildren<CameraControl>();
            AttachedMovement = GetComponentInChildren<PlayerMovement>();
            _visualsObject = transform.Find("Visuals");

            _pControls = new PlayerControllerActions();
            _pControls.PlayerControls.Move.performed += ctx =>
                _movementInput = ctx.ReadValue<Vector2>();

        }

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        private void Start()
        {
            _hitParticles = Instantiate(Values.TailHitPrefab)
                .GetComponent<ParticleSystem>();
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if (_playerHP == 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        /// <summary>
        /// Method that contains the functionality for when the player is hit
        /// </summary>
        private void Hit()
        {
            _playerHP -= 1;
            _damagedParticles.Emit(Random.Range(7,10));
            print("Player got hit");
        }

        /// <summary>
        /// Method to enable or disable player camera rig, consequentially 
        /// turning on or off the player cameras
        /// </summary>
        /// <param name="active">
        /// Bool to make the camera rig gameObject
        /// enable or disable
        /// </param>
        public void TogglePause(bool active)
        {
            _cameraControl.gameObject.SetActive(!active);
            Interacting = active;
        }

        public void DoHitParticles(Vector3 at)
        {
            // Temp fix due to unknown bug
            if (_hitParticles == null)
                _hitParticles = Instantiate(Values.TailHitPrefab)
                .GetComponent<ParticleSystem>();

            _hitParticles.transform.position = at;
            _hitParticles.Emit(1);
        }

        /// <summary>
        /// Method to return the player to the last planet it was on
        /// </summary>
        public void ReturnToLastPlanet()
        {
            AttachedMovement.ReturnToLastPlanet();
        }

        /// <summary>
        /// Method to be called when player is hit
        /// </summary>
        public void OnHit()
        {
            Hit();
            PlayerHit?.Invoke();
        }

        public System.Action PlayerHit;

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        private void OnEnable()
        {
            _pControls.Enable();
        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled or inactive.
        /// </summary>
        private void OnDisable()
        {
            _pControls.Disable();
        }
    }
}