﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedParticles : MonoBehaviour
{
    [SerializeField] private float _changeSpeed;
    [SerializeField] private ParticleSystem[] _speedParticleGroup;
    
    private IFovValue _fovValue;
    private ParticleSystem _subPart;
    private ParticleSystem.EmissionModule[] _emission;
    // Start is called before the first frame update
    void Start()
    {
        _fovValue = GetComponent<IFovValue>();
        _emission = new ParticleSystem.EmissionModule[_speedParticleGroup.Length];
        for (int i = 0; i < _speedParticleGroup.Length; i++)
        {
            _emission[i] = _speedParticleGroup[i].emission;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < _speedParticleGroup.Length; i++)
        {
            _emission[i].rateOverTime = 200 * _fovValue.LerpValue;
        }
    }
}
