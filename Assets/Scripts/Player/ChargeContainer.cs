﻿using System.Collections;
using System.Collections.Generic;
using Interactables.Individuals;
using UnityEngine;

namespace Player
{
    public class ChargeContainer : MonoBehaviour
    {
        private List<PhotonCharge> _pickedUpCharges;

        public List<PhotonCharge> PickedUpCharge => _pickedUpCharges;

        private void Awake()
        {
            _pickedUpCharges = new List<PhotonCharge>();
        }

        public void AddNewCharge(PhotonCharge charge)
        {
            _pickedUpCharges.Add(charge);
        }
    }
}