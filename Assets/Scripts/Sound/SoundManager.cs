﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

/// <summary>
/// Has methods PlaySound to be used from sound related scripts
/// </summary>
public class SoundManager
{
    private float _defaultVol;
    private float _lowVol;

    private void Start()
    {
        _defaultVol = 1f;
        _lowVol = _defaultVol / 11f;
    }

    /// <summary>
    /// Plays a sound
    /// </summary>
    /// <param name="clip"> Accepts an audioclip to play </param>
    public void PlaySound(AudioClip clip, AudioSource source)
    {
        source.PlayOneShot(clip);
    }

    /// <summary>
    /// Plays a sound, assigns volume, has its own audiosource,
    /// Resets volume after playing
    /// </summary>
    /// <param name="clip"> Accepts an audioclip to play </param>
    /// <param name="clip"> Accepts a float to adjust volume </param>
    /// <param name="clip"> Accepts an audio source </param>
    public void PlaySound(AudioClip clip, float vol, AudioSource source)
    {
        source.volume = vol;
        source.PlayOneShot(clip);
    }

    /// <summary>
    /// Plays a sound and loop unless it's order to stop
    /// </summary>
    /// <param name="clip"> Accepts an audioclip to play </param>
    /// <param name="clip"> Accepts a bool to know when to stop looping </param>
    public void PlaySound(AudioClip clip, bool loopState, AudioSource source)
    {
        if (loopState)
        {
            if (source.clip != clip || source.clip == null)
            {
                source.clip = clip;
            }

            source.loop = loopState;
            source.Play();
        }
        else if (!loopState)
        {
            source.loop = false;
        }
    }

    public void PlaySound()
    {

    }
}
