﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class PlayVFX : MonoBehaviour
{
    [SerializeField] private AudioClip _clip;
    private AudioSource _source;
    private SoundManager _soundManager;

    private AudioMixerGroup _mixerGroup;

    private void Awake() 
    {
        _soundManager = new SoundManager();
        _source = GetComponent<AudioSource>();
        AudioMixer mixer = Resources.Load<AudioMixer>("Audio/ReverbPlayer");
        _mixerGroup = mixer.FindMatchingGroups("PhotonReverb")[0];
    }

    public void PlaySound()
    {
        _soundManager.PlaySound(_clip, _source);
    }
}
