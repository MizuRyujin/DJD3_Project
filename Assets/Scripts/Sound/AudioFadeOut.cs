﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Class with method that fades out sound
/// </summary>
public class AudioFadeOut
{
    /// <summary>
    /// Fades out a sound from a given audio source
    /// </summary>
    /// <param name="audioSource">
    /// Accepts an audio source to fade it's volume 
    /// </param>
    /// <param name="FadeTime"> Accepts a float to determine fade time </param>
    /// <returns></returns>
    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            Debug.Log("I'm being called");

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }
}
