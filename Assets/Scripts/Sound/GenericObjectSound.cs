﻿using UnityEngine;
using Galaxy;
using UnityEngine.Audio;
using Interactables.Individuals;
using PuzzleComponents;

/// <summary>
/// Generic object sounds, sets up an audiosource to loop a sound and has an
/// on trigger sound to be played
/// </summary>
public class GenericObjectSound : MonoBehaviour
{
    [Header("Sound Specs")]
    [SerializeField] private float _minDistance;
    [SerializeField] private float _maxDistance;
    [SerializeField] private float _volume;
    [SerializeField] private bool _repeatAfterTrigger;

    [Header("Sounds")]
    [SerializeField] private AudioClip _onAwakeLoopSound;
    [SerializeField] private AudioClip _onTriggerSound;

    private AudioSource _audioSource;
    private SoundManager _soundManager;

    private AudioMixer _audioMixer;
    private AudioMixerGroup _reverbMixer;

    /// <summary>
    /// Setup audio source, so that it plays the object's sound on loop
    /// </summary>
    private void Awake()
    {
        _soundManager = new SoundManager();
        _audioSource = new AudioSource();

        _audioMixer = Resources.Load<AudioMixer>("Audio/ReverbPlayer");
        _reverbMixer = _audioMixer.FindMatchingGroups("PhotonReverb")[0];

        SetUpAudioSourceLoop();
    }

    private void Start()
    {
        PhotonCharge charge;
        InteractionGobble gobble;
        GooDoor door;

        if (TryGetComponent<PhotonCharge>(out charge))
        {
            charge.OnHit += OnHit;
            _volume = 0.05f;
        }
        else if (TryGetComponent<InteractionGobble>(out gobble))
        {
            gobble.OnHit += OnHit;
            _volume = 1f;
        }
        else if (TryGetComponent<GooDoor>(out door))
        {
            door.onDoorOpen += OnHit;
            door.onDoorClose += OnHit;
        }
    }

    /// <summary>
    /// Colides with player, plays a sound and hopefully the object will disappear
    /// </summary>
    private void OnTriggerEnter()
    {

    }

    private void OnHit()
    {
        _audioSource.loop = false;
        _audioSource.Stop();

        _soundManager.PlaySound(_onTriggerSound, _audioSource);

        // If it's supposed to, it will continue background sound of object
        if (_repeatAfterTrigger)
        {
            SetUpAudioSourceLoop();
        }
    }

    /// <summary>
    /// Sets up audio source in order to loop its background sound
    /// </summary>
    private void SetUpAudioSourceLoop()
    {
        if (_audioSource == null)
        {
            _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.outputAudioMixerGroup = _reverbMixer;
            _audioSource.volume = _volume;
            _audioSource.minDistance = _minDistance;
            _audioSource.maxDistance = _maxDistance;
            _audioSource.spatialBlend = 1f;
        }

        _audioSource.clip = _onAwakeLoopSound;
        _audioSource.Play();
        _audioSource.loop = true;
    }
}
