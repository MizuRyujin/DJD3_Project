﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class BombSound : MonoBehaviour
{
    [Header("Sound Specs")]
    [SerializeField] private float _minDistance;
    [SerializeField] private float _maxDistance;
    [SerializeField] private float _volume;

    [Header("Sounds")]
    [SerializeField] private AudioClip _loopSound;
    [SerializeField] private AudioClip _awakeSound;

    private AudioSource _audioSource;
    private SoundManager _soundManager;

    private AudioMixer _audioMixer;
    private AudioMixerGroup _reverbMixer;

    private bool _playedOnAwake;
    private int _timer;

    /// <summary>
    /// Setup audio source, so that it plays the object's sound on loop
    /// </summary>
    private void Awake()
    {
        _playedOnAwake = true;
        _soundManager = new SoundManager();
        _audioSource = new AudioSource();

        _audioMixer = Resources.Load<AudioMixer>("Audio/ReverbPlayer");
        _reverbMixer = _audioMixer.FindMatchingGroups("PhotonReverb")[0];

        SetUpAudioSource();
    }

    private void SetUpAudioSource()
    {
        if (_audioSource == null)
        {
            _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.outputAudioMixerGroup = _reverbMixer;
            _audioSource.volume = _volume;
            _audioSource.minDistance = _minDistance;
            _audioSource.maxDistance = _maxDistance;
            _audioSource.spatialBlend = 1f;
        }

        _audioSource.clip = _awakeSound;
        _audioSource.Play();
        _audioSource.loop = false;
    }

    // Update is called once per frame
    private void Update()
    {
        // Play loop sound
        if (_playedOnAwake && _timer <= 0)
        {
            LoopAudioSource();
        }


    }

    /// <summary>
    /// Loops audiosource
    /// </summary>
    private void LoopAudioSource()
    {
        _audioSource.clip = _loopSound;
        _audioSource.Play();
        _audioSource.loop = true;

        _playedOnAwake = false;
    }
}
