﻿using UnityEngine;
using UnityEngine.Audio;

public class PlayerSounds : MonoBehaviour
{
    [Header("Player Sounds")]
    [SerializeField] private AudioClip _liftOf;       // Press Sound
    [SerializeField] private AudioClip _jumpDrop;     // Collision/Trigger
    [SerializeField] private AudioClip _move;         // Press + continuous
    [SerializeField] private AudioClip _photonThrust; // Press Sound
    [SerializeField] private AudioClip _interaction;  // Press Sound

    private PlayerControllerActions _pControls;       // Sound Input System
    private SoundManager _soundManager;

    private AudioSource _audioSource;
    private AudioSource _moveAudiosource;

    private AudioMixer _audioMixer;
    private AudioMixerGroup _reverbMixer;

    private float _audioVolume;
    private float _moveVolume;

    private bool _onPlanet;
    private bool _onGround;
    private bool _moving;
    private bool _jumping;
    private bool _dashing;

    /// <summary>
    /// Init input system, assign variables
    /// </summary>
    private void Awake()
    {
        _onPlanet = true;
        _onGround = true;
        _moving = false;
        _dashing = false;
        _jumping = false;

        _audioVolume = 0.5f;
        _moveVolume = 0.04f;

        _pControls = new PlayerControllerActions();
        _soundManager = new SoundManager();

        _audioMixer = Resources.Load<AudioMixer>("Audio/ReverbPlayer");
        _reverbMixer = _audioMixer.FindMatchingGroups("Reverb")[1];
    
        _audioSource = gameObject.AddComponent<AudioSource>();
        _moveAudiosource = gameObject.AddComponent<AudioSource>();

        _audioSource.volume = _audioVolume;
        _moveAudiosource.volume = _moveVolume;

        _audioSource.outputAudioMixerGroup = _reverbMixer;
        _moveAudiosource.outputAudioMixerGroup = _reverbMixer;

        SetUpActions();
    }

    /// <summary>
    /// Play move sound depending on moving or not
    /// </summary>
    /// <param name="moving"> Accepts a bool to manage if moving or not </param>
    private void MoveSound()
    {
        _moving = !_moving;

        if (_onPlanet && _onGround)
        {
            _soundManager.PlaySound(_move, _moving, _moveAudiosource);
        }
    }

    /// <summary>
    /// Plays Jump or Dash sound depending on wether is on planet or not 
    /// </summary>
    /// <param name="started">
    /// Accepts a bool, if it's true,
    /// then it will act as if player started pressing the button
    /// if it's false it will act as if player let go of the button 
    /// </param>
    private void JumpOrDashSound(bool started = true, string type = "jump")
    {
        //// If player is on planet and pressed jump
        //if (_onPlanet && started && _onGround)
        //{
        //    _jumping = true;
        //} 

        _soundManager.PlaySound(_photonThrust,
                    Random.Range(0.07f, 0.09f), _audioSource);

        _soundManager.PlaySound(_liftOf, _audioSource);

        if (type == "dash")
        {
            if(!_dashing)
            {
                

                _dashing = true;
            }
        }

        _audioSource.volume = _audioVolume;
    }

    /// <summary>
    /// Manage collisions (on trigger enter) to trigger sounds
    /// </summary>
    private void OnTriggerEnter()
    {
        Debug.Log("Colided");

        if (_jumping || !_onPlanet)
        {
            // If jumped and is droping
            _soundManager.PlaySound(_jumpDrop, Random.Range(1f, 1.5f),
                _audioSource);
            _onPlanet = true;
            _onGround = true;
            _jumping = false;
            _dashing = false;
        }

        // Colide with planet from outer space
    }

    /// <summary>
    /// Sets up Actions in PlayerSounds
    /// </summary>
    private void SetUpActions()
    {
        // Jump or dash sounds will change either player is pressing or not
        // And either player is on planet or not
        //_pControls.PlayerOnPressSounds.JumpSound.started += ctx
        //    => JumpOrDashSound(true);
        //_pControls.PlayerOnPressSounds.JumpSound.canceled += ctx
        //    => JumpOrDashSound(false);

        // Make movement sound loop while player is moving
        _pControls.PlayerOnPressSounds.MoveSound.performed += ctx => MoveSound();

        _pControls.PlayerOnPressSounds.InteractionSound.performed += ctx
            => _soundManager.PlaySound(_interaction, Random.Range(0.05f, 0.1f), _audioSource);

        _pControls.PlayerOnPressSounds.PhotonThrustSound.started += ctx
            => JumpOrDashSound();
    }

    /// <summary>
    /// Enable sound actions
    /// </summary>
    private void OnEnable()
    {
        _pControls.PlayerOnPressSounds.PrepareForFlightSound.Enable();
        _pControls.PlayerOnPressSounds.JumpSound.Enable();
        _pControls.PlayerOnPressSounds.PhotonThrustSound.Enable();
        _pControls.PlayerOnPressSounds.MoveSound.Enable();
        _pControls.PlayerOnPressSounds.InteractionSound.Enable();

        _pControls.Enable();
    }

    /// <summary>
    /// Disable sound actions
    /// </summary>
    private void OnDisable()
    {
        _pControls.PlayerOnPressSounds.PrepareForFlightSound.Disable();
        _pControls.PlayerOnPressSounds.JumpSound.Disable();
        _pControls.PlayerOnPressSounds.PhotonThrustSound.Disable();
        _pControls.PlayerOnPressSounds.MoveSound.Disable();
        _pControls.PlayerOnPressSounds.InteractionSound.Disable();

        _pControls.Disable();
    }
}
