using System;
using System.Collections.Generic;
using Galaxy.CelestialObjects;
using Galaxy.CelestialObjects.Planets;
using Galaxy.Generation;
using Interactables;
using Interactables.Individuals;
using UnityEngine;

namespace Galaxy
{
    public class PlanetarySystem : MonoBehaviour
    {
        // Planet orbit help variable
        [SerializeField] private bool _doOrbit = true;
        [Space]
        // Variable to determine if the system was generated
        [Tooltip("This parameter tells if it was procedurally generated")]
        [SerializeField] private bool _generated = false;
        // System seed
        [SerializeField] private int _generatorSeed = default;
        /// <summary>
        /// All the planets within this system
        /// </summary>
        /// <value>All the planets</value>
        public List<Planet> Planets { get; private set; }

        // System core reference helper variable
        [SerializeField] private SystemCore _core;
        /// <summary>
        /// System core reference
        /// </summary>
        /// <value>System core</value>
        public SystemCore Core
        {
            get => _core;
            private set { _core = value; }
        }
        // Planetary properties
        [SerializeField] private PlanetarySysProperties _properties;
        /// <summary>
        /// Planetary system properties
        /// </summary>
        /// <value>Properties</value>
        public PlanetarySysProperties Properties
        {
            get => _properties;
            private set { _properties = value; }
        }
        // Planets in system
        [SerializeField] private Planet[] _planets = default;

        [Tooltip("Automatically finds the charges")]
        // Charges in system
        [SerializeField] private PhotonCharge[] _chargesInSystem = default;
        /// <summary>
        /// All the charges in this planetary system
        /// </summary>
        public PhotonCharge[] PhotonCharges => _chargesInSystem;

        [Tooltip("Automatically finds the discoveries")]
        // Discoveries in system
        [SerializeField] private PlanetaryDiscovery[] _planetaryDiscoveries = default;
        /// <summary>
        /// All the discoveries in this system
        /// </summary>
        public PlanetaryDiscovery[] PlanetaryDiscoveries => _planetaryDiscoveries;

        /// <summary>
        /// Amount of planets
        /// </summary>
        /// <value>Amount</value>
        public int AmountOfPlanets { get; private set; }
        /// <summary>
        /// Amount of satelites
        /// </summary>
        /// <value>Amount</value>
        public int AmountOfSatellites { get; private set; }
        /// <summary>
        /// Elapsed time since creation
        /// </summary>
        public float ElapsedTimeSinceCreation => Time.time - _timeOfCreation;
        /// <summary>
        /// Elapsed time while the player is in it
        /// </summary>
        /// <value>Elapsed time</value>
        public float ElapsedActiveTime { get; private set; }
        /// <summary>
        /// Is counting the active time
        /// </summary>
        /// <value>Is counting</value>
        public bool IsCountingTime { get; private set; }

        // Time of system creation (not generated time)
        private float _timeOfCreation;

        /// <summary>
        /// Know the amount of discoveries the player has found
        /// </summary>
        /// <value>amount in percentage (min.0f - max.1f)</value>
        public float DiscoveriesFoundPercentage
        {
            get
            {
                float finalValue = 0;
                float totalFound = 0;
                float total = 0;
                
                for (int i = 0; i < PlanetaryDiscoveries.Length; i++)
                {
                    PlanetaryDiscovery d = PlanetaryDiscoveries[i];
                    total += d.Weight;
                    if (d.Found) totalFound += d.Weight;
                }

                finalValue = totalFound > 0 ? totalFound / total : 0;

                return finalValue;
            }
        }

        public int PhotonChargesCollected
        {
            get
            {
                if(DevButtons.AllCharges) return 100;
                
                int amount = 0;
                for (int i = 0; i < PhotonCharges.Length; i++)
                {
                    if(_chargesInSystem[i].ChargeCollected) amount++;
                }
                return amount;
            }
        }

        // Initialize all variables
        private void Awake()
        {
            Planets = new List<Planet>();
            AmountOfPlanets = 0;
            AmountOfSatellites = 0;

            FindAllCharges();
            FindAllDiscoveries();

            if (_generated)
            {
                foreach (Planet p in _planets)
                    Planets.Add(p);
                FinalizeCreation();
            }
        }

        // Update the behaviours
        private void Update()
        {
            UpdateAction?.Invoke();
        }

        // Count the active time
        private void DoCount()
        {
            ElapsedActiveTime += Time.deltaTime;
        }

        // Find all charges in the system
        private void FindAllCharges()
        {
            //Find all charges
            _chargesInSystem = Resources.FindObjectsOfTypeAll<PhotonCharge>();
            List<PhotonCharge> chargeList = new List<PhotonCharge>(3);

            // Check if they are on scene
            foreach (PhotonCharge c in _chargesInSystem)
                if (c.gameObject.scene.name != null)
                    chargeList.Add(c);

            // Convert them to array
            _chargesInSystem = chargeList.ToArray();
        }

        // Find all discoveries in the system
        private void FindAllDiscoveries()
        {
            //Find all
            _planetaryDiscoveries = Resources.FindObjectsOfTypeAll<PlanetaryDiscovery>();
            List<PlanetaryDiscovery> discoveryList = new List<PlanetaryDiscovery>(5);

            // Check if they are on scene
            foreach (PlanetaryDiscovery c in _planetaryDiscoveries)
                if (c.gameObject.scene.name != null)
                    discoveryList.Add(c);

            // Convert them to array
            _planetaryDiscoveries = discoveryList.ToArray();
        }

        // Toggle time count on or off (This feature did not have time to be 
        // properly implemented)
        public void ToggleTimeCount(bool active)
        {
            if (IsCountingTime == active) return;
            IsCountingTime = active;
            if (IsCountingTime)
                UpdateAction += DoCount;
            else
                UpdateAction -= DoCount;
        }

        // Set generated properties
        public void SetGalaxyProperties(PlanetarySysProperties properties, int seed)
        {
            Properties = properties;
            _generated = true;
            _generatorSeed = seed;
        }

        // Called by the generator when a new planet is created
        public void PlanetCreated(Planet newPlanet)
        {
            if (Planets == null)
                Planets = new List<Planet>();

            Planets.Add(newPlanet);

            AmountOfPlanets++;
            if (newPlanet.HasSatellites) AmountOfSatellites++;

            int size = Planets.Count;
            _planets = new Planet[size];
            for (int i = 0; i < size; i++)
                _planets[i] = Planets[i];
        }

        // Set the core
        public void SetCore(SystemCore core)
        {
            Core = core;
        }

        // Finalizes creation after generation
        public void FinalizeCreation()
        {
            _timeOfCreation = Time.time;
            ElapsedActiveTime = 0;
            IsCountingTime = false;

            ToggleTimeCount(true);
        }

        // Update action
        Action UpdateAction;
    }
}