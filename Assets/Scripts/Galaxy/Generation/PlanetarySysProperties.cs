﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxy.Generation
{
    [CreateAssetMenu(menuName = "System/Generation/Properties")]
    public class PlanetarySysProperties : ScriptableObject
    {
        [Header("System Properties")]
        [Range(0,1)] [Tooltip("0% to 100%")]
        [SerializeField] private int _celestialLayerNum = 13;
        [SerializeField] private Vector2Int _asteroidsBetweenPlanetsRange = new Vector2Int(0, 10);

        [Header("DEV Generation")]
        [SerializeField] private bool _coolMode = false;
        [SerializeField] private bool _randomizeInitialPositions = true;
        [SerializeField] private bool _oscillateOnY = true;

        [Header("Planet Generation")]
        [SerializeField] private byte _maxPlanets = 15;
        [SerializeField] private byte _minPlanets = 5;
        [SerializeField] private float _maxDistanceBetweenPlanets = 200;
        [SerializeField] private float _minDistanceBetweenPlanets = 120;
        [Range(0, 1)]
        [SerializeField] private float _newPlanetChance = .8f;

        [Header("Satellite Generation")]
        [SerializeField] private int _maxSystemSatellites = 20;
        [Range(0, 1)]
        [SerializeField] private float _noneChance = .8f;
        [Range(0, 1)]
        [SerializeField] private float _moonChance = .5f;
        [Range(0, 1)]
        [SerializeField] private float _ringChance = .2f;

        [Header ("Prefab Generation")]
        [SerializeField] private GameObject[] _planets = null;
        [SerializeField] private GameObject[] _moons = null;
        [SerializeField] private GameObject[] _asteroids = null;
        [SerializeField] private GameObject[] _bosses = null;
        [SerializeField] private GameObject _ring = null;
        [SerializeField] private Material[] _ringMaterials = null;


        // Getters
        public byte MaxPlanets => _maxPlanets;
        public byte MinPlanets => _minPlanets;
        public float MaxDistanceBetweenPlanets => _maxDistanceBetweenPlanets;
        public float MinDistanceBetweenPlanets => _minDistanceBetweenPlanets;
        public float NewPlanetChance => _newPlanetChance;

        public int MaxSatellites => _maxSystemSatellites;
        public float NoneChance => _noneChance;
        public float MoonChance => _moonChance;
        public float RingChance => _ringChance;
        
        public GameObject[] Planets => _planets;
        public GameObject[] Moons => _moons;
        public GameObject Ring => _ring;
        public GameObject[] Asteroids => _asteroids;
        public GameObject[] Bosses => _bosses;
        public Material[] RingMaterials => _ringMaterials;

        public bool CoolMode => _coolMode;
        public bool RandomizeInitials => _randomizeInitialPositions;
        public bool YOscillation => _oscillateOnY;

        public int Layer => _celestialLayerNum;

        public Vector2Int AsteroidsBetweenPlanetsRange => _asteroidsBetweenPlanetsRange;
    }
}