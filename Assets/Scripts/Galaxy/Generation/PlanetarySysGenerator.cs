﻿#if UNITY_EDITOR
using Galaxy.CelestialObjects;
using Galaxy.CelestialObjects.Planets;
using Galaxy.CelestialObjects.Satellites;
using UnityEditor;
using UnityEngine;

namespace Galaxy.Generation
{
    public class PlanetarySysGenerator : MonoBehaviour
    {
        private const float _MAX_CORE_SCALE = 450;
        private const float _MIN_CORE_SCALE = 550;

        private const float _MAX_PLANET_SCALE = 70;
        private const float _MIN_PLANET_SCALE = 30;

        private const float _MAX_MOON_SCALE = 7f;
        private const float _MIN_MOON_SCALE = 15;

        [Tooltip("Set to 0 to ignore seed")]
        [SerializeField] private int _seed = 0;
        [SerializeField] private PlanetarySysProperties _systemProperties = null;
        [SerializeField] private PlanetarySystem _sysScript = null;
        [SerializeField] private CelestialObject _targetCelestial = null;
        [SerializeField] private GameObject _sysObject = null;
        [SerializeField] private GameObject _asteroidsObj = null;

        [SerializeField] private bool _useCustomSeed = false;
        private Transform _core = null;

        // Get a random radius from the given distance
        private float RandomRadius => Random.Range(
            _systemProperties.MinDistanceBetweenPlanets,
            _systemProperties.MaxDistanceBetweenPlanets);

        [SerializeField] private string _gInfo = "";
        public string GalaxyInfo => _gInfo;

        public void NewSystem()
        {
            int initialSeed = _seed;

            if (!_useCustomSeed)
                _seed = Random.Range(int.MinValue, int.MaxValue);

            Random.InitState(_seed);

            DestroyCurrent();

            CreateSystemObject();
            Generate();

            UpdateInfo(_sysScript);

            if (!_useCustomSeed) ResetSeed();
        }
        public void DestroyCurrent()
        {
            if (_sysObject != null) DestroyImmediate(_sysObject);
        }

        private void Generate()
        {
            // Add the system core (Boss)
            AddCore();

            // Start off with the first planet
            Populate(_core.transform.position, 0, _systemProperties
                .MaxDistanceBetweenPlanets * 2 + _MAX_CORE_SCALE * 2, 1,
                GetRandomDir());

            _sysScript.FinalizeCreation();
        }

        public void ResetSeed() => _seed = 0;

        //

        private void AddCore()
        {
            _core = InstantiatePrefabFromArray(_systemProperties.Bosses).transform;
            _core.name = $"System Core";

            OrbitInfo outerOrbitInfo = OrbitInfo.NoOrbit;
            OrbitInfo selfOrbitInfo = GetRandomSelfOrbitInfo(_core.gameObject, new Vector2(0.5f, 1f));

            SystemCore cObj = _core.gameObject.AddComponent<SystemCore>();

            cObj.Setup(
                _sysScript,
                outerOrbitInfo,
                selfOrbitInfo,
                0,
                CelestialType.Core_Star,
                Random.Range(_MIN_CORE_SCALE, _MAX_CORE_SCALE),
                0);

            cObj.ParentHierarchy.SetParent(_sysObject.transform);
            cObj.gameObject.layer = _systemProperties.Layer;
            _sysScript.SetCore(cObj);
        }

        // Recursive method adds Planets, moons and Rings to the system
        private CelestialObject Populate(
            Vector3 center,
            float previousRadius,
            float addedNewRadius,
            int planetCount,
            int previousDir)
        {
            // Should a new planet spawn?
            if ((Random.Range(.0f, 1.0f) > _systemProperties.NewPlanetChance ||
                    planetCount > _systemProperties.MaxPlanets) &&
                planetCount > _systemProperties.MinPlanets)
                return null;

            // Initialize position
            Vector3 position = Vector3.zero;

            // Get new radius
            float radius = previousRadius + addedNewRadius;
            position.z = radius;

            // Create object from prefab
            GameObject newPlanetObj =
                InstantiatePrefabFromArray(_systemProperties.Planets);

            // Initialize the object
            newPlanetObj.name = $"Planet {planetCount}";
            newPlanetObj.transform.position = position;

            // Get orbits
            OrbitInfo outerOrbitInfo =
                GetRandomOrbitToCenterInfo(previousDir, new Vector2(2f, 4f));
            OrbitInfo selfOrbitInfo =
                GetRandomSelfOrbitInfo(newPlanetObj.gameObject, new Vector2(1f, 3f));

            // Give the planet script
            Planet planet = newPlanetObj.AddComponent<Planet>();

            // Setup the planet
            planet.Setup(
                _sysScript,
                outerOrbitInfo,
                selfOrbitInfo,
                planetCount,
                CelestialType.Planet,
                Random.Range(_MIN_PLANET_SCALE, _MAX_PLANET_SCALE),
                radius);

            // Try add the satellite
            Satellite planetSatellite;
            if (TryCreateSatellite(planet, out planetSatellite))
                planet.AddSatellite(planetSatellite);

            // Should planets be unaligned?
            if (_systemProperties.YOscillation && !_systemProperties.CoolMode)
                DoPlanetYOscillation(planet);

            // Should the position be randomized around the outer orbit?
            if (_systemProperties.RandomizeInitials)
                RandomizeInitialPosition(planet);

            // Generate gap asteroids
            GenerateAsteroids(previousRadius, addedNewRadius);

            // Next celestial
            CelestialObject next =
                Populate(center, radius, RandomRadius, planetCount + 1, -previousDir);

            // Let the system know it has a new planet
            _sysScript.PlanetCreated(planet);

            // Parent the planet to the system hierarchy
            planet.ParentHierarchy?.SetParent(_sysObject.transform);

            // Set the layer
            planet.gameObject.layer = _systemProperties.Layer;

            // Return created
            return planet;
        }

        private bool TryCreateSatellite(CelestialObject target,
            out Satellite addedSatellite)
        {
            addedSatellite = null;
            Vector3 position = target.transform.position;
            OrbitInfo outerOrbitInfo = OrbitInfo.NoOrbit;
            OrbitInfo selfOrbitInfo = OrbitInfo.NoOrbit;
            CelestialType type = GetRandomSatelliteType();
            float radius = 0;
            float size = 1;

            switch (type)
            { //_systemProperties.Moons
                case CelestialType.Moon:
                    addedSatellite = InstantiatePrefabFromArray(_systemProperties.Moons)
                        .AddComponent<Moon>();
                    addedSatellite.name = "Moon";
                    radius = RandomRadius / 2;
                    position.z += radius;
                    addedSatellite.transform.position = position;
                    size = Random.Range(_MIN_MOON_SCALE, _MAX_MOON_SCALE);
                    addedSatellite.gameObject.layer = _systemProperties.Layer;

                    outerOrbitInfo = new OrbitInfo(
                        target.transform, GetRandomRotationVector(true),
                        Random.Range(15f, 25), GetRandomDir());
                    selfOrbitInfo = GetRandomSelfOrbitInfo(
                        addedSatellite.gameObject,
                        new Vector2(1f, 3f));
                    break;

                case CelestialType.Ring:
                    addedSatellite = InstantiatePrefab(_systemProperties.Ring)
                        .AddComponent<Ring>();
                    addedSatellite.GetComponent<Renderer>().material =
                        _systemProperties.RingMaterials[Random.Range(0, _systemProperties.RingMaterials.Length)];
                    addedSatellite.name = "Ring";
                    addedSatellite.transform.position = position;
                    addedSatellite.transform.localScale *= target.Size;
                    addedSatellite.transform.Rotate(GetRandomRotationVector(),
                        Random.Range(-100, 100));

                    selfOrbitInfo = new OrbitInfo(addedSatellite.transform,
                        Vector3.zero, Random.Range(10f, 25f),
                        GetRandomDir());
                    break;
            }

            addedSatellite?.SetParentBody(target);
            addedSatellite?.Setup(_sysScript, outerOrbitInfo, selfOrbitInfo, 0, type, size, radius);

            return addedSatellite != null;
        }

        // Used to create a moon at will, could be generic, but due to extra
        // work required on the custom inspector, this will have to do
        public void CreateMoon()
        {
            if (_targetCelestial == null)
            {
                Debug.LogWarning("B0ss, gib target celestial or else.");
                return;
            }
            Satellite addedMoon = default;
            Vector3 position = _targetCelestial.transform.position;
            OrbitInfo outerOrbitInfo = OrbitInfo.NoOrbit;
            OrbitInfo selfOrbitInfo = OrbitInfo.NoOrbit;
            CelestialType type = CelestialType.Moon;
            float radius = 0;
            float size = 1;

            addedMoon = InstantiatePrefabFromArray(_systemProperties.Moons)
                .AddComponent<Moon>();
            addedMoon.name = "Moon";
            radius = Random.Range(_systemProperties.MinDistanceBetweenPlanets,
                _systemProperties.MaxDistanceBetweenPlanets) / 1.3f;
            position.z += radius;
            addedMoon.transform.position = position;
            size = Random.Range(_MIN_MOON_SCALE, _MAX_MOON_SCALE);
            addedMoon.gameObject.layer = _systemProperties.Layer;

            outerOrbitInfo = new OrbitInfo(
                _targetCelestial.transform, GetRandomRotationVector(true),
                Random.Range(15f, 25), GetRandomDir());
            selfOrbitInfo = GetRandomSelfOrbitInfo(
                addedMoon.gameObject,
                new Vector2(1f, 3f));

            addedMoon.SetParentBody(_targetCelestial);
            addedMoon.Setup(_sysScript, outerOrbitInfo, selfOrbitInfo, 0, type, size, radius);

            RandomizeInitialPosition(addedMoon);
        }

        private void GenerateAsteroids(float previousRadius, float addedNewRadius)
        {
            int chosenNum = Random.Range(
                _systemProperties.AsteroidsBetweenPlanetsRange.x,
                _systemProperties.AsteroidsBetweenPlanetsRange.y + 1);

            Vector2 radiusRange = new Vector2(previousRadius + (previousRadius * 0.1f),
                (previousRadius + addedNewRadius) - ((previousRadius + addedNewRadius) * 0.1f));

            for (int i = 0; i < chosenNum; i++)
            {
                Asteroid newAsteroid =
                    InstantiatePrefabFromArray(_systemProperties.Asteroids)
                    .AddComponent<Asteroid>();

                float z = Random.Range(radiusRange.x, radiusRange.y);
                newAsteroid.transform.position = new Vector3(0, 0, z);

                OrbitInfo outer =
                    GetRandomOrbitToCenterInfo(Random.Range(0.0f, 1.0f) >= 0.5f ? 1 : -1,
                        new Vector2(15f, 25f), true);
                OrbitInfo self =
                    GetRandomSelfOrbitInfo(newAsteroid.gameObject, new Vector2(5.0f, 10.0f));

                newAsteroid.transform.SetParent(_asteroidsObj.transform);

                newAsteroid.Setup(_sysScript, outer, self, 0,
                    CelestialType.Asteroid, Random.Range(1.0f, 1.6f), z);

                RandomizeInitialPosition(newAsteroid);
            }
        }

        private void DoPlanetYOscillation(CelestialObject cObj)
        {
            int timeRange = _systemProperties.CoolMode ? 1000 : 5;
            cObj.ParentOrbit.transform.RotateAround(
                cObj.OuterOrbitInfo.Target.position,
                Vector3.right,
                Random.Range(-timeRange, timeRange));
        }

        private void RandomizeInitialPosition(CelestialObject cObj)
        {
            cObj.ParentOrbit.transform.RotateAround(
                cObj.OuterOrbitInfo.Target.position,
                cObj.OuterOrbitInfo.Vector,
                Random.Range(-720, 720));
        }

        private OrbitInfo GetRandomOrbitToCenterInfo(int previousDir, Vector2 speedRange, bool force360 = false) =>
            new OrbitInfo(_core, _systemProperties.CoolMode || force360 ?
                GetRandomRotationVector() : Vector3.up,
                Random.Range(speedRange.x, speedRange.y), previousDir);

        private OrbitInfo GetRandomSelfOrbitInfo(GameObject target, Vector2 speedRange) =>
            new OrbitInfo(target.transform, GetRandomRotationVector(),
                Random.Range(speedRange.x, speedRange.y), GetRandomDir());

        private GameObject InstantiatePrefabFromArray(GameObject[] array) =>
            PrefabUtility.InstantiatePrefab(
                array[Random.Range(0, array.Length)]) as GameObject;
        private GameObject InstantiatePrefab(GameObject obj) =>
            PrefabUtility.InstantiatePrefab(obj) as GameObject;

        private int GetRandomDir()
        {
            int dir;

            do
            {
                dir = Random.Range(-1, 2);
            } while (dir == 0);

            return dir;
        }

        private Vector3 GetRandomRotationVector(bool lockZ = false)
        {
            float x;
            float y;
            float z;

            x = Random.Range(-1f, 1f);
            y = Random.Range(-1f, 1f);
            z = lockZ ? 0 : Random.Range(-1f, 1f);

            return new Vector3(x, y, z);
        }

        private CelestialType GetRandomSatelliteType()
        {
            CelestialType chosenType;

            float total =
                _systemProperties.NoneChance +
                _systemProperties.MoonChance +
                _systemProperties.RingChance;

            float noneChance =
                _systemProperties.NoneChance;

            float moonChance =
                noneChance +
                _systemProperties.MoonChance;

            float ringChance =
                moonChance +
                _systemProperties.RingChance;

            float decision = Random.Range(0, total);

            // None chosen
            if (decision <= noneChance)
                chosenType = CelestialType.None;
            // Moon chosen
            else if (decision <= moonChance)
                chosenType = CelestialType.Moon;
            // Ring chosen
            else
                chosenType = CelestialType.Ring;

            return chosenType;
        }

        private void CreateSystemObject()
        {
            _sysObject = new GameObject($"System ({_seed})");
            _sysScript = _sysObject.AddComponent<PlanetarySystem>();
            _sysScript.SetGalaxyProperties(_systemProperties, _seed);

            _asteroidsObj = new GameObject("Asteroids");
            _asteroidsObj.transform.SetParent(_sysObject.transform);
        }

        private void UpdateInfo(PlanetarySystem system)
        {
            string final = "";
            final += $"## CURRENT PLANETARY SYSTEM INFO ##\n\n";
            final += $"SEED: {_seed}\n\n";
            final += $"Planets: {system.AmountOfPlanets}\n";
            final += $"Satellites: {system.AmountOfSatellites}\n\n";

            Planet lastPlanet = _sysScript.Planets[_sysScript.Planets.Count - 1];
            float radius = Vector3.Distance(
                    lastPlanet.transform.position,
                    lastPlanet.OuterOrbitInfo.Target.transform.position) +
                lastPlanet.Size;
            final += $"System Radius: {radius}";
            _gInfo = final;
        }

        private void OnGUI()
        {
            // if (GUI.Button(new Rect(10, 10, 100, 30), "New System"))
            //     NewGalaxy(true);
            // GUI.Label(new Rect(10, 50, 50, 20), $"Seed:");
            // GUI.TextField(new Rect(50, 50, 90, 20), _seed.ToString());
            // GUI.Label(new Rect(10, 70, 58, 80),
            //     $"Planets: {_galaxyScript.AmountOfPlanets} Satellites: {_galaxyScript.AmountOfSatellites}");
        }
    }
}
#endif