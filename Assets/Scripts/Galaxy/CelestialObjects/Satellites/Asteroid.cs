﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxy.CelestialObjects.Satellites
{
    public class Asteroid : CelestialObject
    {
        protected override void Awake()
        {
            base.Awake();
            Behaviour += DoOuterOrbitOverTime;
        }        
    }
}