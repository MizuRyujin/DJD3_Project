using Galaxy.CelestialObjects;
using UnityEngine;

namespace Galaxy.CelestialObjects.Satellites
{
    public abstract class Satellite : CelestialObject
    {
        [SerializeField] private CelestialObject _parentBody;
        public CelestialObject ParentBody { get => _parentBody; private set { _parentBody = value; } }

        public void SetParentBody(CelestialObject parentBody)
        {
            parentBody.SetOrbitalChild(transform);
            ParentBody = parentBody;
        }
    }
}