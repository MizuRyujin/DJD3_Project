﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxy.CelestialObjects.Satellites
{
    public class Moon : Satellite
    {
        protected override void Awake()
        {
            base.Awake();
            Behaviour += DoOuterOrbitOverTime;
        }
    }
}