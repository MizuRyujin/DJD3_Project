﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxy.CelestialObjects.Satellites
{
    public class Ring : Satellite
    {
        protected override void Awake()
        {
            base.Awake();
            
            ParentRotation.localRotation = transform.localRotation;
            transform.localRotation = Quaternion.identity;

            SelfOrbitInfo = new OrbitInfo(SelfOrbitInfo.Target,
                transform.up, SelfOrbitInfo.DegreesPerSecond,
                SelfOrbitInfo.Direction);
        }
    }
}