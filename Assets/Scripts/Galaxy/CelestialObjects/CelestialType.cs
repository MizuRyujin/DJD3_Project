﻿namespace Galaxy.CelestialObjects
{
    public enum CelestialType
    {
        None,
        Planet,
        Moon,
        Asteroid,
        Ring,
        Core_Star
    }
}