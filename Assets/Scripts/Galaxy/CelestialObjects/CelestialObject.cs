using System;
using System.Collections.Generic;
using Galaxy;
using Galaxy.CelestialObjects.Satellites;
using UnityEngine;

namespace Galaxy.CelestialObjects
{
    public abstract class CelestialObject : MonoBehaviour
    {
        [Tooltip("This parameter tells if it was procedurally generated")]
        [SerializeField] private bool _generated = false;
        [Tooltip("Toggle to see the Orbit")]
        [SerializeField] private bool _drawOrbit = false;
        [SerializeField] private Transform _parentHierarchy;
        public Transform ParentHierarchy { get => _parentHierarchy; private set { _parentHierarchy = value; } }

        [SerializeField] private Transform _parentOrbit;
        public Transform ParentOrbit { get => _parentOrbit; private set { _parentOrbit = value; } }

        [SerializeField] private Transform _parentRotation;
        public Transform ParentRotation { get => _parentRotation; private set { _parentRotation = value; } }

        [SerializeField] private CelestialType _type;
        public CelestialType Type { get => _type; private set { _type = value; } }

        [SerializeField] private OrbitInfo _outerOrbitInfo;
        public OrbitInfo OuterOrbitInfo { get => _outerOrbitInfo; private set { _outerOrbitInfo = value; } }

        [SerializeField] private OrbitInfo _selfOrbitInfo;
        public OrbitInfo SelfOrbitInfo { get => _selfOrbitInfo; protected set { _selfOrbitInfo = value; } }

        [SerializeField] private Satellite[] _satellites;
        public List<Satellite> Satellites { get; protected set; }

        [SerializeField] private float _size;
        public float Size { get => _size; private set { _size = value; } }

        [SerializeField] private float _orbitRadius;
        public float OrbitRadius { get => _orbitRadius; private set { _orbitRadius = value; } }

        [SerializeField] private int _id;
        public int ID { get => _id; private set { _id = value; } }

        [SerializeField] private PlanetarySystem _system;
        public PlanetarySystem CurrentSystem { get => _system; private set { _system = value; } }

        public bool HasSatellites => _satellites == null ? false : _satellites.Length > 0;

        protected virtual void Awake()
        {
            Satellites = new List<Satellite>(2);
            if (_generated)
            {
                foreach (Satellite s in _satellites)
                    Satellites.Add(s);
                PrepareBehaviour();
            }
        }

        /// <summary>
        /// Add a new satellite
        /// </summary>
        /// <param name="newS">New satellite</param>
        public void AddSatellite(Satellite newS)
        {
            if (Satellites == null)
                Satellites = new List<Satellite>(2);

            Satellites.Add(newS);
            int size = Satellites.Count;
            _satellites = new Satellite[size];
            for (int i = 0; i < size; i++)
                _satellites[i] = Satellites[i];
        }

        /// <summary>
        /// Does the outer orbit
        /// </summary>
        public void DoOuterOrbit(float elapsedTime)
        {
            ParentOrbit?.RotateAround(
                OuterOrbitInfo.Target.transform.position,
                OuterOrbitInfo.Vector,
                OuterOrbitInfo.DegreesPerSecond * elapsedTime *
                OuterOrbitInfo.Direction);

            ParentOrbit.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// Does the self rotation
        /// </summary>
        private void DoSelfOrbit()
        {
            ParentRotation?.RotateAround(
                SelfOrbitInfo.Target.transform.position,
                SelfOrbitInfo.Vector,
                SelfOrbitInfo.DegreesPerSecond * Time.deltaTime * SelfOrbitInfo.Direction);

            ParentRotation.transform.localPosition = Vector3.zero;
        }

        protected void DoOuterOrbitOverTime()
        {
            DoOuterOrbit(Time.deltaTime);
        }

        /// <summary>
        /// Calls the movement behaviour
        /// </summary>
        protected virtual void LateUpdate()
        {
            Behaviour?.Invoke();
        }

        public virtual void Setup(PlanetarySystem system,
            OrbitInfo outerOrbit, OrbitInfo selfOrbit,
            int id, CelestialType type,
            float size, float orbitRadius)
        {
            OuterOrbitInfo = outerOrbit;
            SelfOrbitInfo = selfOrbit;

            Type = type;
            Size = size;
            OrbitRadius = orbitRadius;
            ID = id;
            CurrentSystem = system;

            PrepareBehaviour();

            CreateHierarchy(id);

            transform.localScale *= size;

            _generated = true;
        }

        public void SetOrbitalChild(Transform t)
        {
            t.SetParent(ParentOrbit);
        }

        public void SetPlanetChild(Transform t)
        {
            t.SetParent(transform);
        }

        private void PrepareBehaviour()
        {
            if (!OuterOrbitInfo.IsNoOrbit())
            {
                //Behaviour += DoOuterOrbit;
            }

            if (!SelfOrbitInfo.IsNoOrbit())
            {
                Behaviour += DoSelfOrbit;
            }
        }

        public virtual GameObject CreateHierarchy(int planetID)
        {
            GameObject parentObj = new GameObject($"{name} Hierarchy");
            parentObj.transform.position = transform.position;
            GameObject planetOrbitObj = new GameObject($"{name} Orbit");
            planetOrbitObj.transform.position = transform.position;
            GameObject planetSelfOrbitObj = new GameObject($"{name} Rotation");
            planetSelfOrbitObj.transform.position = transform.position;

            planetOrbitObj.transform.position = transform.position;

            parentObj.transform.SetParent(transform.parent);
            parentObj.transform.localPosition = Vector3.zero;
            planetOrbitObj.transform.SetParent(parentObj.transform);
            planetSelfOrbitObj.transform.SetParent(planetOrbitObj.transform);
            transform.SetParent(planetSelfOrbitObj.transform);

            ParentHierarchy = parentObj.transform;
            ParentOrbit = planetOrbitObj.transform;
            ParentRotation = planetSelfOrbitObj.transform;

            return planetOrbitObj;
        }

        private void OnDrawGizmos()
        {
            if (!_drawOrbit) return;

            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(Vector3.zero, OrbitRadius);
        }

        protected Action Behaviour;
    }

}