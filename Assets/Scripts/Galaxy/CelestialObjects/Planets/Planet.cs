﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxy.CelestialObjects.Planets
{
    public class Planet : CelestialObject
    {
        // [SerializeField] private bool _doOrbit = true;
        // [SerializeField] private Vector3 _orbitVector = Vector3.up;
        // [SerializeField] private float _degreesPerSecond = 30.0f;
        // [SerializeField] private int _dir = 0;

        private void Start()
        {
            //UpdateAction += DoOuterOrbitOverTime;
        }

        // Update is called once per frame
        void Update()
        {
            UpdateAction?.Invoke();
        }

        public virtual void OnPlayerEnterAtmosphere() { }
        public virtual void OnPlayerExitAtmosphere() { }
        public virtual void OnPlayerTouchdown() { }

        private Action UpdateAction;
    }
}