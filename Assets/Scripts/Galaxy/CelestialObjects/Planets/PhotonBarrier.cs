﻿using UnityEngine;
using Player.Movement;

namespace Galaxy.CelestialObjects.Planets
{
    public class PhotonBarrier : MonoBehaviour
    {
        private CelestialObject _parent;
        private void Awake()
        {
            _parent = GetComponentInParent<CelestialObject>();
        }

        private void OnTriggerEnter(Collider other)
        {
            Player.Player p;
            if (other.TryGetComponent<Player.Player>(out p))
            {
                Debug.Log("Our sperm is trying to reach the golden egg!");
                if (p.ChargeContainer.PickedUpCharge.Count ==
                        _parent.CurrentSystem.PhotonCharges.Length)
                {
                    PlayerMovement.staticBossFight = true;
                    SceneLoader.Load("BossFlexScene");
                    Debug.LogError("Is player in a boss?" + PlayerMovement.staticBossFight);
                }
                else
                {
                    p.ReturnToLastPlanet();
                }
            }
        }
    }
}