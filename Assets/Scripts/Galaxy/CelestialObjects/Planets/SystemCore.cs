﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;

namespace Galaxy.CelestialObjects.Planets
{
    public class SystemCore : Planet
    {
        [Header("Core properties")]
        public Player.Player _player;

        protected Action<Player.Player> _moveOne;
        protected Action<Player.Player> _moveTwo;
        protected Action<Player.Player> _moveThree;
        protected Action<Player.Player> _moveFour;
        protected Action<Player.Player> _moveSpecial;

        protected byte _coreStage;

        public void StartBattle(Player.Player p)
        {
            _player = p;
        }

        protected void NextStage()
        {
            _coreStage++;
        }

        protected void DoMove()
        {
            Action<Player.Player> finalMove = null;
            int decision;

            if (_coreStage == 0)
            {

            }
            else if (_coreStage == 1)
            {

            }
            else if (_coreStage == 2)
            {

            }

            finalMove.Invoke(_player);
        }
    }
}