﻿using UnityEngine;

namespace Galaxy.CelestialObjects
{
    [System.Serializable]
    public struct OrbitInfo
    {
        [SerializeField] private Transform _target;
        public Transform Target { get => _target; }
        [SerializeField] private Vector3 _vector;
        public Vector3 Vector { get => _vector; }
        [SerializeField] private float _degreesPerSecond;
        public float DegreesPerSecond { get => _degreesPerSecond; }
        [SerializeField] private int _direction;
        public int Direction { get => _direction; }

        public static OrbitInfo NoOrbit => new OrbitInfo(null, Vector3.zero, 0, 0);

        public OrbitInfo(Transform target, Vector3 orbitVector,
            float degreesPerSec, int dir)
        {
            _target = target;
            _vector = orbitVector;
            _degreesPerSecond = degreesPerSec;
            _direction = Mathf.Clamp(dir, -1, 1);
        }

        public bool IsNoOrbit()
        {
            return Target == null;
        }
    }
}