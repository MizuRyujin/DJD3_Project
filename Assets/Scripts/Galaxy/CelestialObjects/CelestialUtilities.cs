﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Galaxy.CelestialObjects
{
    [ExecuteAlways]
    public class CelestialUtilities : MonoBehaviour
    {
        [SerializeField] private Vector3? _targetPosition = default;
        [SerializeField] private Vector3? _savedPosition = default;
        [SerializeField] private CelestialObject _target = default;
        [SerializeField] private int _timeToCheck = default;
        [SerializeField] private double _timeOfLastUpdate = default;
        [SerializeField] private double _deltaTime = default;

        private const string I_HOPE_THIS_DOESNT_AFFECT_PERFORMANCE =
            "The worlds can be one together " +
            "Cosmos without hatred " +
            "Stars like diamonds in your eyes " +
            "The ground can be space, space, space, space, space " +
            "With feet marching towards a peaceful sky " +
            "All the moonmen want things their way " +
            "But we make sure they see the sun " +
            "Goodbye moonmen " +
            "Yeah we say goodbye moonmen " +
            "Goodbye moonmen " +
            "Goodbye moonmen " +
            "Oh goodbye " +
            "Cosmos without hatred " +
            "Diamond stars of cosmic light " +
            "Quasars shine through endless night " +
            "And everything is one in the beauty " +
            "And now we say goodbye moonmen " +
            "Yeah we say goodbye moonmen " +
            "Goodbye moonmen " +
            "Goodbye moonmen " +
            "Oh goodbye.";

        [SerializeField] private int _minutes = default;
        [SerializeField] private int _seconds = default;

        public Vector3 GetGetOrbitPositionFromElapsedTime(int minutes, int seconds)
        {
            _target = null;
            # if UnityEditor
            _timeOfLastUpdate = EditorApplication.timeSinceStartup;
            # endif
            _savedPosition = transform.position;
            _targetPosition = default;

            if (_target == null)
                _target = GetComponent<CelestialObject>();

            if (_target == null)
            {
                Debug.LogError($"A CelestialObject script was not found on {name}.");
                return default;
            }

            _timeToCheck = seconds + (minutes * 60);
            GameObject dummy = new GameObject(I_HOPE_THIS_DOESNT_AFFECT_PERFORMANCE);
            dummy.transform.position = _target.transform.position;

            dummy.transform.RotateAround(
                _target.OuterOrbitInfo.Target.position,
                _target.OuterOrbitInfo.Vector,
                _target.OuterOrbitInfo.DegreesPerSecond * _timeToCheck * _target.OuterOrbitInfo.Direction);

            _targetPosition = dummy.transform.position;
            DestroyImmediate(dummy);
            return _targetPosition == null ? Vector3.zero : _targetPosition.Value;
        }

        private void Update()
        {
            if (_targetPosition == null) return;

            double thisUpdate = default;

            # if UNITY_EDITOR
            thisUpdate = EditorApplication.timeSinceStartup;
            # endif

            _deltaTime = thisUpdate - _timeOfLastUpdate;

            _savedPosition = Vector3.Lerp(_savedPosition.Value, _targetPosition.Value, (float) _deltaTime * 2);

            _timeOfLastUpdate = thisUpdate;
        }

        public void MoveToNewPosition()
        {
            if (_targetPosition != null)
                _target.ParentOrbit.transform.position = _targetPosition.Value;
        }

        private void OnDrawGizmos()
        {
            if (_targetPosition != null)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(_savedPosition.Value, _target.Size);
                Gizmos.color = Color.gray;
                Gizmos.DrawLine(transform.position, _savedPosition.Value);
            }
        }
    }
}