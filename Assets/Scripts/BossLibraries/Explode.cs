﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explode : MonoBehaviour
{
    [SerializeField] bool _destroyAfterExplosion = true;
    [SerializeField] private GameObject _explosionParticlesPrefab;

    public void Explosion()
    {
        if (_explosionParticlesPrefab)
        {
            ParticleSystem p = Instantiate(_explosionParticlesPrefab, transform.position, transform.rotation).GetComponentInChildren<ParticleSystem>();
            if (p.isPlaying) p.Stop();
            p.Play();
        }
        if (_destroyAfterExplosion) Destroy(gameObject);
    }
}
