﻿using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Player.Player p = other.gameObject.GetComponent<Player.Player>();
            p.AttachedMovement.Rigidbody.AddForce(0f, 2500f, 0f);
            p.OnHit();
        }
    }

    public void KillBill()
    {
        Destroy(gameObject);
    }
}
