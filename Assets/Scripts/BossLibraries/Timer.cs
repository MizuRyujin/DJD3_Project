﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Timer : MonoBehaviour
{
    [SerializeField] private float _timerSeconds;
    [SerializeField] private UnityEvent _onTimerEnd;

    private ActionOverTime[] _actionsOverTime;
    private Coroutine _timerCoroutine;
    public float Progress{get; private set;}

    private void Awake() 
    {
        _actionsOverTime = GetComponents<ActionOverTime>();
    }
    
    // Start is called before the first frame update
    private void Start()
    {
        _timerCoroutine = StartCoroutine(CountTime());
    }

    private IEnumerator CountTime()
    {
        float count = 0;
        while(count < _timerSeconds)
        {
            Progress = count / _timerSeconds;
            if (_actionsOverTime != null)
            {
                for (int i = 0; i < _actionsOverTime.Length; i++)
                {
                    _actionsOverTime[i].OverTime(Progress);
                }
            }
            count += Time.deltaTime;
            yield return null;
        }

        // Csharp action
        onTimerEnd?.Invoke();
        // Unity action
        _onTimerEnd?.Invoke();
    }

    public void CancelTimer()
    {
        StopCoroutine(_timerCoroutine);
    }

    public event Action onTimerEnd;
}
