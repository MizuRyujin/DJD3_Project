/// <summary>
/// Interface determining what all task should have
/// </summary>
public interface ITaskNode
{
    /// <summary>
    /// Method to run the specific task
    /// </summary>
    /// <returns> A fail or success status</returns>
    bool Run();
}
