﻿using Object = UnityEngine.Object;
using UnityEngine;

public class BaseBehaviourTree : MonoBehaviour
{
    [Header("TREE NODES ARE BEING HARD-CODED")]
    [Header("THIS NOT BEING USED RIGHT NOW.")]
    [SerializeField] private Object[] _treeNodes = default;

    private ITaskNode[] _bossTree;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        _bossTree = new ITaskNode[] { };
    }

    private void Start()
    {
        for (int i = 0; i < _bossTree.Length; i++)
        {
            _bossTree[i].Run();
        }
    }
}

