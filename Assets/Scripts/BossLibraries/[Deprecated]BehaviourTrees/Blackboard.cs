using System;
using System.Collections.Generic;
using UnityEngine;

public class Blackboard : MonoBehaviour
{
    [Header("the first element of the value list should be the animation to be used as 1")]
    [Header("Ex: If on the first list you add a string \" animation1\" ")]
    [Header("These to lists are internally converted to a dictionary.")]

    [Tooltip("Name of the key to be used the the dictionary")]
    [SerializeField] private List<string> _dictKeys = default;

    [Tooltip("Object to be link to the key in the dictionary")]
    [SerializeField] private List<GameObject> _dictValues = default;


    public static readonly Dictionary<string, System.Object> treeTasks = default;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        foreach (string s in _dictKeys)
        {
            foreach (System.Object t in _dictValues)
            {
                treeTasks.Add(s, t);
            }
        }
    }
}
