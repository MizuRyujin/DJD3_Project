namespace Scripts
{
    /// <summary>
    /// Class that goes through task in a deterministic fashion
    /// </summary>
    public class SequencerNode : ITaskNode
    {
        /// <summary>
        /// Array of tasks from which this node will select from
        /// </summary>
        private ITaskNode[] _childrenTask;

        /// <summary>
        /// Constructor for the sequencer node
        /// </summary>
        /// <param name="_children">
        /// Collection (array) of tasks to be considered 
        /// children of this node
        /// </param>
        public SequencerNode(params ITaskNode[] _children)
        {
            _childrenTask = new ITaskNode[_children.Length];

            for (int i = 0; i < _children.Length; i++)
            {
                _childrenTask[i] = _children[i];
            }
        }

        /// <summary>
        /// Method to run through the children task given in the constructor. 
        /// Implemented from ITaskNode
        /// </summary>
        /// <returns>
        /// Success if all children return a success status
        /// or failure as soon as 1 child fails
        /// </returns>
        public bool Run()
        {
            for (int i = 0; i < _childrenTask.Length; i++)
            {
                if (!_childrenTask[i].Run()) return false;
            }

            return true;
        }
    }
}