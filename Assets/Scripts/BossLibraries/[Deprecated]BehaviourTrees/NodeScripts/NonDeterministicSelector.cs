using System;

/// <summary>
/// Class that selects task in a non deterministic fashion
/// </summary>
public class NonDeterministicSelector : ITaskNode
{
    /// <summary>
    /// Reference to a Random instance;
    /// </summary>
    private Random _rnd;

    /// <summary>
    /// Array of tasks from which this node will select from
    /// </summary>
    private ITaskNode[] _childrenTask = default;

    /// <summary>
    /// Constructor for the non deterministic selector node
    /// </summary>
    /// <param name="_children">
    /// Collection (array) of tasks to be considered 
    /// children of this node
    /// </param>
    public NonDeterministicSelector(params ITaskNode[] _children)
    {
        _rnd = new Random();

        _childrenTask = new ITaskNode[_children.Length];

        for (int i = 0; i < _children.Length; i++)
        {
            _childrenTask[i] = _children[i];
        }
    }

    /// <summary>
    /// Method to run through the children task given in the constructor,
    /// after they are shuffled. Implemented from ITaskNode
    /// </summary>
    /// <returns>
    /// Success when the first child returns a success status
    /// or failure if all child tasks fail
    /// </returns>
    public bool Run()
    {
        ITaskNode[] shuffled = RandomShuffle(_childrenTask);

        for (int i = 0; i < shuffled.Length; i++)
        {
            if (shuffled[i].Run()) return true;
        }

        return false;
    }

    /// <summary>
    /// Method to shuffle the array of tasks
    /// </summary>
    /// <param name="original">
    /// The original array, created in the 
    /// constructor
    /// </param>
    /// <returns>
    /// A shuffled array of child tasks
    /// </returns>
    private ITaskNode[] RandomShuffle(ITaskNode[] original)
    {
        ITaskNode[] shuffle = (ITaskNode[])original.Clone();

        int n = shuffle.Length;
        int k;

        while (n > 1)
        {
            k = _rnd.Next(n);
            n--;
            ITaskNode aux = shuffle[n];
            shuffle[n] = shuffle[k];
            shuffle[k] = aux;
        }

        return shuffle;
    }
}