﻿using UnityEngine;

namespace Scripts
{
    /// <summary>
    /// Class responsible to play a specific animation from the Animator component
    /// </summary>
    public class PlayAnimation : ITaskNode
    {
        /// <summary>
        /// Name of the animation in the Animator (Animator State)
        /// </summary>
        private string _animationName = default;

        /// <summary>
        /// Reference to the Animator component
        /// </summary>
        private Animator _animator = default;

        /// <summary>
        /// Variable to control the animation current play time
        /// </summary>
        private float elapsedTime;

        /// <summary>
        /// Variable to store the animation full lenght
        /// </summary>
        private float animLength;

        public PlayAnimation(string animName, Animator animator)
        {
            _animationName = animName;
            _animator = animator;
        }

        /// <summary>
        /// Method implemented from ITaskNode. Runs a specific animation 
        /// from the Animator
        /// </summary>
        /// <returns>
        /// True when the animation is finished
        /// </returns>
        public bool Run()
        {
            throw new System.NotImplementedException();

            _animator.Play(_animationName);
            while (_animator.GetCurrentAnimatorClipInfo(0).Length < elapsedTime)
            {
                elapsedTime++;
                // return TaskStatus.Running;
            }

            // return TaskStatus.Complete
        }
    }
}
