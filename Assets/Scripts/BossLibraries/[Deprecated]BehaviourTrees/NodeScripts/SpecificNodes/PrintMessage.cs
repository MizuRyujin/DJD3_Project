﻿using Random = System.Random;
using UnityEngine;

namespace Scripts
{
    /// <summary>
    /// Class responsible to create debug messages for behaviour trees for 
    /// different nodes
    /// </summary>
    public class PrintMessage : ITaskNode
    {
        /// <summary>
        /// Variable that stores the desired message
        /// </summary>
        private string _message;

        /// <summary>
        /// Variable to store the reference to an instance of Random
        /// </summary>
        private Random _rnd;

        /// <summary>
        /// Variable to check if this task is being used to indicate a
        /// selector task tree
        /// </summary>
        private bool _selector;

        /// <summary>
        /// Constructor to create a debug message when testing behaviour trees
        /// </summary>
        /// <param name="msg">
        ///  The specific message you want to show. Default is null or use _ to 
        /// be considered as null
        /// </param>
        /// <param name="rnd">
        /// An instance of System.Random. Default is null
        /// </param>
        /// <param name="slctr">
        /// Bool to be used in case of print message is used to indicate the 
        /// existence of a selector tree.false Default is false
        /// </param>
        public PrintMessage(string msg = null, Random rnd = null, bool slctr = false)
        {
            if (msg != "_")
            {
                _message = msg;
            }
            _rnd = rnd;
            _selector = slctr;
        }

        /// <summary>
        /// Method implemented from ITaskNode, prints a debug message
        /// </summary>
        /// <returns> Success or fail value of this task </returns>
        public bool Run()
        {
            //* Check to see if the Random variable is null...
            if (_rnd == null)
            {
                //* ... if it is, check for personalized debug message
                if (_message == null)
                {
                    Debug.Log("I went through with this tree");
                    return CheckForSelectorNode(_selector);
                }
                else
                {
                    Debug.Log(_message);
                    return CheckForSelectorNode(_selector);
                }
            }
            //* If this class was given a Random instance.
            //* Normally used for selector nodes
            else
            {     
                float rndValue = (float)_rnd.NextDouble();

                if (rndValue > 0.75f)
                {
                    Debug.Log("This had a random val of: " + rndValue);
                    return true;
                }
                else
                {
                    Debug.Log("This had a random val of: " + rndValue);
                    return false;
                }
            }
        }

        /// <summary>
        /// Method to determine if this class is being used in a selector node
        /// </summary>
        /// <param name="selector">
        /// Value of the selector bool. Given in the 
        /// in the constructor
        /// </param>
        /// <returns> False if in a selector node, to let it continue.
        ///  True if otherwise.
        /// </returns>
        private bool CheckForSelectorNode(bool selector)
        {
            bool isSelector;

            if (_selector)
            {
                isSelector = false;
            }
            else
            {
                isSelector =  true;
            }

            return isSelector;
        }
    }
}
