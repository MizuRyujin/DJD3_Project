using UnityEngine;

namespace Scripts
{
    /// <summary>
    /// Abstract class to be used as a base to all boss attacks
    /// </summary>
    public abstract class BossAttacks
    {
        /// <summary>
        /// Abstract method to instantiate an attack
        /// </summary>
        /// <param name="prefab"></param>
        public abstract void InstantiateAttack(Transform spawn);
    }
}