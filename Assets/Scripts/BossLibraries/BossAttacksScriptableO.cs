using UnityEngine;

namespace Scripts
{
    [CreateAssetMenu(fileName = "BossAttacks", menuName = "Boss/Attacks", order = 0)]
    public class BossAttacksScriptableO : ScriptableObject
    {
        [SerializeField] private string _attack1 = default;
        [SerializeField] private float _att1Time = default;
        [SerializeField] private GameObject _att1Prefab = default;
        [SerializeField] private string _attack2 = default;
        [SerializeField] private float _att2Time = default;
        [SerializeField] private GameObject _att2Prefab = default;
        [SerializeField] private string _attack3 = default;
        [SerializeField] private float _att3Time = default;
        [SerializeField] private GameObject _att3Prefab = default;


        //* Properties to be accessed
        public string Attack1 => _attack1;
        public float Att1Time => _att1Time;
        public GameObject Att1Prefab => _att1Prefab;
        public string Attack2 => _attack2;
        public float Att2Time => _att2Time;
        public GameObject Att2Prefab => _att2Prefab;
        public string Attack3 => _attack3;
        public float Att3Time => _att3Time;
        public GameObject Att3Prefab => _att3Prefab;
    }
}