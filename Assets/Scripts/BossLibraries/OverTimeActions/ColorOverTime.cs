﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorOverTime : ActionOverTime
{
    [SerializeField] private Color _startColor = Color.black;
    [SerializeField] private Color _finalColor = Color.white;
    [SerializeField] private Renderer _renderer = default;

    private Material _material;
    protected void Awake()
    {
        _material = _renderer.material;
    }

    public override void OverTime(float progress)
    {
        _material.SetColor("_BaseColor", Color.Lerp(_startColor, _finalColor, progress));
    }
}
