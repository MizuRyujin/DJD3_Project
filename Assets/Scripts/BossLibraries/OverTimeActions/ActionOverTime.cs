﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Timer))]
public abstract class ActionOverTime : MonoBehaviour
{
    public abstract void OverTime(float progress);
}
