﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleOverTime : ActionOverTime
{
    [SerializeField] private Vector3 _startScale;
    [SerializeField] private Vector3 _finalScale;

    public override void OverTime(float progress)
    {
        transform.localScale = Vector3.Lerp(_startScale, _finalScale, progress);
    }
}
