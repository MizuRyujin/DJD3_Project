using UnityEngine;
using NPBehave;
using Random = UnityEngine.Random;

namespace Scripts
{
    /// <summary>
    /// Class responsible for the creation of the Frag boss behaviour tree
    /// </summary>
    public class FragTree : MonoBehaviour
    {
        //* Tree general variables
        /// <summary>
        /// Reference to the boss attack objects
        /// </summary>
        [SerializeField] private BossAttacksScriptableO _bossAttacks = default;
        [SerializeField] private Transform[] _ballSpawns = default;
        [SerializeField] private Collider _armSwipeCol = default;
        [SerializeField] private AudioSource _sourceArmSwipe = default;
        [SerializeField] private Transform _ringSpawn = default;

        /// <summary>
        /// Variable to store the root node
        /// </summary>
        private Root _behaviourTree;

        /// <summary>
        /// Variable to store behaviour tree blackboard
        /// </summary>
        private NPBehave.Blackboard _blackboard;

        /// <summary>
        /// Reference to the boss stats script
        /// </summary>
        private BossStats _stats;

        //* Attack scripts and variables
        private FireballAttack _fireball;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _fireball = new FireballAttack(_bossAttacks.Att1Prefab);
            _stats = GetComponent<BossStats>();
            _behaviourTree = CreateBehaviourTree();
            _blackboard = _behaviourTree.Blackboard;
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
#if UNITY_EDITOR
            Debugger debugger = (Debugger)this.gameObject.AddComponent(typeof(Debugger));
            debugger.BehaviorTree = _behaviourTree;
#endif

            _behaviourTree.Start();
        }

        /// <summary>
        /// Method to create the behaviour tree
        /// </summary>
        /// <returns> Root node with all the tree constructed</returns>
        private Root CreateBehaviourTree()
        {
            return new Root(

                new Service(0.125f, CheckBossHP,

                    new Selector(

                        new Failer(
                            new Wait(5f)
                        )
                        { Label = "IdleTime" },

                        new BlackboardCondition(
                            "bossHP", Operator.IS_EQUAL, 3f, Stops.IMMEDIATE_RESTART,
                            CreateFase1()
                        )
                        { Label = "Fase1" },

                        new BlackboardCondition(
                            "bossHP", Operator.IS_EQUAL, 2f, Stops.IMMEDIATE_RESTART,
                            CreateFase2()
                        )
                        { Label = "Fase2" },

                        new BlackboardCondition(
                            "bossHP", Operator.IS_EQUAL, 1f, Stops.IMMEDIATE_RESTART,
                            CreateFase3()
                        )
                        { Label = "Fase3" },

                        BossDied()
                    )
                )
            );
        }

        /// <summary>
        /// Method to be used in a Service node (from NPBehave), to check if
        /// the boss hp changed
        /// </summary>
        private void CheckBossHP()
        {
            float bossHP = _stats.BossHP; //* Get the HP value from script
            _behaviourTree.Blackboard["bossHP"] = bossHP; //* Store it in the blackboard
        }

        /// <summary>
        /// Override method to play boss arms throwing the ball
        /// </summary>
        /// <param name="animator">
        /// Reference to the boss animator component
        /// </param>
        /// <param name="animName">
        /// The animation to be played
        /// </param>
        public void PlayAnimation(string animName)
        {
            _stats.Animator.SetTrigger(animName);
        }

        /// <summary>
        /// Method to create the first fase of combat
        /// </summary>
        /// <returns>
        /// A random sequence with the fase 1 attacks
        /// </returns>
        private RandomSequence CreateFase1()
        {
            return new RandomSequence(
                FireballAttack(),
                ArmSwipe(),
                LavaRing()
            )
            { Label = "F1_Attacks" };
        }

        /// <summary>
        /// Method to create the second fase of combat
        /// </summary>
        /// <returns>
        /// A random sequence with the fase 2 attacks
        /// </returns>
        private RandomSequence CreateFase2()
        {
            return new RandomSequence(
                FireballAttack(),
                ArmSwipe(),
                LavaRing()
            )
            { Label = "F2_Attacks" };
        }

        /// <summary>
        /// Method to create the third fase of combat
        /// </summary>
        /// <returns>
        /// A random sequence with the fase 3 attacks
        /// </returns>
        private RandomSequence CreateFase3()
        {
            return new RandomSequence(
                FireballAttack(),
                ArmSwipe(),
                LavaRing()
            )
            { Label = "F3_Attacks" };
        }

        /// <summary>
        /// Method to create fireball attack sequence to be used in the BT
        /// </summary>
        /// <returns> Fireball attack sequence </returns>
        private Sequence FireballAttack()
        {
            return new Sequence(
                new Action(() =>
                {
                    PlayAnimation(_bossAttacks.Attack1);
                })
                { Label = "Fireball" },

                new Wait(_bossAttacks.Att1Time)
            );
        }

        /// <summary>
        /// Method to instantiate a droplet of lava, to be used as an animation
        /// event
        /// </summary>
        private void InstantiateDroplet()
        {
            _fireball.InstantiateAttack(
                _ballSpawns[Random.Range(0, _ballSpawns.Length)]);
        }

        /// <summary>
        /// Method to create de arm swipe attack sequence to be used in the BT
        /// </summary>
        /// <returns> Arm swipe attack sequence </returns>
        private Sequence ArmSwipe()
        {
            return new Sequence(
                new Action(() =>
                {
                    PlayAnimation(_bossAttacks.Attack2);
                })
                { Label = "ArmSwipe" },

                new Wait(_bossAttacks.Att2Time)
            );
        }

        private void EnableColider()
        {
            _armSwipeCol.enabled = true;
            _sourceArmSwipe.Play();
        }

        private void DisableColider()
        {
            _armSwipeCol.enabled = false;
        }

        private Sequence LavaRing()
        {
            return new Sequence(
                new Action(() =>
                {
                    PlayAnimation(_bossAttacks.Attack3);
                })
                { Label = "Scream Anim" },

                new Action(() => Instantiate(
                    _bossAttacks.Att3Prefab, _ringSpawn)
                    )
                { Label = "LavaRing" },

                new Wait(_bossAttacks.Att3Time)
            );
        }

        private Sequence BossDied()
        {
            return new Sequence(
                new Action(() => PlayAnimation(_bossAttacks.Attack3))
                { Label = "KillSequence" },

                new Wait(1f),

                new Action(() => PlayAnimation(_bossAttacks.Attack3)),

                new Wait(1f),

                new Action(() => PlayAnimation(_bossAttacks.Attack3)),

                new Wait(1f),

                new Action(() => PlayAnimation(_bossAttacks.Attack3)),

                new Wait(1f),

                new Action(() => SceneLoader.Load("ThankYou")),
                
                new Action(() => OnStop())
            );
        }


        /// <summary>
        /// Method to lazily create debug messages to be shown 
        /// in a behaviour tree. Prints the node number (even numbers only) and
        /// has a Wait node of 1sec between them
        /// </summary>
        /// <param name="x">
        /// Number of messages to be created
        /// </param>
        /// <param name="message">
        /// The specified message to be shown
        /// </param>
        /// <returns>
        /// Array of nodes
        /// </returns>
        private Node[] CreateDebugMessages(int x, string message)
        {
            Node[] debugMess = new Node[x];

            for (int i = 0; i < x; i++)
            {
                int n = i;
                if (i % 2 == 0)
                {
                    debugMess[i] = new Action(() => Debug.Log(
                        "This node " + n + " " + message));
                }
                else
                {
                    debugMess[i] = new Wait(1f);
                }
            }

            return debugMess;
        }

        /// <summary>
        /// Method to stop the behaviour tree once the boss is dead
        /// </summary>
        private void StopBehaviourTree()
        {
            if (_behaviourTree != null &&
                _behaviourTree.CurrentState == Node.State.ACTIVE)
            {
                _behaviourTree.Stop();
            }
        }

        /// <summary>
        /// Method to be called when object is destroyed
        /// </summary>
        public void OnStop()
        {
            StopBehaviourTree();
        }
    }
}