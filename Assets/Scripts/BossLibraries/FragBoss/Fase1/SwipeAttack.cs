﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class SwipeAttack : MonoBehaviour
    {
        /// <summary>
        /// OnCollisionEnter is called when this collider/rigidbody has begun
        /// touching another rigidbody/collider.
        /// </summary>
        /// <param name="other">The Collision data associated with this collision.</param>
        void OnTriggerEnter(Collider other)
        {
            print("Hit something");
            if (other.gameObject.CompareTag("Player"))
            {
                Player.Player p = other.gameObject.GetComponent<Player.Player>();
                p.OnHit();
                p.AttachedMovement.Rigidbody.AddForce(0f, 5000f, 0f);
            }
        }
    }
}
