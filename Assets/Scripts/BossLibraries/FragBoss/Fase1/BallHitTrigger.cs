﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class BallHitTrigger : MonoBehaviour
{
    public UnityEvent OnTrigger;
    public bool IsActive { get; set; }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    private void OnTriggerEnter(Collider other)
    {
        if(!IsActive) return;
        if(other.gameObject.CompareTag("Boss"))
        {
            Scripts.BossStats b = other.gameObject.GetComponent<Scripts.BossStats>();
            b.OnHit();
            OnTrigger?.Invoke();
        }
    }
}
