﻿using System.Collections;
using UnityEngine;

public class RingAttack : MonoBehaviour
{
    [SerializeField] private float _timeInSec = default;
    private WaitForSeconds _timer;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        _timer = new WaitForSeconds(_timeInSec);
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    private void OnCollisionEnter(Collision other)
    {
        print("Hit something");
        if (other.gameObject.CompareTag("Player"))
        {
            Player.Player p = other.gameObject.GetComponent<Player.Player>();
            p.OnHit();
        }
    }

    /// <summary>
    /// Method to start coroutine in a unity event
    /// </summary>
    public void WaitCoroutine()
    {
        StartCoroutine(WaitBeforeDestroy());
    }

    /// <summary>
    /// Coroutine to leave the ring alive for a bit before destroy
    /// </summary>
    /// <returns>
    /// Time to wait
    /// </returns>
    private IEnumerator WaitBeforeDestroy()
    {
        yield return _timer;
        Destroy(gameObject);
    }
}
