using UnityEngine;

namespace Scripts
{
    /// <summary>
    /// Class responsible for the fireball attack
    /// </summary>
    public class FireballAttack : BossAttacks
    {
        private GameObject _attackPrefab;

        public FireballAttack(GameObject prefab)
        {
            _attackPrefab = prefab;
        }

        /// <summary>
        /// Override method to instantiate the ball
        /// </summary>
        /// <param name="prefab"> FireBall prefab</param>
        public override void InstantiateAttack(Transform spawn)
        {
            GameObject.Instantiate(_attackPrefab, spawn.position, Quaternion.identity);
        }
    }
}