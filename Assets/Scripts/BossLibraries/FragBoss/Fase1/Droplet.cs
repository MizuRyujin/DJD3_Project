﻿using UnityEngine;

public class Droplet : MonoBehaviour
{
    [SerializeField] private GameObject _explosion;
    [SerializeField] private GameObject _fireballPrefab;

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ground"))
        {
            Vector3 v = Scripts.BossStats.Instance.position - transform.position;

            GameObject f = Instantiate(
                _fireballPrefab, transform.position, Quaternion.identity);

            ParticleSystem p = Instantiate(
                _explosion, transform.position,
                Quaternion.identity).GetComponentInChildren<ParticleSystem>();
            
            if(p.isPlaying) p.Stop();
            p.Play();
            
            f.transform.forward = v;
            
            Destroy(gameObject);
        }
    }
}
