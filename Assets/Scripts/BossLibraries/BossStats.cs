﻿using UnityEngine;

namespace Scripts
{
    /// <summary>
    /// Class responsible for having the stats of the Frag boss
    /// </summary>
    public class BossStats : MonoBehaviour
    {
        /// <summary>
        /// Variable to store Boss Hit Points value
        /// </summary>
        [SerializeField] private float _bossHP;

        /// <summary>
        /// Variable reference to animator component
        /// </summary>
        private Animator _animator;
        private static Transform _instance;
        public static Transform Instance => _instance;

        //* Properties
        /// <summary>
        /// Property to read the current boss HP
        /// </summary>
        public float BossHP => _bossHP;

        /// <summary>
        /// Property to read the reference to the animator component
        /// </summary>
        public Animator Animator => _animator;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _animator = GetComponent<Animator>();

            _instance = transform;
        }

        /// <summary>
        /// Method that has the functionality of when the boss is hit
        /// </summary>
        private void Hit()
        {
            _animator.SetTrigger("Hit");
            _bossHP -= 1f;
            print("Got hit");
        }

        /// <summary>
        /// Open method to be called from outside the call when the boss is hit
        /// </summary>
        public void OnHit()
        {
            Hit();
        }
    }
}
