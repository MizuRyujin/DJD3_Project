﻿using System.Collections;
using UnityEngine;

public class ThrowToTarget : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _throwLength;
    [SerializeField] private float _throwAngle = 45.0f;

    public void Throw()
    {
        transform.SetParent(null);
        StartCoroutine(LeapTowards(_speed, _throwAngle, 9f));
    }
    private IEnumerator LeapTowards(float throwSpeed, float throwAngle, float gravity)
    {
        Vector3 targ = transform.position + (transform.forward * _throwLength);
        float distance = Vector3.Distance(transform.position, targ);

        // Get parabola velocities
        float parabVelocity = distance / (Mathf.Sin(2 * throwAngle * Mathf.Deg2Rad) / gravity);
        float velZ = Mathf.Sqrt(parabVelocity) * Mathf.Cos(throwAngle * Mathf.Deg2Rad);
        float velY = Mathf.Sqrt(parabVelocity) * Mathf.Sin(throwAngle * Mathf.Deg2Rad);

        float ballFlightDuration = distance / velZ;
        float elapsedTime = 0;

        transform.LookAt(targ);

        while (true)
        {
            transform.Translate(0, (velY - (gravity * elapsedTime)) * throwSpeed * Time.deltaTime, velZ * Time.deltaTime, Space.Self);

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private void OnDrawGizmos() 
    {
        Vector3 targ = transform.position + (transform.forward * _throwLength);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, targ);
        Gizmos.DrawSphere(targ, 1f);
    }
}
