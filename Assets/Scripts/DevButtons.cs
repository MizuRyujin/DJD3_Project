﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DevButtons : MonoBehaviour
{
    public static bool AllCharges {get; private set;}
    PlayerControllerActions actions;

    // Start is called before the first frame update
    void Awake()
    {
        actions = new PlayerControllerActions();

        actions.DevButtons.Reset.performed += ctx => ResetScene();
        actions.DevButtons.Exit.performed += ctx => ExitDemo();
        actions.DevButtons.GetAllCharges.performed += ctx => GetAllCharges();
        actions.DevButtons.ForceBoss.performed += ctx => ForceEnterBossScene();
        actions.DevButtons.GoToMainMenu.performed += ctx => ForceMainMenu();
        
        AllCharges = false;
    }

    private void ResetScene()
    {
        Debug.LogWarning("Reload Scene");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        OnActionTaken("Reset Scene");
    }

    private void ExitDemo()
    {
        Debug.LogWarning("Quit App");
        Application.Quit();
    }

    private void GetAllCharges()
    {
        AllCharges = !AllCharges;
        OnActionTaken("Get all charges");
    }

    private void ForceEnterBossScene()
    {
        SceneManager.LoadScene("BossFlexScene");
        OnActionTaken("Force enter boss");
    }

    private void ForceMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
        OnActionTaken("Force main menu");
    }

    private void OnActionTaken(string name)
    {
        Debug.Log(name);
        Debug.LogError("This action was used: " + name);
    }

    private void OnEnable()
    {
        actions.Enable();
    }
    private void OnDisable()
    {
        actions.Disable();
    }
}