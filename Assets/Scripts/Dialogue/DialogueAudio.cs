﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

namespace Dialogue
{
    public class DialogueAudio : MonoBehaviour
    {
        [SerializeField] AudioClip _lineStartAudio;
        [SerializeField] AudioClip _nextLineAudio;
        [SerializeField] AudioClip _emotionExample;
        [SerializeField] AudioClip _charSound;
        private DialogueRunner _runner;

        private void Awake()
        {
            _runner = GetComponent<DialogueRunner>();
        }

        public void PlayLineStart()
        {
            if (_lineStartAudio == null) return;
        }

        public void PlayNextLineAudio()
        {
            if (_nextLineAudio == null) return;
        }

        public void CharUpdateSound(string lol)
        {
            if(_charSound == null) return;
        }
    }
}
