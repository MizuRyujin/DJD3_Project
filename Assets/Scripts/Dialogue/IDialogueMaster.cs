using Yarn.Unity;

namespace Dialogue
{
    public interface IDialogueMaster
    {
        /// <summary>
        /// Starts a dialogue from a given node
        /// </summary>
        /// <param name="node"> Node name to start from</param>
        void StartDialogue(string node);
        /// <summary>
        /// Creates a new dialogue to be run further on in the game
        /// </summary>
        /// <param name="dialogue"></param>
        void SetNewDialogue(YarnProgram dialogue);
        /// <summary>
        /// Activates the dialogue to be skipped automatically by time.
        /// </summary>
        /// <param name="time"></param>
        /// <param name="force"></param>
        void SetSkipSpeed(float time, bool force);

        DialogueRunner DialogueEvents {get;}
    }
}