using UnityEngine;
using Yarn.Unity;
using System.Collections.Generic;

namespace Dialogue
{
    [CreateAssetMenu(menuName = "NSOY/Dialogue Tracker")]
    public class DialogueTracker : ScriptableObject
    {
        private HashSet<string> _visitedDialogue = new HashSet<string>();

        public bool CheckNodeName(string nodeName)
        {
            if (nodeName == null || _visitedDialogue == null) return false;
            return _visitedDialogue.Contains(nodeName);
        } 

        public void NodeComplete(string nodeName)
        {
            _visitedDialogue.Add(nodeName);
        }
    }
}