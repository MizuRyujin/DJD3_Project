using Yarn.Unity;
using UnityEngine;

namespace Dialogue
{
    public class DialogueSkipper : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Events.UnityEvent onNextLine;
        private DialogueControls _controls;
        private DialogueUI _uiController;
        private bool _forceNextLine = false;
        private float _nextLineTime = 0;
        private float _counterTime;
        private bool _skip;
        
        private void OnEnable() {
            _controls.Enable();
        }

        private void OnDisable() {
            _controls.Disable();
        }

        private void Awake() 
        {
            _controls = new DialogueControls();
            _controls.Dialogue.Skip.performed += ctx => _skip = true;
            _uiController = GetComponent<DialogueUI>();    
        }

        private void Update()
        {
            if (_forceNextLine)
            {
                _counterTime -= Time.deltaTime;
                if (_counterTime <= 0f)
                {
                    _counterTime = _nextLineTime;
                    _uiController.MarkLineComplete();
                }
            }

            if (_skip)
            {
                onNextLine?.Invoke();
                _uiController.MarkLineComplete();
                _skip = false;
            }    
        }

        public void ForceLines(float timeToNext, bool shouldForce)
        {
            _forceNextLine = shouldForce;
            _nextLineTime = timeToNext;
            _counterTime = _nextLineTime;
        }
    }
}