using UnityEngine;
using Yarn.Unity;

namespace Dialogue
{
    public class DialogueUpdater : MonoBehaviour, IDialogueMaster
    {
        [SerializeField] private DialogueTracker _tracker = default;
        [SerializeField] private DialogueRunner _runner = default;
        [SerializeField] private DialogueUI _dialogueUI = default;
        [SerializeField] private DialogueSkipper _dialogueSkipper = default;
        [SerializeField] private CinematicBars _dialogueBars = default;

        public DialogueRunner DialogueEvents => _runner;

        private void Awake()
        {
            _runner.AddFunction("visited", 1, delegate (Yarn.Value[] parameters)
            {
                // Name of the node is parameter 0 from YarnValue
                var nodeName = parameters[0];
                return _tracker.CheckNodeName(nodeName.AsString);
            });
        }

        /// <summary>
        /// Starts a new dialogue from a specified node
        /// </summary>
        /// <param name="node"> Node name to start the dialogue</param>
        public void StartDialogue(string node)
        {
            _runner.StartDialogue(node);
            _dialogueBars?.Show(250f, .3f);
        }

        public void SetNewDialogue(YarnProgram dialogue)
        {
            _runner.Add(dialogue);
        }

        public void SetSkipSpeed(float time, bool force)
        {
            _dialogueSkipper.ForceLines(time, force);
        }
    }
}