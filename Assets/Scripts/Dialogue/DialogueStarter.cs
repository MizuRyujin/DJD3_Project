﻿using System.Collections;
using System.Collections.Generic;
using Interactables;
using UnityEngine;
using Yarn.Unity;

namespace Dialogue
{
    public class DialogueStarter : Interaction
    {
        [SerializeField] private bool _countToNextPhrase;
        [SerializeField] private float _timeToNextPhrase;
        [SerializeField] YarnProgram _dialogue;
        [SerializeField] private UnityEngine.Events.UnityEvent onDialogueComplete;
        private IDialogueMaster _dialogueRunner;
        public string TalkNode { get; private set; }

        private void OnDialogueComplete()
        {
            onDialogueComplete?.Invoke();
        }
        void Start()
        {
            _dialogueRunner = GameObject.Find("DialogueRunner").GetComponent<DialogueUpdater>();
            _dialogueRunner.DialogueEvents.onDialogueComplete.AddListener(OnDialogueComplete);
            if (_dialogue != null)
            {
                TalkNode = _dialogue.name;
                _dialogueRunner.SetNewDialogue(_dialogue);
            }
        }

        public override void Interact(Player.Player p = null, Vector3 dir = default)
        {
            base.Interact();
            _dialogueRunner.StartDialogue(TalkNode);
            _dialogueRunner.SetSkipSpeed(_timeToNextPhrase, _countToNextPhrase);
        }
    }
}