﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobUI : MonoBehaviour
{
    [SerializeField] float _speed;
    [Range(0, 1)]
    [SerializeField] float _amplitude;

    private RectTransform _rectTrans;

    private void Awake() {
        _rectTrans = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = _rectTrans.anchoredPosition;
        newPos.y += Mathf.Sin(Time.time * _speed) + 10f;
        newPos.x -= 5f;
        _rectTrans.anchoredPosition = newPos * _amplitude;
    }
}
